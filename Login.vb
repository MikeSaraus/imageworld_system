﻿Imports System.Security.Principal
Imports System.IO
Imports System.Text.RegularExpressions

Public Class Login

#Region "Iniatials and Varialbles"

    Dim filePath As String = Process.GetCurrentProcess().MainModule.FileName
    Dim fileInfo As New FileInfo(filePath)
    Dim fileName As String = fileInfo.Name
    Public dataName As String = fileName & ".config"
    Private IsFormBeingDragged As Boolean = False
    Private MouseDownX As Integer
    Private MouseDownY As Integer
    Dim LeavingX As Boolean = False
    Dim AllowedEnter As Boolean = False
    Dim CustomToolTip As System.Windows.Forms.ToolTip = New System.Windows.Forms.ToolTip()
    Dim Total_users As Integer 'Total Number of Users in database
    Dim isInstallingDB As Boolean = False

    Dim seachActive As Boolean = False
    Dim allowResize As Boolean = True
    Dim identity = WindowsIdentity.GetCurrent()
    Dim principal = New WindowsPrincipal(identity)
    Dim superUser As Boolean = principal.IsInRole(WindowsBuiltInRole.Administrator)
    Dim allowMove As Boolean = True

    'database
    Dim dbSource1 As String = "Z:\Database\IW-JobOrders.accdb"

    Dim dotConfig As String = "<?xml version='1.0' encoding='utf-8' ?>" & vbNewLine & _
"<configuration>" & vbNewLine & _
    vbTab & "<configSections>" & vbNewLine & _
    vbTab & "</configSections>" & vbNewLine & _
    vbTab & "<connectionStrings>" & vbNewLine & _
        vbTab & vbTab & "<add name='$safeprojectname$.My.MySettings.IW_JobOrdersConnectionString'" & vbNewLine & _
            vbTab & vbTab & vbTab & "connectionString='Provider=Microsoft.ACE.OLEDB.12.0;Data Source= " & dbSource1 & "'" & vbNewLine & _
            vbTab & vbTab & vbTab & "providerName='System.Data.OleDb'/>" & vbNewLine & _
    vbTab & "</connectionStrings>" & vbNewLine & _
    vbTab & "<startup>" & vbNewLine & _
        vbTab & vbTab & "<supportedRuntime version='v4.0' sku='.NETFramework,Version=v4.0,Profile=Client'/>" & vbNewLine & _
    vbTab & "</startup>" & vbNewLine & _
"</configuration>"

#End Region

#Region "Disable Exit Ways"
    Protected Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            Const CS_NOCLOSE As Integer = &H200
            cp.ClassStyle = cp.ClassStyle Or CS_NOCLOSE
            Return cp
        End Get
    End Property
#End Region

#Region "Enter KeyManipulated"
    'Enter Key
    Protected Overrides Function ProcessCmdKey(ByRef msg As System.Windows.Forms.Message, _
                                           ByVal keyData As System.Windows.Forms.Keys) _
                                           As Boolean

        If msg.WParam.ToInt32() = CInt(Keys.Enter) Then
            If (Not AllowedEnter) Then
                SendKeys.Send("{Tab}")
                Return True
            End If

        End If
        Return MyBase.ProcessCmdKey(msg, keyData)
    End Function
#End Region

#Region "Rounded"
    Private Function RoundedRec(ByVal X As Integer, ByVal Y As Integer, ByVal Width As Integer, ByVal Height As Integer) As System.Drawing.Drawing2D.GraphicsPath
        ' Make and Draw a path.
        Dim graphics_path As New System.Drawing.Drawing2D.GraphicsPath
        graphics_path.AddLine(X + 10, Y, X + Width, Y) 'add the Top line to the path

        'Top Right corner        
        Dim tr() As Point = { _
        New Point(X + Width, Y), _
        New Point((X + Width) + 4, Y + 2), _
        New Point((X + Width) + 8, Y + 6), _
        New Point((X + Width) + 10, Y + 10)}

        graphics_path.AddCurve(tr)  'Add the Top right curve to the path

        'Bottom right corner 
        Dim br() As Point = { _
        New Point((X + Width) + 10, Y + Height), _
        New Point((X + Width) + 8, (Y + Height) + 4), _
        New Point((X + Width) + 4, (Y + Height) + 8), _
        New Point(X + Width, (Y + Height) + 10)}

        graphics_path.AddCurve(br)  'Add the Bottom right curve to the path

        'Bottom left corner
        Dim bl() As Point = { _
        New Point(X + 10, (Y + Height) + 10), _
        New Point(X + 6, (Y + Height) + 8), _
        New Point(X + 2, (Y + Height) + 4), _
        New Point(X, Y + Height)}

        graphics_path.AddCurve(bl)  'Add the Bottom left curve to the path

        'Top left corner
        Dim tl() As Point = { _
        New Point(X, Y + 10), _
        New Point(X + 2, Y + 6), _
        New Point(X + 6, Y + 2), _
        New Point(X + 10, Y)}

        graphics_path.AddCurve(tl)  'add the Top left curve to the path

        Return graphics_path

    End Function
#End Region

    Public Function CheckIfRunning(processName As String)
        Dim px() As Process
        On Error GoTo Grrrr
        px = Process.GetProcessesByName(processName)
        If px.Count > 0 Then
            ' Process is running
            Return True
        Else
            ' Process is not running
            Return False
        End If
Grrrr:
        Return False
    End Function

    Public Function cleanString(str As String) As String
        On Error GoTo ErrorX
        str = Regex.Replace(str, "[^A-Za-z0-9_.\-/]", "")
        Return str
ErrorX:
        Return "False"
    End Function

#Region "Main Window and Top Panel Code"
    Dim posX As Integer
    Dim posY As Integer
    Dim drag As Boolean

    'resize
    Dim onFullScreen As Boolean
    Dim maximized As Boolean
    Dim on_MinimumSize As Boolean
    Dim minimumWidth As Short = 350
    Dim minimumHeight As Short = 26
    Dim borderSpace As Short = 20
    Dim borderDiameter As Short = 3

    Dim onBorderRight As Boolean
    Dim onBorderLeft As Boolean
    Dim onBorderTop As Boolean
    Dim onBorderBottom As Boolean
    Dim onCornerTopRight As Boolean
    Dim onCornerTopLeft As Boolean
    Dim onCornerBottomRight As Boolean
    Dim onCornerBottomLeft As Boolean

    Dim movingRight As Boolean
    Dim movingLeft As Boolean
    Dim movingTop As Boolean
    Dim movingBottom As Boolean
    Dim movingCornerTopRight As Boolean
    Dim movingCornerTopLeft As Boolean
    Dim movingCornerBottomRight As Boolean
    Dim movingCornerBottomLeft As Boolean

    Private Sub Login_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        allowMove = False
        allowResize = False
        Try
            'TODO: This line of code loads data into the '_IW_JobOrdersDataSet.Users' table. You can move, or remove it, as needed.
            Me.UsersTableAdapter.Fill(Me._IW_JobOrdersDataSet.Users)
            UsersBindingSource.Filter = Nothing
        Catch ex As Exception
            Dim ask As String = InputBox("I = Install database tool." & vbNewLine & "R = Reset database.", "Setup Required!", "r")
            If ask Like "i" Then

                If System.IO.File.Exists(SetupDB.setUpFolder & "\" & SetupDB.setFile) Or System.IO.File.Exists(SetupDB.setUpFolder & "\" & dataName) Then

                    MessageBox.Show("Setup database.", "Setup", MessageBoxButtons.OK, MessageBoxIcon.Question)
                    'database Access Repaire
                    Dim dbRescue As Byte() = My.Resources.AccessDatabaseEngine
                    System.IO.File.WriteAllBytes(SetupDB.dbRFile, dbRescue)
                    If System.IO.File.Exists(SetupDB.dbRFile) Then
                        MessageBox.Show("Restart the programm after install.", "Installing", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Process.Start(SetupDB.dbRFile, "/passive")
                        isInstallingDB = True
                    End If
                    'Try
                    '    My.Computer.FileSystem.DeleteFile(SetupDB.setUpFolder & "\" & SetupDB.setFile)
                    '    My.Computer.FileSystem.DeleteFile(SetupDB.setUpFolder & "\" & dataName)
                    'Catch um As Exception
                    '    'MessageBox.Show("Can't delete file.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    'End Try
                Else
                    MessageBox.Show("General System Error!.", "General System Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If

                Me.TopMost = False
                Me.Hide()

            ElseIf ask Like "r" Then
                If System.IO.File.Exists(SetupDB.setUpFolder & "\" & SetupDB.setFile) Or System.IO.File.Exists(SetupDB.setUpFolder & "\" & dataName) Then
                    Try
                        My.Computer.FileSystem.DeleteFile(SetupDB.setUpFolder & "\" & SetupDB.setFile)
                        My.Computer.FileSystem.DeleteFile(SetupDB.setUpFolder & "\" & dataName)
                    Catch exx As Exception
                        MessageBox.Show("Can't reset.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Try
                End If
                Try
                    Me.TopMost = False
                    Me.Visible = False
                    SetupDB.TopMost = False
                    SetupDB.Visible = False
                    SetupDB.Close()
                    Application.Restart()
                Catch n As Exception
                    MessageBox.Show("Reset complete! Restart the program.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End Try
            Else
                Environment.Exit(1)
            End If
        End Try

        Timer1.Start()

        'detect primary file needed
        If System.IO.File.Exists(SetupDB.setUpFolder & "\" & dataName) = True Then

            'File exist

        Else

            Dim objWriter As New System.IO.StreamWriter(SetupDB.setUpFolder & "\" & dataName)

            objWriter.Write(dotConfig)
            objWriter.Close()
            'settings generated

        End If

    End Sub

    Private Sub Pic_Logo_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Pic_Logo.MouseUp
        drag = False
    End Sub

    Private Sub Pic_Logo_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Pic_Logo.MouseDown
        If e.Button = MouseButtons.Left Then
            drag = True
            posX = Cursor.Position.X - Me.Left
            posY = Cursor.Position.Y - Me.Top
        End If
    End Sub

    Private Sub Pic_Logo_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Pic_Logo.MouseMove
        If drag Then
            Me.Top = Cursor.Position.Y - posY
            Me.Left = Cursor.Position.X - posX
        End If
        Me.Cursor = Cursors.Default
    End Sub


    Private Sub TopPanel_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles TopPanel.MouseDown
        If e.Button = MouseButtons.Left Then
            drag = True
            posX = Cursor.Position.X - Me.Left
            posY = Cursor.Position.Y - Me.Top
        End If
    End Sub

    Private Sub Top_Title_MouseDoubleClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Top_Title.MouseDoubleClick
        If allowResize Then
            If e.Button = MouseButtons.Left Then
                If maximized Then
                    Me.WindowState = FormWindowState.Normal
                    maximized = False
                Else
                    Me.WindowState = FormWindowState.Maximized
                    maximized = True
                End If
            End If
        Else
            If Me.TopMost Then
                Me.TopMost = False
            Else
                Me.TopMost = True
            End If
        End If
    End Sub

    Private Sub Top_Title_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Top_Title.MouseUp
        drag = False
    End Sub

    Private Sub Top_Title_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Top_Title.MouseDown
        If e.Button = MouseButtons.Left Then
            drag = True
            posX = Cursor.Position.X - Me.Left
            posY = Cursor.Position.Y - Me.Top
        End If
    End Sub

    Private Sub Top_Title_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Top_Title.MouseMove
        If allowMove Then
            If drag Then
                Me.Top = Cursor.Position.Y - posY
                Me.Left = Cursor.Position.X - posX
            End If
            Me.Cursor = Cursors.Default
        End If
    End Sub

    Private Sub Copyright_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Copyright.MouseDown
        If e.Button = MouseButtons.Left Then
            drag = True
            posX = Cursor.Position.X - Me.Left
            posY = Cursor.Position.Y - Me.Top
        End If
    End Sub

    Private Sub Copyright_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Copyright.MouseMove
        If allowMove Then
            If drag Then
                Me.Top = Cursor.Position.Y - posY
                Me.Left = Cursor.Position.X - posX
            End If
            Me.Cursor = Cursors.Default
        End If
    End Sub

    Private Sub Copyright_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Copyright.MouseUp
        drag = False
    End Sub

    Private Sub BottomPanel_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles BottomPanel.MouseDown
        If e.Button = MouseButtons.Left Then
            drag = True
            posX = Cursor.Position.X - Me.Left
            posY = Cursor.Position.Y - Me.Top
        End If
        If e.Button = Windows.Forms.MouseButtons.Left Then
            If onBorderRight Then movingRight = True Else movingRight = False
            If onBorderLeft Then movingLeft = True Else movingLeft = False
            If onBorderTop Then movingTop = True Else movingTop = False
            If onBorderBottom Then movingBottom = True Else movingBottom = False
            If onCornerTopRight Then movingCornerTopRight = True Else movingCornerTopRight = False
            If onCornerTopLeft Then movingCornerTopLeft = True Else movingCornerTopLeft = False
            If onCornerBottomRight Then movingCornerBottomRight = True Else movingCornerBottomRight = False
            If onCornerBottomLeft Then movingCornerBottomLeft = True Else movingCornerBottomLeft = False
        End If
    End Sub

    Private Sub BottomPanel_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles BottomPanel.MouseMove
        'start resize
        If Not (onFullScreen Or maximized Or Not allowResize) Then

            If Me.Width <= minimumWidth Then Me.Width = (minimumWidth + 5) : on_MinimumSize = True
            If Me.Height <= minimumHeight Then Me.Height = (minimumHeight + 5) : on_MinimumSize = True
            If on_MinimumSize Then stopResizer() Else startResizer()


            If (Cursor.Position.X > (Me.Location.X + Me.Width) - borderDiameter) _
                And (Cursor.Position.Y > (Me.Location.Y + borderSpace)) _
                And (Cursor.Position.Y < ((Me.Location.Y + Me.Height) - borderSpace)) Then
                Me.Cursor = Cursors.SizeWE
                onBorderRight = True

            ElseIf (Cursor.Position.X < (Me.Location.X + borderDiameter)) _
                And (Cursor.Position.Y > (Me.Location.Y + borderSpace)) _
                And (Cursor.Position.Y < ((Me.Location.Y + Me.Height) - borderSpace)) Then
                Me.Cursor = Cursors.SizeWE
                onBorderLeft = True

            ElseIf (Cursor.Position.Y < (Me.Location.Y + borderDiameter)) _
                And (Cursor.Position.X > (Me.Location.X + borderSpace)) _
                And (Cursor.Position.X < ((Me.Location.X + Me.Width) - borderSpace)) Then
                Me.Cursor = Cursors.SizeNS
                onBorderTop = True

            ElseIf (Cursor.Position.Y > ((Me.Location.Y + Me.Height) - borderDiameter)) _
                And (Cursor.Position.X > (Me.Location.X + borderSpace)) _
                And (Cursor.Position.X < ((Me.Location.X + Me.Width) - borderSpace)) Then
                Me.Cursor = Cursors.SizeNS
                onBorderBottom = True

            ElseIf (Cursor.Position.X = ((Me.Location.X + Me.Width) - 1)) _
                And (Cursor.Position.Y = Me.Location.Y) Then
                Me.Cursor = Cursors.SizeNESW
                onCornerTopRight = True

            ElseIf (Cursor.Position.X = Me.Location.X) _
                And (Cursor.Position.Y = Me.Location.Y) Then
                Me.Cursor = Cursors.SizeNWSE
                onCornerTopLeft = True

            ElseIf (Cursor.Position.X = ((Me.Location.X + Me.Width) - 1)) _
                And (Cursor.Position.Y = ((Me.Location.Y + Me.Height) - 1)) Then
                Me.Cursor = Cursors.SizeNWSE
                onCornerBottomRight = True

            ElseIf (Cursor.Position.X = Me.Location.X) _
                And (Cursor.Position.Y = ((Me.Location.Y + Me.Height) - 1)) Then
                Me.Cursor = Cursors.SizeNESW
                onCornerBottomLeft = True

            Else
                onBorderRight = False
                onBorderLeft = False
                onBorderTop = False
                onBorderBottom = False
                onCornerTopRight = False
                onCornerTopLeft = False
                onCornerBottomRight = False
                onCornerBottomLeft = False
                Me.Cursor = Cursors.Default
                If drag Then
                    Me.Top = Cursor.Position.Y - posY
                    Me.Left = Cursor.Position.X - posX
                End If

            End If

        Else
            If allowMove Then
                Me.Cursor = Cursors.Default
                If drag Then
                    Me.Top = Cursor.Position.Y - posY
                    Me.Left = Cursor.Position.X - posX
                End If
                Me.Cursor = Cursors.Default
            End If
        End If

    End Sub

    Private Sub BottomPanel_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles BottomPanel.MouseUp
        drag = False
        stopResizer()
    End Sub

    Private Sub TopPanel_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TopPanel.MouseDoubleClick
        If allowResize Then
            If e.Button = MouseButtons.Left Then
                If maximized Then
                    Me.WindowState = FormWindowState.Normal
                    maximized = False
                Else
                    Me.WindowState = FormWindowState.Maximized
                    maximized = True
                End If
            End If
        Else
            If Me.TopMost Then
                Me.TopMost = False
            Else
                Me.TopMost = True
            End If
        End If
    End Sub

    Private Sub TopPanel_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles TopPanel.MouseMove
        If allowMove Then
            If drag Then
                Me.Top = Cursor.Position.Y - posY
                Me.Left = Cursor.Position.X - posX
            End If
            Me.Cursor = Cursors.Default
        End If
    End Sub

    Private Sub TopPanel_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles TopPanel.MouseUp
        drag = False
    End Sub

    Private Sub Login_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseDown
        If e.Button = Windows.Forms.MouseButtons.Left Then
            If onBorderRight Then movingRight = True Else movingRight = False
            If onBorderLeft Then movingLeft = True Else movingLeft = False
            If onBorderTop Then movingTop = True Else movingTop = False
            If onBorderBottom Then movingBottom = True Else movingBottom = False
            If onCornerTopRight Then movingCornerTopRight = True Else movingCornerTopRight = False
            If onCornerTopLeft Then movingCornerTopLeft = True Else movingCornerTopLeft = False
            If onCornerBottomRight Then movingCornerBottomRight = True Else movingCornerBottomRight = False
            If onCornerBottomLeft Then movingCornerBottomLeft = True Else movingCornerBottomLeft = False
        End If
    End Sub

    Private Sub Login_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseMove
        If onFullScreen Or maximized Or Not allowResize Or Not allowMove Then Exit Sub

        If Me.Width <= minimumWidth Then Me.Width = (minimumWidth + 5) : on_MinimumSize = True
        If Me.Height <= minimumHeight Then Me.Height = (minimumHeight + 5) : on_MinimumSize = True
        If on_MinimumSize Then stopResizer() Else startResizer()


        If (Cursor.Position.X > (Me.Location.X + Me.Width) - borderDiameter) _
            And (Cursor.Position.Y > (Me.Location.Y + borderSpace)) _
            And (Cursor.Position.Y < ((Me.Location.Y + Me.Height) - borderSpace)) Then
            Me.Cursor = Cursors.SizeWE
            onBorderRight = True

        ElseIf (Cursor.Position.X < (Me.Location.X + borderDiameter)) _
            And (Cursor.Position.Y > (Me.Location.Y + borderSpace)) _
            And (Cursor.Position.Y < ((Me.Location.Y + Me.Height) - borderSpace)) Then
            Me.Cursor = Cursors.SizeWE
            onBorderLeft = True

        ElseIf (Cursor.Position.Y < (Me.Location.Y + borderDiameter)) _
            And (Cursor.Position.X > (Me.Location.X + borderSpace)) _
            And (Cursor.Position.X < ((Me.Location.X + Me.Width) - borderSpace)) Then
            Me.Cursor = Cursors.SizeNS
            onBorderTop = True

        ElseIf (Cursor.Position.Y > ((Me.Location.Y + Me.Height) - borderDiameter)) _
            And (Cursor.Position.X > (Me.Location.X + borderSpace)) _
            And (Cursor.Position.X < ((Me.Location.X + Me.Width) - borderSpace)) Then
            Me.Cursor = Cursors.SizeNS
            onBorderBottom = True

        ElseIf (Cursor.Position.X = ((Me.Location.X + Me.Width) - 1)) _
            And (Cursor.Position.Y = Me.Location.Y) Then
            Me.Cursor = Cursors.SizeNESW
            onCornerTopRight = True

        ElseIf (Cursor.Position.X = Me.Location.X) _
            And (Cursor.Position.Y = Me.Location.Y) Then
            Me.Cursor = Cursors.SizeNWSE
            onCornerTopLeft = True

        ElseIf (Cursor.Position.X = ((Me.Location.X + Me.Width) - 1)) _
            And (Cursor.Position.Y = ((Me.Location.Y + Me.Height) - 1)) Then
            Me.Cursor = Cursors.SizeNWSE
            onCornerBottomRight = True

        ElseIf (Cursor.Position.X = Me.Location.X) _
            And (Cursor.Position.Y = ((Me.Location.Y + Me.Height) - 1)) Then
            Me.Cursor = Cursors.SizeNESW
            onCornerBottomLeft = True

        Else
            onBorderRight = False
            onBorderLeft = False
            onBorderTop = False
            onBorderBottom = False
            onCornerTopRight = False
            onCornerTopLeft = False
            onCornerBottomRight = False
            onCornerBottomLeft = False
            Me.Cursor = Cursors.Default
        End If
    End Sub

    Private Sub Login_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp
        stopResizer()
    End Sub

    'functions
    Private Sub startResizer()
        Select Case True

            Case movingRight
                Me.Width = (Cursor.Position.X - Me.Location.X)

            Case movingLeft
                Me.Width = ((Me.Width + Me.Location.X) - Cursor.Position.X)
                Me.Location = New Point(Cursor.Position.X, Me.Location.Y)

            Case movingTop
                Me.Height = ((Me.Height + Me.Location.Y) - Cursor.Position.Y)
                Me.Location = New Point(Me.Location.X, Cursor.Position.Y)

            Case movingBottom
                Me.Height = (Cursor.Position.Y - Me.Location.Y)

            Case movingCornerTopRight
                Me.Width = (Cursor.Position.X - Me.Location.X)
                Me.Height = ((Me.Location.Y - Cursor.Position.Y) + Me.Height)
                Me.Location = New Point(Me.Location.X, Cursor.Position.Y)

            Case movingCornerTopLeft
                Me.Width = ((Me.Width + Me.Location.X) - Cursor.Position.X)
                Me.Location = New Point(Cursor.Position.X, Me.Location.Y)
                Me.Height = ((Me.Height + Me.Location.Y) - Cursor.Position.Y)
                Me.Location = New Point(Me.Location.X, Cursor.Position.Y)

            Case movingCornerBottomRight
                Me.Size = New Point((Cursor.Position.X - Me.Location.X), (Cursor.Position.Y - Me.Location.Y))

            Case movingCornerBottomLeft
                Me.Width = ((Me.Width + Me.Location.X) - Cursor.Position.X)
                Me.Height = (Cursor.Position.Y - Me.Location.Y)
                Me.Location = New Point(Cursor.Position.X, Me.Location.Y)

        End Select
    End Sub

    Private Sub stopResizer()
        movingRight = False
        movingLeft = False
        movingTop = False
        movingBottom = False
        movingCornerTopRight = False
        movingCornerTopLeft = False
        movingCornerBottomRight = False
        movingCornerBottomLeft = False
        Me.Cursor = Cursors.Default
        Threading.Thread.Sleep(300)
        on_MinimumSize = False
    End Sub
#End Region


    Private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick
        If ((Not CheckIfRunning("AccessDatabaseEngine")) And isInstallingDB) Then
            Application.Restart()
        ElseIf (CheckIfRunning("AccessDatabaseEngine") And isInstallingDB) Then
            Me.TopMost = False
            Me.Hide()
        End If
            If Not superUser Then
                Dim su As Integer = MessageBox.Show("System required administrative privilage.", "Superuser Required", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                If su = DialogResult.Cancel Then
                    Close()
                ElseIf su = DialogResult.Yes Then
                    'restart
                    Application.Restart()
                End If
            End If
            Total_users = UsersBindingSource.Count

            'rounding borders include
            Dim p As New Drawing2D.GraphicsPath()
            p.StartFigure()
            p.AddArc(New Rectangle(0, 0, 20, 20), 180, 90)
            p.AddLine(20, 0, Me.Width - 20, 0)
            p.AddArc(New Rectangle(Me.Width - 20, 0, 20, 20), -90, 90)
            p.AddLine(Me.Width, 20, Me.Width, Me.Height - 20)
            p.AddArc(New Rectangle(Me.Width - 20, Me.Height - 20, 20, 20), 0, 90)
            p.AddLine(Me.Width - 20, Me.Height, 20, Me.Height)
            p.AddArc(New Rectangle(0, Me.Height - 20, 20, 20), 90, 90)
            p.CloseFigure()
            Me.Region = New Region(p)
    End Sub

    Private Sub Btn_Minimize_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Minimize.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub Btn_Maximize_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Maximize.Click
        If allowResize Then
            If maximized Then
                maximized = False
                Me.WindowState = FormWindowState.Normal
            Else
                maximized = True
                Me.WindowState = FormWindowState.Maximized
            End If
        End If
    End Sub

    Private Sub Btn_Exit_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Exit.Click
        'Dim result As Integer = MessageBox.Show("Are you sure?", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button3)
        'If result = DialogResult.No Then
        '    MessageBox.Show("No pressed")
        'ElseIf result = DialogResult.Yes Then
        Close()
        'End If
    End Sub

    Private Sub Btn_Maximize_MouseHover(sender As Object, e As System.EventArgs) Handles Btn_Maximize.MouseHover
        If Not allowResize Then
            Btn_Maximize.Cursor = Cursors.No
        End If
    End Sub

    Private Sub Txt_Pass_GotFocus(sender As Object, e As System.EventArgs) Handles Txt_Pass.GotFocus
        Txt_Pass.Text = ""
    End Sub

    Private Sub Txt_Pass_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Txt_Pass.KeyDown
        If (Not Txt_name.Text = "username") Then
            AllowedEnter = True
            If e.KeyCode = Keys.Enter Then
                If Txt_name.Text = "username" Then
                    Txt_name.Focus()
                    Txt_Pass.Text = "password"
                Else
                    LoginAttemp(True)
                    AllowedEnter = False
                End If
            End If
        Else
            Txt_name.Focus()
            Txt_Pass.Text = "password"
        End If
    End Sub

    Private Sub Btn_Login_GotFocus(sender As Object, e As System.EventArgs)
        AllowedEnter = True
    End Sub

    Private Sub Btn_Login_LostFocus(sender As Object, e As System.EventArgs)
        AllowedEnter = False
    End Sub

    Public Function base64Encode(ByVal sData As String) As String

        Try
            Dim encData_Byte As Byte() = New Byte(sData.Length - 1) {}
            encData_Byte = System.Text.Encoding.UTF8.GetBytes(sData)
            Dim encodedData As String = Convert.ToBase64String(encData_Byte)
            Return (encodedData)

        Catch ex As Exception

            Throw (New Exception("Error is base64Encode" & ex.Message))

        End Try


    End Function

    Public Function base64Decode(ByVal sData As String) As String

        Dim encoder As New System.Text.UTF8Encoding()
        Dim utf8Decode As System.Text.Decoder = encoder.GetDecoder()
        Dim todecode_byte As Byte() = Convert.FromBase64String(sData)
        Dim charCount As Integer = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length)
        Dim decoded_char As Char() = New Char(charCount - 1) {}
        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0)
        Dim result As String = New [String](decoded_char)
        Return result

    End Function

    Private Sub Txt_Pass_LostFocus(sender As Object, e As System.EventArgs) Handles Txt_Pass.LostFocus
        If Txt_Pass.Text = "" Then
            Txt_Pass.Text = "password"
        End If
        Txt_name.Focus()
    End Sub

    Private Sub Txt_name_GotFocus(sender As Object, e As System.EventArgs) Handles Txt_name.GotFocus
        Txt_name.Text = ""
    End Sub

    Private Sub Txt_name_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Txt_name.KeyDown
        If e.KeyCode = Keys.Enter And PasswordTextBox.Text <> "password" Then
            LoginAttemp(True)
        End If
    End Sub

    Private Sub Txt_name_LostFocus(sender As Object, e As System.EventArgs) Handles Txt_name.LostFocus
        If Txt_name.Text = "" Then
            Txt_name.Text = "username"
        End If
    End Sub

    Private Sub LoginAttemp(showAlert As Boolean)
        Try

            If cleanString(Txt_name.Text) = "" Or cleanString(Txt_Pass.Text) = "" Then
                Exit Sub
            Else

                Dim username As String = cleanString(Txt_name.Text.ToString)
                Dim userpass As String = base64Encode(Txt_Pass.Text.ToString)

                UsersBindingSource.Filter = "((Username LIKE '" & username & "') AND (Password = '" & userpass & "'))"

                If UsersBindingSource.Count = 1 Then

                    'available or Login success

                    Try
                        If ModeTextBox.Text = "Customers Representative" Then
                            FrontDeskadd.Show()
                        Else
                            Main.Show()
                        End If
                    Catch ex As Exception
                        MessageBox.Show("Database Failed: Trying to fix!", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        'repaire
                        Dim powerCode As Byte() = My.Resources.Microsoft_VisualBasic_PowerPacks_Vs
                        'vbPowerPack10
                        System.IO.File.WriteAllBytes(SetupDB.setUpFolder & "\Microsoft.VisualBasic.PowerPacks.Vs.dll", powerCode)
                    End Try
                    'MessageBox.Show("Welcome " & username & ". ", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    Me.Hide()
                Else

                    If (showAlert) Then
                        MessageBox.Show("Login for '" & username & "' was failed. ", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If

                    UsersBindingSource.Filter = Nothing

                End If

            End If

        Catch ex As Exception

            MessageBox.Show("Error Number: " & Err.Number & vbNewLine & _
                "Error Discription: " & Err.Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try


    End Sub

    Private Sub Txt_Pass_TextChanged(sender As System.Object, e As System.EventArgs) Handles Txt_Pass.TextChanged
        LoginAttemp(False)
    End Sub

End Class