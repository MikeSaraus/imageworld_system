﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrontDeskadd
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrontDeskadd))
        Dim InstructionsLabel As System.Windows.Forms.Label
        Dim OutputLabel As System.Windows.Forms.Label
        Dim ColorsLabel As System.Windows.Forms.Label
        Dim UnitLabel As System.Windows.Forms.Label
        Dim QuantityLabel As System.Windows.Forms.Label
        Dim TypeLabel As System.Windows.Forms.Label
        Dim CustomerLabel As System.Windows.Forms.Label
        Me.BottomPanel = New System.Windows.Forms.Panel()
        Me.Btn_Clear = New System.Windows.Forms.Button()
        Me.Btn_Save = New System.Windows.Forms.Button()
        Me.Copyright = New System.Windows.Forms.Label()
        Me.TopPanel = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Top_Error = New System.Windows.Forms.Label()
        Me.Btn_Logo = New System.Windows.Forms.PictureBox()
        Me.Top_Title = New System.Windows.Forms.Label()
        Me.Btn_Exit = New System.Windows.Forms.PictureBox()
        Me.JobOrdersTableAdapter = New $safeprojectname$._IW_JobOrdersDataSetTableAdapters.JobOrdersTableAdapter()
        Me.TableAdapterManager = New $safeprojectname$._IW_JobOrdersDataSetTableAdapters.TableAdapterManager()
        Me.MainTitle = New System.Windows.Forms.Label()
        Me.JobOrdersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me._IW_JobOrdersDataSet = New $safeprojectname$._IW_JobOrdersDataSet()
        Me.JONumberTextBox = New System.Windows.Forms.TextBox()
        Me.InstructionsTextBox = New System.Windows.Forms.TextBox()
        Me.OutputComboBox = New System.Windows.Forms.ComboBox()
        Me.DateRequiredLabel = New System.Windows.Forms.Label()
        Me.DateRequiredDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.DateRecievedLabel = New System.Windows.Forms.Label()
        Me.DateRecievedDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.ColorsComboBox = New System.Windows.Forms.ComboBox()
        Me.UnitTextBox = New System.Windows.Forms.TextBox()
        Me.QuantityTextBox = New System.Windows.Forms.TextBox()
        Me.TypeTextBox = New System.Windows.Forms.TextBox()
        Me.CustomerTextBox = New System.Windows.Forms.TextBox()
        Me.JONumberLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        InstructionsLabel = New System.Windows.Forms.Label()
        OutputLabel = New System.Windows.Forms.Label()
        ColorsLabel = New System.Windows.Forms.Label()
        UnitLabel = New System.Windows.Forms.Label()
        QuantityLabel = New System.Windows.Forms.Label()
        TypeLabel = New System.Windows.Forms.Label()
        CustomerLabel = New System.Windows.Forms.Label()
        Me.BottomPanel.SuspendLayout()
        Me.TopPanel.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Btn_Logo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Btn_Exit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JobOrdersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._IW_JobOrdersDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Label1.BackColor = System.Drawing.Color.Red
        Label1.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Label1.Location = New System.Drawing.Point(438, 34)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(154, 27)
        Label1.TabIndex = 88
        Label1.Text = "Add New Order"
        Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BottomPanel
        '
        Me.BottomPanel.BackColor = System.Drawing.Color.Black
        Me.BottomPanel.Controls.Add(Me.Btn_Clear)
        Me.BottomPanel.Controls.Add(Me.Btn_Save)
        Me.BottomPanel.Controls.Add(Me.Copyright)
        Me.BottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BottomPanel.Location = New System.Drawing.Point(0, 379)
        Me.BottomPanel.Name = "BottomPanel"
        Me.BottomPanel.Size = New System.Drawing.Size(592, 25)
        Me.BottomPanel.TabIndex = 55
        '
        'Btn_Clear
        '
        Me.Btn_Clear.BackColor = System.Drawing.SystemColors.Window
        Me.Btn_Clear.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Btn_Clear.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Btn_Clear.Location = New System.Drawing.Point(398, 2)
        Me.Btn_Clear.Name = "Btn_Clear"
        Me.Btn_Clear.Size = New System.Drawing.Size(91, 20)
        Me.Btn_Clear.TabIndex = 33
        Me.Btn_Clear.Text = "CLEAR"
        Me.Btn_Clear.UseVisualStyleBackColor = False
        '
        'Btn_Save
        '
        Me.Btn_Save.BackColor = System.Drawing.SystemColors.Window
        Me.Btn_Save.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Btn_Save.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Btn_Save.Location = New System.Drawing.Point(495, 2)
        Me.Btn_Save.Name = "Btn_Save"
        Me.Btn_Save.Size = New System.Drawing.Size(91, 20)
        Me.Btn_Save.TabIndex = 32
        Me.Btn_Save.Text = "SAVE"
        Me.Btn_Save.UseVisualStyleBackColor = False
        '
        'Copyright
        '
        Me.Copyright.AutoSize = True
        Me.Copyright.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Copyright.ForeColor = System.Drawing.Color.DimGray
        Me.Copyright.Location = New System.Drawing.Point(7, 6)
        Me.Copyright.Name = "Copyright"
        Me.Copyright.Size = New System.Drawing.Size(171, 13)
        Me.Copyright.TabIndex = 31
        Me.Copyright.Text = "© 2017 Image World Digital System"
        '
        'TopPanel
        '
        Me.TopPanel.BackColor = System.Drawing.Color.Black
        Me.TopPanel.Controls.Add(Me.PictureBox1)
        Me.TopPanel.Controls.Add(Me.Top_Error)
        Me.TopPanel.Controls.Add(Me.Btn_Logo)
        Me.TopPanel.Controls.Add(Me.Top_Title)
        Me.TopPanel.Controls.Add(Me.Btn_Exit)
        Me.TopPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me.TopPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopPanel.Name = "TopPanel"
        Me.TopPanel.Size = New System.Drawing.Size(592, 25)
        Me.TopPanel.TabIndex = 54
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Black
        Me.PictureBox1.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.PictureBox1.ErrorImage = Nothing
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.InitialImage = CType(resources.GetObject("PictureBox1.InitialImage"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(567, 5)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(15, 15)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 56
        Me.PictureBox1.TabStop = False
        Me.PictureBox1.UseWaitCursor = True
        '
        'Top_Error
        '
        Me.Top_Error.AutoSize = True
        Me.Top_Error.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Top_Error.ForeColor = System.Drawing.Color.Red
        Me.Top_Error.Location = New System.Drawing.Point(1196, 6)
        Me.Top_Error.Name = "Top_Error"
        Me.Top_Error.Size = New System.Drawing.Size(29, 13)
        Me.Top_Error.TabIndex = 55
        Me.Top_Error.Text = "Error"
        Me.Top_Error.Visible = False
        '
        'Btn_Logo
        '
        Me.Btn_Logo.BackColor = System.Drawing.Color.Black
        Me.Btn_Logo.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.Btn_Logo.ErrorImage = Nothing
        Me.Btn_Logo.Image = CType(resources.GetObject("Btn_Logo.Image"), System.Drawing.Image)
        Me.Btn_Logo.InitialImage = CType(resources.GetObject("Btn_Logo.InitialImage"), System.Drawing.Image)
        Me.Btn_Logo.Location = New System.Drawing.Point(1235, 5)
        Me.Btn_Logo.Name = "Btn_Logo"
        Me.Btn_Logo.Size = New System.Drawing.Size(15, 15)
        Me.Btn_Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Logo.TabIndex = 54
        Me.Btn_Logo.TabStop = False
        Me.Btn_Logo.UseWaitCursor = True
        '
        'Top_Title
        '
        Me.Top_Title.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.Top_Title.AutoSize = True
        Me.Top_Title.BackColor = System.Drawing.Color.Black
        Me.Top_Title.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.Top_Title.Location = New System.Drawing.Point(606, 6)
        Me.Top_Title.Name = "Top_Title"
        Me.Top_Title.Size = New System.Drawing.Size(10, 13)
        Me.Top_Title.TabIndex = 52
        Me.Top_Title.Text = " "
        Me.Top_Title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Btn_Exit
        '
        Me.Btn_Exit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Exit.ErrorImage = Nothing
        Me.Btn_Exit.Image = CType(resources.GetObject("Btn_Exit.Image"), System.Drawing.Image)
        Me.Btn_Exit.InitialImage = CType(resources.GetObject("Btn_Exit.InitialImage"), System.Drawing.Image)
        Me.Btn_Exit.Location = New System.Drawing.Point(9, 3)
        Me.Btn_Exit.Name = "Btn_Exit"
        Me.Btn_Exit.Size = New System.Drawing.Size(20, 20)
        Me.Btn_Exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Exit.TabIndex = 48
        Me.Btn_Exit.TabStop = False
        '
        'JobOrdersTableAdapter
        '
        Me.JobOrdersTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.EmailTableAdapter = Nothing
        Me.TableAdapterManager.JobOrdersTableAdapter = Me.JobOrdersTableAdapter
        Me.TableAdapterManager.SettingTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = $safeprojectname$._IW_JobOrdersDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UsersTableAdapter = Nothing
        '
        'MainTitle
        '
        Me.MainTitle.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.MainTitle.Font = New System.Drawing.Font("Futura Heavy", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MainTitle.ForeColor = System.Drawing.Color.Yellow
        Me.MainTitle.Image = CType(resources.GetObject("MainTitle.Image"), System.Drawing.Image)
        Me.MainTitle.Location = New System.Drawing.Point(0, 24)
        Me.MainTitle.Name = "MainTitle"
        Me.MainTitle.Size = New System.Drawing.Size(191, 357)
        Me.MainTitle.TabIndex = 87
        '
        'JobOrdersBindingSource
        '
        Me.JobOrdersBindingSource.DataMember = "JobOrders"
        Me.JobOrdersBindingSource.DataSource = Me._IW_JobOrdersDataSet
        '
        '_IW_JobOrdersDataSet
        '
        Me._IW_JobOrdersDataSet.DataSetName = "_IW_JobOrdersDataSet"
        Me._IW_JobOrdersDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'JONumberTextBox
        '
        Me.JONumberTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "JONumber", True))
        Me.JONumberTextBox.Location = New System.Drawing.Point(363, 82)
        Me.JONumberTextBox.Name = "JONumberTextBox"
        Me.JONumberTextBox.Size = New System.Drawing.Size(185, 20)
        Me.JONumberTextBox.TabIndex = 88
        '
        'InstructionsLabel
        '
        InstructionsLabel.AutoSize = True
        InstructionsLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        InstructionsLabel.Location = New System.Drawing.Point(267, 304)
        InstructionsLabel.Name = "InstructionsLabel"
        InstructionsLabel.Size = New System.Drawing.Size(69, 15)
        InstructionsLabel.TabIndex = 103
        InstructionsLabel.Text = "Instructions"
        '
        'InstructionsTextBox
        '
        Me.InstructionsTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.InstructionsTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList
        Me.InstructionsTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "Instructions", True))
        Me.InstructionsTextBox.Location = New System.Drawing.Point(363, 297)
        Me.InstructionsTextBox.Multiline = True
        Me.InstructionsTextBox.Name = "InstructionsTextBox"
        Me.InstructionsTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.InstructionsTextBox.Size = New System.Drawing.Size(185, 33)
        Me.InstructionsTextBox.TabIndex = 97
        '
        'OutputLabel
        '
        OutputLabel.AutoSize = True
        OutputLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        OutputLabel.Location = New System.Drawing.Point(290, 344)
        OutputLabel.Name = "OutputLabel"
        OutputLabel.Size = New System.Drawing.Size(43, 15)
        OutputLabel.TabIndex = 113
        OutputLabel.Text = "Output"
        '
        'OutputComboBox
        '
        Me.OutputComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.OutputComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.OutputComboBox.BackColor = System.Drawing.Color.White
        Me.OutputComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "Output", True))
        Me.OutputComboBox.FormattingEnabled = True
        Me.OutputComboBox.Items.AddRange(New Object() {"CTP – Computer to Plate", "DG – Digital Printing", "Duplo", "PVC"})
        Me.OutputComboBox.Location = New System.Drawing.Point(363, 340)
        Me.OutputComboBox.Name = "OutputComboBox"
        Me.OutputComboBox.Size = New System.Drawing.Size(185, 21)
        Me.OutputComboBox.TabIndex = 98
        '
        'DateRequiredLabel
        '
        Me.DateRequiredLabel.AutoSize = True
        Me.DateRequiredLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateRequiredLabel.Location = New System.Drawing.Point(250, 270)
        Me.DateRequiredLabel.Name = "DateRequiredLabel"
        Me.DateRequiredLabel.Size = New System.Drawing.Size(84, 15)
        Me.DateRequiredLabel.TabIndex = 112
        Me.DateRequiredLabel.Text = "DateRequired"
        '
        'DateRequiredDateTimePicker
        '
        Me.DateRequiredDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.JobOrdersBindingSource, "DateRequired", True))
        Me.DateRequiredDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateRequiredDateTimePicker.Location = New System.Drawing.Point(363, 269)
        Me.DateRequiredDateTimePicker.MaxDate = New Date(2050, 12, 31, 0, 0, 0, 0)
        Me.DateRequiredDateTimePicker.MinDate = New Date(2000, 12, 31, 0, 0, 0, 0)
        Me.DateRequiredDateTimePicker.Name = "DateRequiredDateTimePicker"
        Me.DateRequiredDateTimePicker.Size = New System.Drawing.Size(185, 20)
        Me.DateRequiredDateTimePicker.TabIndex = 96
        Me.DateRequiredDateTimePicker.Value = New Date(2017, 9, 14, 12, 21, 5, 0)
        '
        'DateRecievedLabel
        '
        Me.DateRecievedLabel.AutoSize = True
        Me.DateRecievedLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateRecievedLabel.Location = New System.Drawing.Point(251, 242)
        Me.DateRecievedLabel.Name = "DateRecievedLabel"
        Me.DateRecievedLabel.Size = New System.Drawing.Size(84, 15)
        Me.DateRecievedLabel.TabIndex = 111
        Me.DateRecievedLabel.Text = "DateRecieved"
        '
        'DateRecievedDateTimePicker
        '
        Me.DateRecievedDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.JobOrdersBindingSource, "DateRecieved", True))
        Me.DateRecievedDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateRecievedDateTimePicker.Location = New System.Drawing.Point(363, 237)
        Me.DateRecievedDateTimePicker.MaxDate = New Date(2050, 12, 31, 0, 0, 0, 0)
        Me.DateRecievedDateTimePicker.MinDate = New Date(2000, 12, 31, 0, 0, 0, 0)
        Me.DateRecievedDateTimePicker.Name = "DateRecievedDateTimePicker"
        Me.DateRecievedDateTimePicker.Size = New System.Drawing.Size(185, 20)
        Me.DateRecievedDateTimePicker.TabIndex = 95
        Me.DateRecievedDateTimePicker.Value = New Date(2017, 9, 14, 0, 0, 0, 0)
        '
        'ColorsLabel
        '
        ColorsLabel.AutoSize = True
        ColorsLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ColorsLabel.Location = New System.Drawing.Point(291, 206)
        ColorsLabel.Name = "ColorsLabel"
        ColorsLabel.Size = New System.Drawing.Size(42, 15)
        ColorsLabel.TabIndex = 110
        ColorsLabel.Text = "Colors"
        '
        'ColorsComboBox
        '
        Me.ColorsComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.ColorsComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ColorsComboBox.BackColor = System.Drawing.Color.White
        Me.ColorsComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "Colors", True))
        Me.ColorsComboBox.FormattingEnabled = True
        Me.ColorsComboBox.Items.AddRange(New Object() {"1 – One Color", "4 – Full Colors"})
        Me.ColorsComboBox.Location = New System.Drawing.Point(363, 205)
        Me.ColorsComboBox.Name = "ColorsComboBox"
        Me.ColorsComboBox.Size = New System.Drawing.Size(185, 21)
        Me.ColorsComboBox.TabIndex = 94
        '
        'UnitLabel
        '
        UnitLabel.AutoSize = True
        UnitLabel.Location = New System.Drawing.Point(469, 176)
        UnitLabel.Name = "UnitLabel"
        UnitLabel.Size = New System.Drawing.Size(26, 13)
        UnitLabel.TabIndex = 108
        UnitLabel.Text = "Unit"
        '
        'UnitTextBox
        '
        Me.UnitTextBox.AutoCompleteCustomSource.AddRange(New String() {"Copies", "Box/es", "Book/s", "Pad/s", "Pc/s"})
        Me.UnitTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.UnitTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.UnitTextBox.BackColor = System.Drawing.Color.White
        Me.UnitTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "Unit", True))
        Me.UnitTextBox.Location = New System.Drawing.Point(502, 173)
        Me.UnitTextBox.Name = "UnitTextBox"
        Me.UnitTextBox.Size = New System.Drawing.Size(46, 20)
        Me.UnitTextBox.TabIndex = 92
        '
        'QuantityLabel
        '
        QuantityLabel.AutoSize = True
        QuantityLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        QuantityLabel.Location = New System.Drawing.Point(283, 174)
        QuantityLabel.Name = "QuantityLabel"
        QuantityLabel.Size = New System.Drawing.Size(51, 15)
        QuantityLabel.TabIndex = 107
        QuantityLabel.Text = "Quantity"
        '
        'QuantityTextBox
        '
        Me.QuantityTextBox.BackColor = System.Drawing.Color.White
        Me.QuantityTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "Quantity", True))
        Me.QuantityTextBox.Location = New System.Drawing.Point(363, 173)
        Me.QuantityTextBox.Name = "QuantityTextBox"
        Me.QuantityTextBox.Size = New System.Drawing.Size(100, 20)
        Me.QuantityTextBox.TabIndex = 91
        '
        'TypeLabel
        '
        TypeLabel.AutoSize = True
        TypeLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TypeLabel.Location = New System.Drawing.Point(269, 144)
        TypeLabel.Name = "TypeLabel"
        TypeLabel.Size = New System.Drawing.Size(69, 15)
        TypeLabel.TabIndex = 106
        TypeLabel.Text = "Type of Job"
        '
        'TypeTextBox
        '
        Me.TypeTextBox.AutoCompleteCustomSource.AddRange(New String() {"Calling Card", "Book Publication", "Handbook", "Folio", "Offset", "Duplo", "Reciept", "Poster", "Brochure", "PVC ID", "ID", "Magazines", "Sovenier Program"})
        Me.TypeTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.TypeTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.TypeTextBox.BackColor = System.Drawing.Color.White
        Me.TypeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "Type", True))
        Me.TypeTextBox.Location = New System.Drawing.Point(363, 142)
        Me.TypeTextBox.Name = "TypeTextBox"
        Me.TypeTextBox.Size = New System.Drawing.Size(185, 20)
        Me.TypeTextBox.TabIndex = 90
        '
        'CustomerLabel
        '
        CustomerLabel.AutoSize = True
        CustomerLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CustomerLabel.Location = New System.Drawing.Point(275, 116)
        CustomerLabel.Name = "CustomerLabel"
        CustomerLabel.Size = New System.Drawing.Size(60, 15)
        CustomerLabel.TabIndex = 105
        CustomerLabel.Text = "Customer"
        '
        'CustomerTextBox
        '
        Me.CustomerTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CustomerTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.CustomerTextBox.BackColor = System.Drawing.Color.White
        Me.CustomerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "Customer", True))
        Me.CustomerTextBox.Location = New System.Drawing.Point(363, 111)
        Me.CustomerTextBox.Name = "CustomerTextBox"
        Me.CustomerTextBox.Size = New System.Drawing.Size(185, 20)
        Me.CustomerTextBox.TabIndex = 89
        '
        'JONumberLabel
        '
        Me.JONumberLabel.AutoSize = True
        Me.JONumberLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.JONumberLabel.Location = New System.Drawing.Point(229, 85)
        Me.JONumberLabel.Name = "JONumberLabel"
        Me.JONumberLabel.Size = New System.Drawing.Size(109, 15)
        Me.JONumberLabel.TabIndex = 104
        Me.JONumberLabel.Text = "Job Order Number"
        '
        'FrontDeskadd
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(592, 404)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.JONumberTextBox)
        Me.Controls.Add(InstructionsLabel)
        Me.Controls.Add(Me.InstructionsTextBox)
        Me.Controls.Add(OutputLabel)
        Me.Controls.Add(Me.OutputComboBox)
        Me.Controls.Add(Me.DateRequiredLabel)
        Me.Controls.Add(Me.DateRequiredDateTimePicker)
        Me.Controls.Add(Me.DateRecievedLabel)
        Me.Controls.Add(Me.DateRecievedDateTimePicker)
        Me.Controls.Add(ColorsLabel)
        Me.Controls.Add(Me.ColorsComboBox)
        Me.Controls.Add(UnitLabel)
        Me.Controls.Add(Me.UnitTextBox)
        Me.Controls.Add(QuantityLabel)
        Me.Controls.Add(Me.QuantityTextBox)
        Me.Controls.Add(TypeLabel)
        Me.Controls.Add(Me.TypeTextBox)
        Me.Controls.Add(CustomerLabel)
        Me.Controls.Add(Me.CustomerTextBox)
        Me.Controls.Add(Me.JONumberLabel)
        Me.Controls.Add(Me.BottomPanel)
        Me.Controls.Add(Me.TopPanel)
        Me.Controls.Add(Me.MainTitle)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrontDeskadd"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add New"
        Me.BottomPanel.ResumeLayout(False)
        Me.BottomPanel.PerformLayout()
        Me.TopPanel.ResumeLayout(False)
        Me.TopPanel.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Btn_Logo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Btn_Exit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JobOrdersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._IW_JobOrdersDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BottomPanel As System.Windows.Forms.Panel
    Friend WithEvents Copyright As System.Windows.Forms.Label
    Friend WithEvents TopPanel As System.Windows.Forms.Panel
    Friend WithEvents Top_Error As System.Windows.Forms.Label
    Friend WithEvents Btn_Logo As System.Windows.Forms.PictureBox
    Friend WithEvents Top_Title As System.Windows.Forms.Label
    Friend WithEvents Btn_Exit As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents _IW_JobOrdersDataSet As $safeprojectname$._IW_JobOrdersDataSet
    Friend WithEvents JobOrdersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents JobOrdersTableAdapter As $safeprojectname$._IW_JobOrdersDataSetTableAdapters.JobOrdersTableAdapter
    Friend WithEvents TableAdapterManager As $safeprojectname$._IW_JobOrdersDataSetTableAdapters.TableAdapterManager
    Friend WithEvents Btn_Clear As System.Windows.Forms.Button
    Friend WithEvents Btn_Save As System.Windows.Forms.Button
    Friend WithEvents MainTitle As System.Windows.Forms.Label
    Friend WithEvents JONumberTextBox As System.Windows.Forms.TextBox
    Friend WithEvents InstructionsTextBox As System.Windows.Forms.TextBox
    Friend WithEvents OutputComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents DateRequiredLabel As System.Windows.Forms.Label
    Friend WithEvents DateRequiredDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateRecievedLabel As System.Windows.Forms.Label
    Friend WithEvents DateRecievedDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents ColorsComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents UnitTextBox As System.Windows.Forms.TextBox
    Friend WithEvents QuantityTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TypeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CustomerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents JONumberLabel As System.Windows.Forms.Label
End Class
