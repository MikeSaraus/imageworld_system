﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AInfo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CustomerLabel As System.Windows.Forms.Label
        Dim TypeLabel As System.Windows.Forms.Label
        Dim StatusLabel As System.Windows.Forms.Label
        Dim DateRecievedLabel1 As System.Windows.Forms.Label
        Dim DateRequiredLabel As System.Windows.Forms.Label
        Dim InstructionsLabel As System.Windows.Forms.Label
        Dim JONumberLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AInfo))
        Me.BottomPanel = New System.Windows.Forms.Panel()
        Me.Btn_Logo = New System.Windows.Forms.PictureBox()
        Me.Copyright = New System.Windows.Forms.Label()
        Me._IW_JobOrdersDataSet = New $safeprojectname$._IW_JobOrdersDataSet()
        Me.JobOrdersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.JobOrdersTableAdapter = New $safeprojectname$._IW_JobOrdersDataSetTableAdapters.JobOrdersTableAdapter()
        Me.TableAdapterManager = New $safeprojectname$._IW_JobOrdersDataSetTableAdapters.TableAdapterManager()
        Me.CustomerTextBox = New System.Windows.Forms.TextBox()
        Me.TypeTextBox = New System.Windows.Forms.TextBox()
        Me.StatusTextBox = New System.Windows.Forms.TextBox()
        Me.DateRecievedLabel2 = New System.Windows.Forms.Label()
        Me.DateRequiredDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.InstructionsTextBox = New System.Windows.Forms.TextBox()
        Me.JONumberLabel1 = New System.Windows.Forms.Label()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.RectangleShape2 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.RectangleShape1 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.TotalCounter = New System.Windows.Forms.Label()
        Me.Btn_Last = New System.Windows.Forms.PictureBox()
        Me.Btn_Next = New System.Windows.Forms.PictureBox()
        Me.Btn_Prev = New System.Windows.Forms.PictureBox()
        Me.Btn_First = New System.Windows.Forms.PictureBox()
        Me.Top_Title = New System.Windows.Forms.Label()
        Me.TopPanel = New System.Windows.Forms.Panel()
        Me.Btn_Exit = New System.Windows.Forms.PictureBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        CustomerLabel = New System.Windows.Forms.Label()
        TypeLabel = New System.Windows.Forms.Label()
        StatusLabel = New System.Windows.Forms.Label()
        DateRecievedLabel1 = New System.Windows.Forms.Label()
        DateRequiredLabel = New System.Windows.Forms.Label()
        InstructionsLabel = New System.Windows.Forms.Label()
        JONumberLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Me.BottomPanel.SuspendLayout()
        CType(Me.Btn_Logo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._IW_JobOrdersDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JobOrdersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Btn_Last, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Btn_Next, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Btn_Prev, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Btn_First, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TopPanel.SuspendLayout()
        CType(Me.Btn_Exit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CustomerLabel
        '
        CustomerLabel.AutoSize = True
        CustomerLabel.BackColor = System.Drawing.Color.SlateGray
        CustomerLabel.ForeColor = System.Drawing.Color.Snow
        CustomerLabel.Location = New System.Drawing.Point(69, 144)
        CustomerLabel.Name = "CustomerLabel"
        CustomerLabel.Size = New System.Drawing.Size(54, 13)
        CustomerLabel.TabIndex = 70
        CustomerLabel.Text = "Customer:"
        '
        'TypeLabel
        '
        TypeLabel.AutoSize = True
        TypeLabel.BackColor = System.Drawing.Color.SlateGray
        TypeLabel.ForeColor = System.Drawing.Color.Snow
        TypeLabel.Location = New System.Drawing.Point(64, 170)
        TypeLabel.Name = "TypeLabel"
        TypeLabel.Size = New System.Drawing.Size(59, 13)
        TypeLabel.TabIndex = 71
        TypeLabel.Text = "Discription:"
        '
        'StatusLabel
        '
        StatusLabel.AutoSize = True
        StatusLabel.BackColor = System.Drawing.Color.SlateGray
        StatusLabel.ForeColor = System.Drawing.Color.Snow
        StatusLabel.Location = New System.Drawing.Point(83, 196)
        StatusLabel.Name = "StatusLabel"
        StatusLabel.Size = New System.Drawing.Size(40, 13)
        StatusLabel.TabIndex = 72
        StatusLabel.Text = "Status:"
        '
        'DateRecievedLabel1
        '
        DateRecievedLabel1.AutoSize = True
        DateRecievedLabel1.Location = New System.Drawing.Point(327, 40)
        DateRecievedLabel1.Name = "DateRecievedLabel1"
        DateRecievedLabel1.Size = New System.Drawing.Size(82, 13)
        DateRecievedLabel1.TabIndex = 75
        DateRecievedLabel1.Text = "Date Recieved:"
        '
        'DateRequiredLabel
        '
        DateRequiredLabel.AutoSize = True
        DateRequiredLabel.BackColor = System.Drawing.Color.SlateGray
        DateRequiredLabel.ForeColor = System.Drawing.Color.Snow
        DateRequiredLabel.Location = New System.Drawing.Point(44, 218)
        DateRequiredLabel.Name = "DateRequiredLabel"
        DateRequiredLabel.Size = New System.Drawing.Size(79, 13)
        DateRequiredLabel.TabIndex = 76
        DateRequiredLabel.Text = "Date Required:"
        '
        'InstructionsLabel
        '
        InstructionsLabel.AutoSize = True
        InstructionsLabel.BackColor = System.Drawing.Color.SlateGray
        InstructionsLabel.ForeColor = System.Drawing.Color.Snow
        InstructionsLabel.Location = New System.Drawing.Point(59, 242)
        InstructionsLabel.Name = "InstructionsLabel"
        InstructionsLabel.Size = New System.Drawing.Size(64, 13)
        InstructionsLabel.TabIndex = 77
        InstructionsLabel.Text = "Instructions:"
        '
        'JONumberLabel
        '
        JONumberLabel.AutoSize = True
        JONumberLabel.Location = New System.Drawing.Point(5, 40)
        JONumberLabel.Name = "JONumberLabel"
        JONumberLabel.Size = New System.Drawing.Size(60, 13)
        JONumberLabel.TabIndex = 78
        JONumberLabel.Text = "JONumber:"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.BackColor = System.Drawing.Color.SlateGray
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.Snow
        Label1.Location = New System.Drawing.Point(50, 117)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(30, 9)
        Label1.TabIndex = 88
        Label1.Text = "TOTAL"
        '
        'BottomPanel
        '
        Me.BottomPanel.BackColor = System.Drawing.Color.Black
        Me.BottomPanel.Controls.Add(Me.Btn_Logo)
        Me.BottomPanel.Controls.Add(Me.Copyright)
        Me.BottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BottomPanel.Location = New System.Drawing.Point(0, 289)
        Me.BottomPanel.Name = "BottomPanel"
        Me.BottomPanel.Size = New System.Drawing.Size(506, 15)
        Me.BottomPanel.TabIndex = 70
        '
        'Btn_Logo
        '
        Me.Btn_Logo.BackColor = System.Drawing.Color.Black
        Me.Btn_Logo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Logo.ErrorImage = Nothing
        Me.Btn_Logo.Image = CType(resources.GetObject("Btn_Logo.Image"), System.Drawing.Image)
        Me.Btn_Logo.InitialImage = CType(resources.GetObject("Btn_Logo.InitialImage"), System.Drawing.Image)
        Me.Btn_Logo.Location = New System.Drawing.Point(430, 0)
        Me.Btn_Logo.Name = "Btn_Logo"
        Me.Btn_Logo.Size = New System.Drawing.Size(71, 20)
        Me.Btn_Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Logo.TabIndex = 53
        Me.Btn_Logo.TabStop = False
        '
        'Copyright
        '
        Me.Copyright.AutoSize = True
        Me.Copyright.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Copyright.ForeColor = System.Drawing.Color.DimGray
        Me.Copyright.Location = New System.Drawing.Point(0, 1)
        Me.Copyright.Name = "Copyright"
        Me.Copyright.Size = New System.Drawing.Size(44, 13)
        Me.Copyright.TabIndex = 0
        Me.Copyright.Text = "@ 2017"
        '
        '_IW_JobOrdersDataSet
        '
        Me._IW_JobOrdersDataSet.DataSetName = "_IW_JobOrdersDataSet"
        Me._IW_JobOrdersDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'JobOrdersBindingSource
        '
        Me.JobOrdersBindingSource.DataMember = "JobOrders"
        Me.JobOrdersBindingSource.DataSource = Me._IW_JobOrdersDataSet
        '
        'JobOrdersTableAdapter
        '
        Me.JobOrdersTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.EmailTableAdapter = Nothing
        Me.TableAdapterManager.JobOrdersTableAdapter = Me.JobOrdersTableAdapter
        Me.TableAdapterManager.SettingTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = $safeprojectname$._IW_JobOrdersDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UsersTableAdapter = Nothing
        '
        'CustomerTextBox
        '
        Me.CustomerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "Customer", True))
        Me.CustomerTextBox.Location = New System.Drawing.Point(141, 137)
        Me.CustomerTextBox.Name = "CustomerTextBox"
        Me.CustomerTextBox.Size = New System.Drawing.Size(290, 20)
        Me.CustomerTextBox.TabIndex = 1
        '
        'TypeTextBox
        '
        Me.TypeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "Type", True))
        Me.TypeTextBox.Location = New System.Drawing.Point(141, 163)
        Me.TypeTextBox.Name = "TypeTextBox"
        Me.TypeTextBox.Size = New System.Drawing.Size(290, 20)
        Me.TypeTextBox.TabIndex = 2
        '
        'StatusTextBox
        '
        Me.StatusTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "Status", True))
        Me.StatusTextBox.Location = New System.Drawing.Point(141, 189)
        Me.StatusTextBox.Name = "StatusTextBox"
        Me.StatusTextBox.Size = New System.Drawing.Size(290, 20)
        Me.StatusTextBox.TabIndex = 3
        '
        'DateRecievedLabel2
        '
        Me.DateRecievedLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "DateRecieved", True))
        Me.DateRecievedLabel2.ForeColor = System.Drawing.Color.Red
        Me.DateRecievedLabel2.Location = New System.Drawing.Point(408, 39)
        Me.DateRecievedLabel2.Name = "DateRecievedLabel2"
        Me.DateRecievedLabel2.Size = New System.Drawing.Size(93, 15)
        Me.DateRecievedLabel2.TabIndex = 76
        Me.DateRecievedLabel2.Text = "Date Recieved"
        Me.DateRecievedLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'DateRequiredDateTimePicker
        '
        Me.DateRequiredDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.JobOrdersBindingSource, "DateRequired", True))
        Me.DateRequiredDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateRequiredDateTimePicker.Location = New System.Drawing.Point(141, 214)
        Me.DateRequiredDateTimePicker.Name = "DateRequiredDateTimePicker"
        Me.DateRequiredDateTimePicker.Size = New System.Drawing.Size(290, 20)
        Me.DateRequiredDateTimePicker.TabIndex = 4
        Me.DateRequiredDateTimePicker.Value = New Date(2017, 9, 20, 18, 30, 15, 0)
        '
        'InstructionsTextBox
        '
        Me.InstructionsTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "Instructions", True))
        Me.InstructionsTextBox.Location = New System.Drawing.Point(141, 240)
        Me.InstructionsTextBox.Multiline = True
        Me.InstructionsTextBox.Name = "InstructionsTextBox"
        Me.InstructionsTextBox.Size = New System.Drawing.Size(290, 40)
        Me.InstructionsTextBox.TabIndex = 5
        '
        'JONumberLabel1
        '
        Me.JONumberLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "JONumber", True))
        Me.JONumberLabel1.ForeColor = System.Drawing.Color.Red
        Me.JONumberLabel1.Location = New System.Drawing.Point(64, 40)
        Me.JONumberLabel1.Name = "JONumberLabel1"
        Me.JONumberLabel1.Size = New System.Drawing.Size(100, 15)
        Me.JONumberLabel1.TabIndex = 79
        Me.JONumberLabel1.Text = "JONumber"
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.RectangleShape2, Me.RectangleShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(506, 304)
        Me.ShapeContainer1.TabIndex = 80
        Me.ShapeContainer1.TabStop = False
        '
        'RectangleShape2
        '
        Me.RectangleShape2.Location = New System.Drawing.Point(-4, 25)
        Me.RectangleShape2.Name = "RectangleShape2"
        Me.RectangleShape2.Size = New System.Drawing.Size(514, 33)
        '
        'RectangleShape1
        '
        Me.RectangleShape1.BackColor = System.Drawing.Color.SlateGray
        Me.RectangleShape1.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.RectangleShape1.Location = New System.Drawing.Point(37, 76)
        Me.RectangleShape1.Name = "RectangleShape1"
        Me.RectangleShape1.Size = New System.Drawing.Size(415, 238)
        '
        'TotalCounter
        '
        Me.TotalCounter.BackColor = System.Drawing.Color.Red
        Me.TotalCounter.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TotalCounter.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.TotalCounter.Location = New System.Drawing.Point(44, 76)
        Me.TotalCounter.Name = "TotalCounter"
        Me.TotalCounter.Size = New System.Drawing.Size(46, 39)
        Me.TotalCounter.TabIndex = 81
        Me.TotalCounter.Text = "0"
        Me.TotalCounter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Btn_Last
        '
        Me.Btn_Last.BackColor = System.Drawing.Color.AliceBlue
        Me.Btn_Last.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Last.ErrorImage = Nothing
        Me.Btn_Last.Image = CType(resources.GetObject("Btn_Last.Image"), System.Drawing.Image)
        Me.Btn_Last.InitialImage = CType(resources.GetObject("Btn_Last.InitialImage"), System.Drawing.Image)
        Me.Btn_Last.Location = New System.Drawing.Point(401, 95)
        Me.Btn_Last.Name = "Btn_Last"
        Me.Btn_Last.Size = New System.Drawing.Size(30, 20)
        Me.Btn_Last.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Last.TabIndex = 85
        Me.Btn_Last.TabStop = False
        '
        'Btn_Next
        '
        Me.Btn_Next.BackColor = System.Drawing.Color.AliceBlue
        Me.Btn_Next.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Next.ErrorImage = Nothing
        Me.Btn_Next.Image = CType(resources.GetObject("Btn_Next.Image"), System.Drawing.Image)
        Me.Btn_Next.InitialImage = CType(resources.GetObject("Btn_Next.InitialImage"), System.Drawing.Image)
        Me.Btn_Next.Location = New System.Drawing.Point(365, 95)
        Me.Btn_Next.Name = "Btn_Next"
        Me.Btn_Next.Size = New System.Drawing.Size(30, 20)
        Me.Btn_Next.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Next.TabIndex = 84
        Me.Btn_Next.TabStop = False
        '
        'Btn_Prev
        '
        Me.Btn_Prev.BackColor = System.Drawing.Color.AliceBlue
        Me.Btn_Prev.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Prev.ErrorImage = Nothing
        Me.Btn_Prev.Image = CType(resources.GetObject("Btn_Prev.Image"), System.Drawing.Image)
        Me.Btn_Prev.InitialImage = CType(resources.GetObject("Btn_Prev.InitialImage"), System.Drawing.Image)
        Me.Btn_Prev.Location = New System.Drawing.Point(329, 95)
        Me.Btn_Prev.Name = "Btn_Prev"
        Me.Btn_Prev.Size = New System.Drawing.Size(30, 20)
        Me.Btn_Prev.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Prev.TabIndex = 87
        Me.Btn_Prev.TabStop = False
        '
        'Btn_First
        '
        Me.Btn_First.BackColor = System.Drawing.Color.AliceBlue
        Me.Btn_First.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_First.ErrorImage = Nothing
        Me.Btn_First.Image = CType(resources.GetObject("Btn_First.Image"), System.Drawing.Image)
        Me.Btn_First.InitialImage = CType(resources.GetObject("Btn_First.InitialImage"), System.Drawing.Image)
        Me.Btn_First.Location = New System.Drawing.Point(293, 95)
        Me.Btn_First.Name = "Btn_First"
        Me.Btn_First.Size = New System.Drawing.Size(30, 20)
        Me.Btn_First.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_First.TabIndex = 86
        Me.Btn_First.TabStop = False
        '
        'Top_Title
        '
        Me.Top_Title.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.Top_Title.AutoSize = True
        Me.Top_Title.BackColor = System.Drawing.Color.Black
        Me.Top_Title.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.Top_Title.Location = New System.Drawing.Point(7, 7)
        Me.Top_Title.Name = "Top_Title"
        Me.Top_Title.Size = New System.Drawing.Size(59, 13)
        Me.Top_Title.TabIndex = 51
        Me.Top_Title.Text = "Information"
        Me.Top_Title.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'TopPanel
        '
        Me.TopPanel.BackColor = System.Drawing.Color.Black
        Me.TopPanel.Controls.Add(Me.Btn_Exit)
        Me.TopPanel.Controls.Add(Me.Top_Title)
        Me.TopPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me.TopPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopPanel.Name = "TopPanel"
        Me.TopPanel.Size = New System.Drawing.Size(506, 25)
        Me.TopPanel.TabIndex = 69
        '
        'Btn_Exit
        '
        Me.Btn_Exit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Exit.ErrorImage = Nothing
        Me.Btn_Exit.Image = CType(resources.GetObject("Btn_Exit.Image"), System.Drawing.Image)
        Me.Btn_Exit.InitialImage = CType(resources.GetObject("Btn_Exit.InitialImage"), System.Drawing.Image)
        Me.Btn_Exit.Location = New System.Drawing.Point(479, 3)
        Me.Btn_Exit.Name = "Btn_Exit"
        Me.Btn_Exit.Size = New System.Drawing.Size(20, 20)
        Me.Btn_Exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Exit.TabIndex = 52
        Me.Btn_Exit.TabStop = False
        '
        'Timer1
        '
        '
        'AInfo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(506, 304)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.Btn_Prev)
        Me.Controls.Add(Me.Btn_First)
        Me.Controls.Add(Me.Btn_Last)
        Me.Controls.Add(Me.Btn_Next)
        Me.Controls.Add(JONumberLabel)
        Me.Controls.Add(Me.JONumberLabel1)
        Me.Controls.Add(InstructionsLabel)
        Me.Controls.Add(Me.InstructionsTextBox)
        Me.Controls.Add(DateRequiredLabel)
        Me.Controls.Add(Me.DateRequiredDateTimePicker)
        Me.Controls.Add(DateRecievedLabel1)
        Me.Controls.Add(Me.DateRecievedLabel2)
        Me.Controls.Add(StatusLabel)
        Me.Controls.Add(Me.StatusTextBox)
        Me.Controls.Add(TypeLabel)
        Me.Controls.Add(Me.TypeTextBox)
        Me.Controls.Add(CustomerLabel)
        Me.Controls.Add(Me.CustomerTextBox)
        Me.Controls.Add(Me.BottomPanel)
        Me.Controls.Add(Me.TopPanel)
        Me.Controls.Add(Me.TotalCounter)
        Me.Controls.Add(Me.ShapeContainer1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "AInfo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Artist Info"
        Me.BottomPanel.ResumeLayout(False)
        Me.BottomPanel.PerformLayout()
        CType(Me.Btn_Logo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._IW_JobOrdersDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JobOrdersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Btn_Last, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Btn_Next, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Btn_Prev, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Btn_First, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TopPanel.ResumeLayout(False)
        Me.TopPanel.PerformLayout()
        CType(Me.Btn_Exit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BottomPanel As System.Windows.Forms.Panel
    Friend WithEvents _IW_JobOrdersDataSet As $safeprojectname$._IW_JobOrdersDataSet
    Friend WithEvents JobOrdersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents JobOrdersTableAdapter As $safeprojectname$._IW_JobOrdersDataSetTableAdapters.JobOrdersTableAdapter
    Friend WithEvents TableAdapterManager As $safeprojectname$._IW_JobOrdersDataSetTableAdapters.TableAdapterManager
    Friend WithEvents CustomerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TypeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents StatusTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DateRecievedLabel2 As System.Windows.Forms.Label
    Friend WithEvents DateRequiredDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents InstructionsTextBox As System.Windows.Forms.TextBox
    Friend WithEvents JONumberLabel1 As System.Windows.Forms.Label
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents RectangleShape1 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents TotalCounter As System.Windows.Forms.Label
    Friend WithEvents Btn_Last As System.Windows.Forms.PictureBox
    Friend WithEvents Btn_Next As System.Windows.Forms.PictureBox
    Friend WithEvents Btn_Prev As System.Windows.Forms.PictureBox
    Friend WithEvents Btn_First As System.Windows.Forms.PictureBox
    Friend WithEvents Copyright As System.Windows.Forms.Label
    Friend WithEvents Top_Title As System.Windows.Forms.Label
    Friend WithEvents TopPanel As System.Windows.Forms.Panel
    Friend WithEvents Btn_Logo As System.Windows.Forms.PictureBox
    Friend WithEvents Btn_Exit As System.Windows.Forms.PictureBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents RectangleShape2 As Microsoft.VisualBasic.PowerPacks.RectangleShape
End Class
