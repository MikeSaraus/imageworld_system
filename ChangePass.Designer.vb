﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ChangePass
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ChangePass))
        Me.TopPanel = New System.Windows.Forms.Panel()
        Me.Btn_Exit = New System.Windows.Forms.PictureBox()
        Me.Top_Title = New System.Windows.Forms.Label()
        Me.Pic = New System.Windows.Forms.PictureBox()
        Me.Save = New System.Windows.Forms.Button()
        Me.NewPassword = New System.Windows.Forms.TextBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.BottomPanel = New System.Windows.Forms.Panel()
        Me.TopPanel.SuspendLayout()
        CType(Me.Btn_Exit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Pic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TopPanel
        '
        Me.TopPanel.BackColor = System.Drawing.Color.Black
        Me.TopPanel.Controls.Add(Me.Btn_Exit)
        Me.TopPanel.Controls.Add(Me.Top_Title)
        Me.TopPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me.TopPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopPanel.Name = "TopPanel"
        Me.TopPanel.Size = New System.Drawing.Size(382, 25)
        Me.TopPanel.TabIndex = 50
        '
        'Btn_Exit
        '
        Me.Btn_Exit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Exit.ErrorImage = Nothing
        Me.Btn_Exit.Image = CType(resources.GetObject("Btn_Exit.Image"), System.Drawing.Image)
        Me.Btn_Exit.InitialImage = CType(resources.GetObject("Btn_Exit.InitialImage"), System.Drawing.Image)
        Me.Btn_Exit.Location = New System.Drawing.Point(3, 3)
        Me.Btn_Exit.Name = "Btn_Exit"
        Me.Btn_Exit.Size = New System.Drawing.Size(20, 20)
        Me.Btn_Exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Exit.TabIndex = 52
        Me.Btn_Exit.TabStop = False
        '
        'Top_Title
        '
        Me.Top_Title.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.Top_Title.AutoSize = True
        Me.Top_Title.BackColor = System.Drawing.Color.Black
        Me.Top_Title.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.Top_Title.Location = New System.Drawing.Point(173, 6)
        Me.Top_Title.Name = "Top_Title"
        Me.Top_Title.Size = New System.Drawing.Size(93, 13)
        Me.Top_Title.TabIndex = 51
        Me.Top_Title.Text = "Change Password"
        Me.Top_Title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Pic
        '
        Me.Pic.BackColor = System.Drawing.SystemColors.Control
        Me.Pic.Image = CType(resources.GetObject("Pic.Image"), System.Drawing.Image)
        Me.Pic.Location = New System.Drawing.Point(11, 30)
        Me.Pic.Name = "Pic"
        Me.Pic.Size = New System.Drawing.Size(55, 57)
        Me.Pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Pic.TabIndex = 62
        Me.Pic.TabStop = False
        '
        'Save
        '
        Me.Save.BackColor = System.Drawing.Color.Black
        Me.Save.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Save.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Save.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Save.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.Save.Location = New System.Drawing.Point(74, 63)
        Me.Save.Name = "Save"
        Me.Save.Size = New System.Drawing.Size(296, 19)
        Me.Save.TabIndex = 63
        Me.Save.Text = "Save"
        Me.Save.UseVisualStyleBackColor = False
        '
        'NewPassword
        '
        Me.NewPassword.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.NewPassword.Location = New System.Drawing.Point(74, 36)
        Me.NewPassword.Name = "NewPassword"
        Me.NewPassword.Size = New System.Drawing.Size(296, 20)
        Me.NewPassword.TabIndex = 1
        Me.NewPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Timer1
        '
        '
        'BottomPanel
        '
        Me.BottomPanel.BackColor = System.Drawing.Color.Black
        Me.BottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BottomPanel.Location = New System.Drawing.Point(0, 92)
        Me.BottomPanel.Name = "BottomPanel"
        Me.BottomPanel.Size = New System.Drawing.Size(382, 5)
        Me.BottomPanel.TabIndex = 65
        '
        'ChangePass
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(382, 97)
        Me.Controls.Add(Me.BottomPanel)
        Me.Controls.Add(Me.NewPassword)
        Me.Controls.Add(Me.Save)
        Me.Controls.Add(Me.Pic)
        Me.Controls.Add(Me.TopPanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "ChangePass"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Change Password"
        Me.TopMost = True
        Me.TopPanel.ResumeLayout(False)
        Me.TopPanel.PerformLayout()
        CType(Me.Btn_Exit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Pic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TopPanel As System.Windows.Forms.Panel
    Friend WithEvents Top_Title As System.Windows.Forms.Label
    Friend WithEvents Pic As System.Windows.Forms.PictureBox
    Friend WithEvents Save As System.Windows.Forms.Button
    Friend WithEvents NewPassword As System.Windows.Forms.TextBox
    Friend WithEvents Btn_Exit As System.Windows.Forms.PictureBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents BottomPanel As System.Windows.Forms.Panel
End Class
