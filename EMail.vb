﻿Imports System.Net.Mail
Imports System.IO

Public Class EMail

    Dim mailError As String = ""
    Private IsFormBeingDragged As Boolean = False
    Private MouseDownX As Integer
    Private MouseDownY As Integer
    Dim LeavingX As Boolean = False
    Dim AllowedEnter As Boolean = False
    Dim CustomToolTip As System.Windows.Forms.ToolTip = New System.Windows.Forms.ToolTip()

#Region "Enter KeyManipulated"
    'Enter Key
    Protected Overrides Function ProcessCmdKey(ByRef msg As System.Windows.Forms.Message, _
                                           ByVal keyData As System.Windows.Forms.Keys) _
                                           As Boolean

        If msg.WParam.ToInt32() = CInt(Keys.Enter) Then
            If (Not AllowedEnter) Then
                SendKeys.Send("{Tab}")
                Return True
            End If

        End If
        Return MyBase.ProcessCmdKey(msg, keyData)
    End Function
#End Region

    Public Function base64Encode(ByVal sData As String) As String

        Try
            Dim encData_Byte As Byte() = New Byte(sData.Length - 1) {}
            encData_Byte = System.Text.Encoding.UTF8.GetBytes(sData)
            Dim encodedData As String = Convert.ToBase64String(encData_Byte)
            Return (encodedData)

        Catch ex As Exception

            Throw (New Exception("Error is base64Encode" & ex.Message))

        End Try


    End Function

    Public Function base64Decode(ByVal sData As String) As String

        Dim encoder As New System.Text.UTF8Encoding()
        Dim utf8Decode As System.Text.Decoder = encoder.GetDecoder()
        Dim todecode_byte As Byte() = Convert.FromBase64String(sData)
        Dim charCount As Integer = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length)
        Dim decoded_char As Char() = New Char(charCount - 1) {}
        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0)
        Dim result As String = New [String](decoded_char)
        Return result

    End Function


#Region "EMail Window and Top Panel Code"
    Dim allowResize As Boolean = True
    Dim posX As Integer
    Dim posY As Integer
    Dim drag As Boolean

    'resize
    Dim onFullScreen As Boolean
    Dim maximized As Boolean
    Dim on_MinimumSize As Boolean
    Dim minimumWidth As Short = 350
    Dim minimumHeight As Short = 26
    Dim borderSpace As Short = 20
    Dim borderDiameter As Short = 3

    Dim onBorderRight As Boolean
    Dim onBorderLeft As Boolean
    Dim onBorderTop As Boolean
    Dim onBorderBottom As Boolean
    Dim onCornerTopRight As Boolean
    Dim onCornerTopLeft As Boolean
    Dim onCornerBottomRight As Boolean
    Dim onCornerBottomLeft As Boolean

    Dim movingRight As Boolean
    Dim movingLeft As Boolean
    Dim movingTop As Boolean
    Dim movingBottom As Boolean
    Dim movingCornerTopRight As Boolean
    Dim movingCornerTopLeft As Boolean
    Dim movingCornerBottomRight As Boolean
    Dim movingCornerBottomLeft As Boolean

    Private Sub TopPanel_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles TopPanel.MouseDown
        If e.Button = MouseButtons.Left Then
            drag = True
            posX = Cursor.Position.X - Me.Left
            posY = Cursor.Position.Y - Me.Top
        End If
    End Sub

    Private Sub Top_Title_MouseDoubleClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Top_Title.MouseDoubleClick
        If allowResize Then
            If e.Button = MouseButtons.Left Then
                If maximized Then
                    Me.WindowState = FormWindowState.Normal
                    maximized = False
                Else
                    Me.WindowState = FormWindowState.Maximized
                    maximized = True
                End If
            End If
        Else
            If Me.TopMost Then
                Me.TopMost = False
            Else
                Me.TopMost = True
            End If
        End If
    End Sub

    Private Sub Top_Title_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Top_Title.MouseUp
        drag = False
    End Sub

    Private Sub Top_Title_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Top_Title.MouseDown
        If e.Button = MouseButtons.Left Then
            drag = True
            posX = Cursor.Position.X - Me.Left
            posY = Cursor.Position.Y - Me.Top
        End If
    End Sub

    Private Sub Top_Title_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Top_Title.MouseMove
        If drag Then
            Me.Top = Cursor.Position.Y - posY
            Me.Left = Cursor.Position.X - posX
        End If
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub Copyright_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Copyright.MouseDown
        If e.Button = MouseButtons.Left Then
            drag = True
            posX = Cursor.Position.X - Me.Left
            posY = Cursor.Position.Y - Me.Top
        End If
    End Sub

    Private Sub Copyright_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Copyright.MouseMove
        If drag Then
            Me.Top = Cursor.Position.Y - posY
            Me.Left = Cursor.Position.X - posX
        End If
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub Copyright_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Copyright.MouseUp
        drag = False
    End Sub

    Private Sub BottomPanel_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles BottomPanel.MouseDown
        If e.Button = MouseButtons.Left Then
            drag = True
            posX = Cursor.Position.X - Me.Left
            posY = Cursor.Position.Y - Me.Top
        End If
        If e.Button = Windows.Forms.MouseButtons.Left Then
            If onBorderRight Then movingRight = True Else movingRight = False
            If onBorderLeft Then movingLeft = True Else movingLeft = False
            If onBorderTop Then movingTop = True Else movingTop = False
            If onBorderBottom Then movingBottom = True Else movingBottom = False
            If onCornerTopRight Then movingCornerTopRight = True Else movingCornerTopRight = False
            If onCornerTopLeft Then movingCornerTopLeft = True Else movingCornerTopLeft = False
            If onCornerBottomRight Then movingCornerBottomRight = True Else movingCornerBottomRight = False
            If onCornerBottomLeft Then movingCornerBottomLeft = True Else movingCornerBottomLeft = False
        End If
    End Sub

    Private Sub BottomPanel_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles BottomPanel.MouseMove
        'start resize
        If Not (onFullScreen Or maximized Or Not allowResize) Then

            If Me.Width <= minimumWidth Then Me.Width = (minimumWidth + 5) : on_MinimumSize = True
            If Me.Height <= minimumHeight Then Me.Height = (minimumHeight + 5) : on_MinimumSize = True
            If on_MinimumSize Then stopResizer() Else startResizer()


            If (Cursor.Position.X > (Me.Location.X + Me.Width) - borderDiameter) _
                And (Cursor.Position.Y > (Me.Location.Y + borderSpace)) _
                And (Cursor.Position.Y < ((Me.Location.Y + Me.Height) - borderSpace)) Then
                Me.Cursor = Cursors.SizeWE
                onBorderRight = True

            ElseIf (Cursor.Position.X < (Me.Location.X + borderDiameter)) _
                And (Cursor.Position.Y > (Me.Location.Y + borderSpace)) _
                And (Cursor.Position.Y < ((Me.Location.Y + Me.Height) - borderSpace)) Then
                Me.Cursor = Cursors.SizeWE
                onBorderLeft = True

            ElseIf (Cursor.Position.Y < (Me.Location.Y + borderDiameter)) _
                And (Cursor.Position.X > (Me.Location.X + borderSpace)) _
                And (Cursor.Position.X < ((Me.Location.X + Me.Width) - borderSpace)) Then
                Me.Cursor = Cursors.SizeNS
                onBorderTop = True

            ElseIf (Cursor.Position.Y > ((Me.Location.Y + Me.Height) - borderDiameter)) _
                And (Cursor.Position.X > (Me.Location.X + borderSpace)) _
                And (Cursor.Position.X < ((Me.Location.X + Me.Width) - borderSpace)) Then
                Me.Cursor = Cursors.SizeNS
                onBorderBottom = True

            ElseIf (Cursor.Position.X = ((Me.Location.X + Me.Width) - 1)) _
                And (Cursor.Position.Y = Me.Location.Y) Then
                Me.Cursor = Cursors.SizeNESW
                onCornerTopRight = True

            ElseIf (Cursor.Position.X = Me.Location.X) _
                And (Cursor.Position.Y = Me.Location.Y) Then
                Me.Cursor = Cursors.SizeNWSE
                onCornerTopLeft = True

            ElseIf (Cursor.Position.X = ((Me.Location.X + Me.Width) - 1)) _
                And (Cursor.Position.Y = ((Me.Location.Y + Me.Height) - 1)) Then
                Me.Cursor = Cursors.SizeNWSE
                onCornerBottomRight = True

            ElseIf (Cursor.Position.X = Me.Location.X) _
                And (Cursor.Position.Y = ((Me.Location.Y + Me.Height) - 1)) Then
                Me.Cursor = Cursors.SizeNESW
                onCornerBottomLeft = True

            Else
                onBorderRight = False
                onBorderLeft = False
                onBorderTop = False
                onBorderBottom = False
                onCornerTopRight = False
                onCornerTopLeft = False
                onCornerBottomRight = False
                onCornerBottomLeft = False
                Me.Cursor = Cursors.Default
                If drag Then
                    Me.Top = Cursor.Position.Y - posY
                    Me.Left = Cursor.Position.X - posX
                End If

            End If

        Else
            Me.Cursor = Cursors.Default
            If drag Then
                Me.Top = Cursor.Position.Y - posY
                Me.Left = Cursor.Position.X - posX
            End If
            Me.Cursor = Cursors.Default
        End If

    End Sub

    Private Sub BottomPanel_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles BottomPanel.MouseUp
        drag = False
        stopResizer()
    End Sub

    Private Sub TopPanel_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TopPanel.MouseDoubleClick
        If allowResize Then
            If e.Button = MouseButtons.Left Then
                If maximized Then
                    Me.WindowState = FormWindowState.Normal
                    maximized = False
                Else
                    Me.WindowState = FormWindowState.Maximized
                    maximized = True
                End If
            End If
        Else
            If Me.TopMost Then
                Me.TopMost = False
            Else
                Me.TopMost = True
            End If
        End If
    End Sub

    Private Sub TopPanel_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles TopPanel.MouseMove
        If drag Then
            Me.Top = Cursor.Position.Y - posY
            Me.Left = Cursor.Position.X - posX
        End If
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub TopPanel_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles TopPanel.MouseUp
        drag = False
    End Sub

    Private Sub EMail_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseDown
        If e.Button = Windows.Forms.MouseButtons.Left Then
            If onBorderRight Then movingRight = True Else movingRight = False
            If onBorderLeft Then movingLeft = True Else movingLeft = False
            If onBorderTop Then movingTop = True Else movingTop = False
            If onBorderBottom Then movingBottom = True Else movingBottom = False
            If onCornerTopRight Then movingCornerTopRight = True Else movingCornerTopRight = False
            If onCornerTopLeft Then movingCornerTopLeft = True Else movingCornerTopLeft = False
            If onCornerBottomRight Then movingCornerBottomRight = True Else movingCornerBottomRight = False
            If onCornerBottomLeft Then movingCornerBottomLeft = True Else movingCornerBottomLeft = False
        End If
    End Sub

    Private Sub EMail_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseMove
        If onFullScreen Or maximized Or Not allowResize Then Exit Sub

        If Me.Width <= minimumWidth Then Me.Width = (minimumWidth + 5) : on_MinimumSize = True
        If Me.Height <= minimumHeight Then Me.Height = (minimumHeight + 5) : on_MinimumSize = True
        If on_MinimumSize Then stopResizer() Else startResizer()


        If (Cursor.Position.X > (Me.Location.X + Me.Width) - borderDiameter) _
            And (Cursor.Position.Y > (Me.Location.Y + borderSpace)) _
            And (Cursor.Position.Y < ((Me.Location.Y + Me.Height) - borderSpace)) Then
            Me.Cursor = Cursors.SizeWE
            onBorderRight = True

        ElseIf (Cursor.Position.X < (Me.Location.X + borderDiameter)) _
            And (Cursor.Position.Y > (Me.Location.Y + borderSpace)) _
            And (Cursor.Position.Y < ((Me.Location.Y + Me.Height) - borderSpace)) Then
            Me.Cursor = Cursors.SizeWE
            onBorderLeft = True

        ElseIf (Cursor.Position.Y < (Me.Location.Y + borderDiameter)) _
            And (Cursor.Position.X > (Me.Location.X + borderSpace)) _
            And (Cursor.Position.X < ((Me.Location.X + Me.Width) - borderSpace)) Then
            Me.Cursor = Cursors.SizeNS
            onBorderTop = True

        ElseIf (Cursor.Position.Y > ((Me.Location.Y + Me.Height) - borderDiameter)) _
            And (Cursor.Position.X > (Me.Location.X + borderSpace)) _
            And (Cursor.Position.X < ((Me.Location.X + Me.Width) - borderSpace)) Then
            Me.Cursor = Cursors.SizeNS
            onBorderBottom = True

        ElseIf (Cursor.Position.X = ((Me.Location.X + Me.Width) - 1)) _
            And (Cursor.Position.Y = Me.Location.Y) Then
            Me.Cursor = Cursors.SizeNESW
            onCornerTopRight = True

        ElseIf (Cursor.Position.X = Me.Location.X) _
            And (Cursor.Position.Y = Me.Location.Y) Then
            Me.Cursor = Cursors.SizeNWSE
            onCornerTopLeft = True

        ElseIf (Cursor.Position.X = ((Me.Location.X + Me.Width) - 1)) _
            And (Cursor.Position.Y = ((Me.Location.Y + Me.Height) - 1)) Then
            Me.Cursor = Cursors.SizeNWSE
            onCornerBottomRight = True

        ElseIf (Cursor.Position.X = Me.Location.X) _
            And (Cursor.Position.Y = ((Me.Location.Y + Me.Height) - 1)) Then
            Me.Cursor = Cursors.SizeNESW
            onCornerBottomLeft = True

        Else
            onBorderRight = False
            onBorderLeft = False
            onBorderTop = False
            onBorderBottom = False
            onCornerTopRight = False
            onCornerTopLeft = False
            onCornerBottomRight = False
            onCornerBottomLeft = False
            Me.Cursor = Cursors.Default
        End If
    End Sub

    Private Sub EMail_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp
        stopResizer()
    End Sub

    'functions
    Private Sub startResizer()
        Select Case True

            Case movingRight
                Me.Width = (Cursor.Position.X - Me.Location.X)

            Case movingLeft
                Me.Width = ((Me.Width + Me.Location.X) - Cursor.Position.X)
                Me.Location = New Point(Cursor.Position.X, Me.Location.Y)

            Case movingTop
                Me.Height = ((Me.Height + Me.Location.Y) - Cursor.Position.Y)
                Me.Location = New Point(Me.Location.X, Cursor.Position.Y)

            Case movingBottom
                Me.Height = (Cursor.Position.Y - Me.Location.Y)

            Case movingCornerTopRight
                Me.Width = (Cursor.Position.X - Me.Location.X)
                Me.Height = ((Me.Location.Y - Cursor.Position.Y) + Me.Height)
                Me.Location = New Point(Me.Location.X, Cursor.Position.Y)

            Case movingCornerTopLeft
                Me.Width = ((Me.Width + Me.Location.X) - Cursor.Position.X)
                Me.Location = New Point(Cursor.Position.X, Me.Location.Y)
                Me.Height = ((Me.Height + Me.Location.Y) - Cursor.Position.Y)
                Me.Location = New Point(Me.Location.X, Cursor.Position.Y)

            Case movingCornerBottomRight
                Me.Size = New Point((Cursor.Position.X - Me.Location.X), (Cursor.Position.Y - Me.Location.Y))

            Case movingCornerBottomLeft
                Me.Width = ((Me.Width + Me.Location.X) - Cursor.Position.X)
                Me.Height = (Cursor.Position.Y - Me.Location.Y)
                Me.Location = New Point(Cursor.Position.X, Me.Location.Y)

        End Select
    End Sub

    Private Sub stopResizer()
        movingRight = False
        movingLeft = False
        movingTop = False
        movingBottom = False
        movingCornerTopRight = False
        movingCornerTopLeft = False
        movingCornerBottomRight = False
        movingCornerBottomLeft = False
        Me.Cursor = Cursors.Default
        Threading.Thread.Sleep(300)
        on_MinimumSize = False
    End Sub
#End Region


    Private Sub Btn_File_Click(sender As System.Object, e As System.EventArgs) Handles Btn_File.Click
        With OpenFileDialogAttachment
            .Title = "Add Attachment"
            .Filter = "Attachment |*.*"
            .ShowDialog()
        End With
    End Sub

    Private Sub BackgroundWorkerSendMail_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorkerSendMail.DoWork
        TransferProgressBar.Visible = True
        Using mail As New MailMessage
            For i = TransferProgressBar.Minimum To (TransferProgressBar.Maximum / 2) + 25
                TransferProgressBar.Value = i
                TransferProgressBar.Update()
                System.Threading.Thread.Sleep(75)
            Next
            mail.From = New MailAddress(SNameTextBox.Text)
            mail.To.Add(destination.Text$)
            mail.Body = body.Text$
            If Not OpenFileDialogAttachment.FileName = vbNullString Then
                For Each attached In OpenFileDialogAttachment.FileNames
                    Dim attach As New Attachment(attached)
                    mail.Attachments.Add(attach)
                Next
            End If
            mail.Subject = subject.Text$
            mail.Priority = MailPriority.Normal
            Using SMTP As New SmtpClient
                SMTP.EnableSsl = True
                SMTP.Port = "587"
                SMTP.Host = "smtp.gmail.com"
                SMTP.Credentials = New Net.NetworkCredential(EmailTextBox.Text, PasswordTextBox.Text)
                SMTP.Send(mail)
            End Using
            For i = TransferProgressBar.Minimum + (TransferProgressBar.Maximum / 2) + 25 To TransferProgressBar.Maximum
                TransferProgressBar.Value = i
                TransferProgressBar.Update()
                System.Threading.Thread.Sleep(25)
            Next
        End Using
    End Sub

    Private Sub OpenFileDialogAttachment_Disposed(sender As Object, e As System.EventArgs) Handles OpenFileDialogAttachment.Disposed
        OpenFileDialogAttachment.FileName = vbNullString
        attach.Text = ""
    End Sub

    Private Sub OpenFileDialogAttachment_FileOk(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles OpenFileDialogAttachment.FileOk
        attach.Text = ""
        If Not OpenFileDialogAttachment.FileName = vbNullString Then
            For Each attached In OpenFileDialogAttachment.FileNames
                Dim fileInfo As New FileInfo(attached)
                If attach.Text = "" Then
                    attach.Text$ = fileInfo.Name
                Else
                    attach.Text$ = attach.Text & ", " & fileInfo.Name
                End If
            Next
        End If
    End Sub

    Private Sub BackgroundWorkerSendMail_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorkerSendMail.ProgressChanged
        TransferProgressBar.Value = e.ProgressPercentage
    End Sub

    Private Sub BackgroundWorkerSendMail_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorkerSendMail.RunWorkerCompleted
        TransferProgressBar.Visible = False
        destination.Enabled = True
        subject.Enabled = True
        attach.Enabled = True
        Btn_File.Enabled = True
        body.Enabled = True
        Btn_Exit.Enabled = True
        Btn_Reset.Enabled = True
        Btn_Send.Enabled = True
        destination.Clear()
        subject.Clear()
        attach.Clear()
        body.Text = vbNewLine & vbNewLine & "--" & Main.userName
        MessageBox.Show("Email Sent", "Sent", MessageBoxButtons.OK, MessageBoxIcon.Information)
        destination.Focus()
    End Sub

    Private Sub Btn_Reset_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Reset.Click
        destination.Clear()
        subject.Clear()
        attach.Clear()
        body.Text = vbNewLine & vbNewLine & "--" & Main.userName
        destination.Focus()
    End Sub

    Private Sub Btn_Send_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Send.Click
        If destination.Text = "" Then
            mailError = "Please enter a recipient."
            destination.Focus()
        ElseIf subject.Text = "" Then
            mailError = "Please enter a subject."
            subject.Focus()
        Else
            mailError = ""
        End If
        If mailError = "" Then
            TransferProgressBar.Visible = True
            BackgroundWorkerSendMail.RunWorkerAsync()
            destination.Enabled = False
            subject.Enabled = False
            attach.Enabled = False
            Btn_File.Enabled = False
            body.Enabled = False
            Btn_Exit.Enabled = False
            Btn_Reset.Enabled = False
            Btn_Send.Enabled = False
        Else
            MessageBox.Show(mailError, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub EMail_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        AllowedEnter = False
        Timer1.Start()
        Try

            'TODO: This line of code loads data into the '_IW_JobOrdersDataSet.Email' table. You can move, or remove it, as needed.
            Me.EmailTableAdapter.Fill(Me._IW_JobOrdersDataSet.Email)

        Catch ex As Exception
            MessageBox.Show("Database error, refer to Developer.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Close()
        End Try
        AllowedEnter = False
        PasswordTextBox.Text = base64Decode(PasswordTextBox.Text)
        allowResize = False
        If Main.Mode = "Administrator" Then
            Pic.Visible = True
            Label5.Visible = True
            RectangleShape1.Visible = True
            EmailTextBox.Size = New Size(150, 18)
            SNameTextBox.Size = New Size(190, 18)
            PasswordTextBox.Size = New Size(150, 18)
        Else
            Pic.Visible = False
            Label5.Text = ""
            RectangleShape1.Visible = False
            EmailTextBox.Size = New Size(0, 18)
            PasswordTextBox.Size = New Size(0, 18)
            SNameTextBox.Size = New Size(0, 18)
        End If

        body.Text = vbNewLine & vbNewLine & "--" & Main.userName
    End Sub

    Private Sub body_GotFocus(sender As Object, e As System.EventArgs) Handles body.GotFocus
        AllowedEnter = True
    End Sub

    Private Sub body_LostFocus(sender As Object, e As System.EventArgs) Handles body.LostFocus
        AllowedEnter = False
    End Sub

    Private Sub Btn_Exit_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Exit.Click
        If Main.Mode = "Administrator" Then
            Try
                Dim saveE As Integer = MessageBox.Show("Save Changes?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                If saveE = DialogResult.Yes Then
                    PasswordTextBox.Text = base64Encode(PasswordTextBox.Text)
                    EmailBindingSource.EndEdit()
                    EmailTableAdapter.Update(_IW_JobOrdersDataSet.Email)
                    MessageBox.Show("Data Saved!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            Catch ex As Exception
                MessageBox.Show("Error: Config not saved!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End If
        Close()
    End Sub

    Private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick
        If Main.isRound Then
            Dim p As New Drawing2D.GraphicsPath()
            p.StartFigure()
            p.AddArc(New Rectangle(0, 0, 20, 20), 180, 90)
            p.AddLine(20, 0, Me.Width - 20, 0)
            p.AddArc(New Rectangle(Me.Width - 20, 0, 20, 20), -90, 90)
            p.AddLine(Me.Width, 20, Me.Width, Me.Height - 20)
            p.AddArc(New Rectangle(Me.Width - 20, Me.Height - 20, 20, 20), 0, 90)
            p.AddLine(Me.Width - 20, Me.Height, 20, Me.Height)
            p.AddArc(New Rectangle(0, Me.Height - 20, 20, 20), 90, 90)
            p.CloseFigure()
            Me.Region = New Region(p)
        Else
            Me.Region = Nothing
        End If
    End Sub

    Private Sub SNameTextBox_MouseHover(sender As Object, e As System.EventArgs) Handles SNameTextBox.MouseHover
        CustomToolTip.SetToolTip(SNameTextBox, "Display Email")
    End Sub

    Private Sub EmailTextBox_MouseHover(sender As Object, e As System.EventArgs) Handles EmailTextBox.MouseHover
        CustomToolTip.SetToolTip(EmailTextBox, "Email")
    End Sub

    Private Sub PasswordTextBox_MouseHover(sender As Object, e As System.EventArgs) Handles PasswordTextBox.MouseHover
        CustomToolTip.SetToolTip(PasswordTextBox, "Password")
    End Sub

    Private Sub attach_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles attach.KeyDown
        If e.KeyCode = Keys.Back Then
            OpenFileDialogAttachment.FileName = vbNullString
            attach.Text = ""
        End If
    End Sub

    Private Sub destination_MouseHover(sender As Object, e As System.EventArgs) Handles destination.MouseHover
        CustomToolTip.SetToolTip(destination, "Recipient")
    End Sub

    Private Sub subject_MouseHover(sender As Object, e As System.EventArgs) Handles subject.MouseHover
        CustomToolTip.SetToolTip(subject, "Subject")
    End Sub

    Private Sub attach_MouseHover(sender As Object, e As System.EventArgs) Handles attach.MouseHover
        CustomToolTip.SetToolTip(attach, "Attachments (25mb | max)")
    End Sub

    Private Sub body_MouseHover(sender As Object, e As System.EventArgs) Handles body.MouseHover
        CustomToolTip.SetToolTip(body, "Message Body")
    End Sub

End Class