﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Message
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Message))
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.CheckScroll = New System.Windows.Forms.CheckBox()
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.Aloading = New System.Windows.Forms.PictureBox()
        Me.BottomPanel = New System.Windows.Forms.Panel()
        Me.TimeLabel = New System.Windows.Forms.Label()
        Me.BackgroundWorkerSendMail = New System.ComponentModel.BackgroundWorker()
        Me.Chatarea = New System.Windows.Forms.WebBrowser()
        Me.Timer3 = New System.Windows.Forms.Timer(Me.components)
        Me.TopPanel = New System.Windows.Forms.Panel()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Top_Title = New System.Windows.Forms.Label()
        Me.Btn_Exit = New System.Windows.Forms.PictureBox()
        Me.TimeBrowser = New System.Windows.Forms.WebBrowser()
        CType(Me.Aloading, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BottomPanel.SuspendLayout()
        Me.TopPanel.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Btn_Exit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Timer1
        '
        '
        'CheckScroll
        '
        Me.CheckScroll.AutoSize = True
        Me.CheckScroll.BackColor = System.Drawing.Color.Silver
        Me.CheckScroll.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CheckScroll.FlatAppearance.BorderSize = 0
        Me.CheckScroll.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckScroll.Location = New System.Drawing.Point(384, 26)
        Me.CheckScroll.Name = "CheckScroll"
        Me.CheckScroll.Size = New System.Drawing.Size(12, 11)
        Me.CheckScroll.TabIndex = 62
        Me.CheckScroll.UseVisualStyleBackColor = False
        Me.CheckScroll.Visible = False
        '
        'Aloading
        '
        Me.Aloading.BackColor = System.Drawing.Color.Black
        Me.Aloading.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.Aloading.ErrorImage = Nothing
        Me.Aloading.Image = CType(resources.GetObject("Aloading.Image"), System.Drawing.Image)
        Me.Aloading.InitialImage = Nothing
        Me.Aloading.Location = New System.Drawing.Point(294, 359)
        Me.Aloading.Name = "Aloading"
        Me.Aloading.Size = New System.Drawing.Size(104, 100)
        Me.Aloading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.Aloading.TabIndex = 65
        Me.Aloading.TabStop = False
        '
        'BottomPanel
        '
        Me.BottomPanel.BackColor = System.Drawing.Color.Black
        Me.BottomPanel.Controls.Add(Me.TimeLabel)
        Me.BottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BottomPanel.Location = New System.Drawing.Point(0, 490)
        Me.BottomPanel.Name = "BottomPanel"
        Me.BottomPanel.Size = New System.Drawing.Size(400, 10)
        Me.BottomPanel.TabIndex = 66
        '
        'TimeLabel
        '
        Me.TimeLabel.AutoSize = True
        Me.TimeLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TimeLabel.ForeColor = System.Drawing.Color.Silver
        Me.TimeLabel.Location = New System.Drawing.Point(151, -2)
        Me.TimeLabel.Name = "TimeLabel"
        Me.TimeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TimeLabel.Size = New System.Drawing.Size(100, 12)
        Me.TimeLabel.TabIndex = 63
        Me.TimeLabel.Text = "Philippine Time is 00:00"
        Me.TimeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Chatarea
        '
        Me.Chatarea.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.Chatarea.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Chatarea.Location = New System.Drawing.Point(0, 25)
        Me.Chatarea.MinimumSize = New System.Drawing.Size(20, 20)
        Me.Chatarea.Name = "Chatarea"
        Me.Chatarea.ScriptErrorsSuppressed = True
        Me.Chatarea.ScrollBarsEnabled = False
        Me.Chatarea.Size = New System.Drawing.Size(400, 475)
        Me.Chatarea.TabIndex = 63
        Me.Chatarea.Url = New System.Uri("https://www.messenger.com/", System.UriKind.Absolute)
        '
        'TopPanel
        '
        Me.TopPanel.BackColor = System.Drawing.Color.Black
        Me.TopPanel.Controls.Add(Me.PictureBox2)
        Me.TopPanel.Controls.Add(Me.PictureBox1)
        Me.TopPanel.Controls.Add(Me.Top_Title)
        Me.TopPanel.Controls.Add(Me.Btn_Exit)
        Me.TopPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me.TopPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopPanel.Name = "TopPanel"
        Me.TopPanel.Size = New System.Drawing.Size(400, 25)
        Me.TopPanel.TabIndex = 64
        '
        'PictureBox2
        '
        Me.PictureBox2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox2.ErrorImage = Nothing
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.InitialImage = CType(resources.GetObject("PictureBox2.InitialImage"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(383, 7)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(12, 12)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 54
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox1.ErrorImage = Nothing
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.InitialImage = CType(resources.GetObject("PictureBox1.InitialImage"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(383, 7)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(12, 12)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 52
        Me.PictureBox1.TabStop = False
        Me.PictureBox1.Visible = False
        '
        'Top_Title
        '
        Me.Top_Title.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.Top_Title.BackColor = System.Drawing.Color.Black
        Me.Top_Title.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.Top_Title.Location = New System.Drawing.Point(73, 7)
        Me.Top_Title.Name = "Top_Title"
        Me.Top_Title.Size = New System.Drawing.Size(255, 15)
        Me.Top_Title.TabIndex = 51
        Me.Top_Title.Text = "Messenger"
        Me.Top_Title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Top_Title.Visible = False
        '
        'Btn_Exit
        '
        Me.Btn_Exit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Exit.ErrorImage = Nothing
        Me.Btn_Exit.Image = CType(resources.GetObject("Btn_Exit.Image"), System.Drawing.Image)
        Me.Btn_Exit.InitialImage = CType(resources.GetObject("Btn_Exit.InitialImage"), System.Drawing.Image)
        Me.Btn_Exit.Location = New System.Drawing.Point(1, 5)
        Me.Btn_Exit.Name = "Btn_Exit"
        Me.Btn_Exit.Size = New System.Drawing.Size(17, 17)
        Me.Btn_Exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Exit.TabIndex = 48
        Me.Btn_Exit.TabStop = False
        Me.Btn_Exit.Visible = False
        '
        'TimeBrowser
        '
        Me.TimeBrowser.Location = New System.Drawing.Point(378, 329)
        Me.TimeBrowser.MinimumSize = New System.Drawing.Size(20, 20)
        Me.TimeBrowser.Name = "TimeBrowser"
        Me.TimeBrowser.ScriptErrorsSuppressed = True
        Me.TimeBrowser.ScrollBarsEnabled = False
        Me.TimeBrowser.Size = New System.Drawing.Size(20, 23)
        Me.TimeBrowser.TabIndex = 67
        Me.TimeBrowser.Url = New System.Uri("http://www.time.is/ph", System.UriKind.Absolute)
        Me.TimeBrowser.Visible = False
        Me.TimeBrowser.WebBrowserShortcutsEnabled = False
        '
        'Message
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(400, 500)
        Me.Controls.Add(Me.CheckScroll)
        Me.Controls.Add(Me.Aloading)
        Me.Controls.Add(Me.BottomPanel)
        Me.Controls.Add(Me.Chatarea)
        Me.Controls.Add(Me.TopPanel)
        Me.Controls.Add(Me.TimeBrowser)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Message"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Messenger"
        CType(Me.Aloading, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BottomPanel.ResumeLayout(False)
        Me.BottomPanel.PerformLayout()
        Me.TopPanel.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Btn_Exit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents CheckScroll As System.Windows.Forms.CheckBox
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents Aloading As System.Windows.Forms.PictureBox
    Friend WithEvents BottomPanel As System.Windows.Forms.Panel
    Friend WithEvents TimeLabel As System.Windows.Forms.Label
    Friend WithEvents BackgroundWorkerSendMail As System.ComponentModel.BackgroundWorker
    Friend WithEvents Chatarea As System.Windows.Forms.WebBrowser
    Friend WithEvents Timer3 As System.Windows.Forms.Timer
    Friend WithEvents TopPanel As System.Windows.Forms.Panel
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Top_Title As System.Windows.Forms.Label
    Friend WithEvents Btn_Exit As System.Windows.Forms.PictureBox
    Friend WithEvents TimeBrowser As System.Windows.Forms.WebBrowser
End Class
