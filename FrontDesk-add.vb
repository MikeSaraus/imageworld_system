﻿Public Class FrontDeskadd
    Dim posX As Integer
    Dim posY As Integer
    Dim drag As Boolean

    'resize
    Dim onFullScreen As Boolean
    Dim maximized As Boolean
    Dim on_MinimumSize As Boolean
    Dim minimumWidth As Short = 350
    Dim minimumHeight As Short = 26
    Dim borderSpace As Short = 20
    Dim borderDiameter As Short = 3

    Dim onBorderRight As Boolean
    Dim onBorderLeft As Boolean
    Dim onBorderTop As Boolean
    Dim onBorderBottom As Boolean
    Dim onCornerTopRight As Boolean
    Dim onCornerTopLeft As Boolean
    Dim onCornerBottomRight As Boolean
    Dim onCornerBottomLeft As Boolean

    Dim movingRight As Boolean
    Dim movingLeft As Boolean
    Dim movingTop As Boolean
    Dim movingBottom As Boolean
    Dim movingCornerTopRight As Boolean
    Dim movingCornerTopLeft As Boolean
    Dim movingCornerBottomRight As Boolean
    Dim movingCornerBottomLeft As Boolean

    Dim LeavingX As Boolean = False
    Dim allowResize As Boolean = True
    Private MouseDownX As Integer
    Private MouseDownY As Integer

#Region "Disable Exit Ways"
    Protected Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            Const CS_NOCLOSE As Integer = &H200
            cp.ClassStyle = cp.ClassStyle Or CS_NOCLOSE
            Return cp
        End Get
    End Property
#End Region

    Private Sub FrontDeskadd_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Try
            Me.JobOrdersTableAdapter.Fill(Me._IW_JobOrdersDataSet.JobOrders)
        Catch ex As Exception
            MessageBox.Show("Database Error: Maybe Open or Not Accessible.", "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        allowResize = False
        JobOrdersBindingSource.AddNew()
        'IDLabel.Text = Total_JO + 1
        JONumberTextBox.Focus()
        DateRecievedDateTimePicker.Value = DateTime.Now
        DateRequiredDateTimePicker.Value = DateTime.Now
        'OutDateDateTimePicker.Value = DateTime.Now
    End Sub


    Private Sub TopPanel_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles TopPanel.MouseDown
        If e.Button = MouseButtons.Left Then
            drag = True
            posX = Cursor.Position.X - Me.Left
            posY = Cursor.Position.Y - Me.Top
        End If
    End Sub

    Private Sub Top_Title_MouseDoubleClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Top_Title.MouseDoubleClick
        If allowResize Then
            If e.Button = MouseButtons.Left Then
                If maximized Then
                    Me.WindowState = FormWindowState.Normal
                    maximized = False
                Else
                    Me.WindowState = FormWindowState.Maximized
                    maximized = True
                End If
            End If
        Else
            If Me.TopMost Then
                Me.TopMost = False
            Else
                Me.TopMost = True
            End If
        End If
    End Sub

    Private Sub Top_Title_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Top_Title.MouseUp
        drag = False
    End Sub

    Private Sub Top_Title_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Top_Title.MouseDown
        If e.Button = MouseButtons.Left Then
            drag = True
            posX = Cursor.Position.X - Me.Left
            posY = Cursor.Position.Y - Me.Top
        End If
    End Sub

    Private Sub Top_Title_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Top_Title.MouseMove
        If drag Then
            Me.Top = Cursor.Position.Y - posY
            Me.Left = Cursor.Position.X - posX
        End If
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub Copyright_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Copyright.MouseDown
        If e.Button = MouseButtons.Left Then
            drag = True
            posX = Cursor.Position.X - Me.Left
            posY = Cursor.Position.Y - Me.Top
        End If
    End Sub

    Private Sub Copyright_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Copyright.MouseMove
        If drag Then
            Me.Top = Cursor.Position.Y - posY
            Me.Left = Cursor.Position.X - posX
        End If
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub Copyright_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Copyright.MouseUp
        drag = False
    End Sub

    Private Sub BottomPanel_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles BottomPanel.MouseDown
        If e.Button = MouseButtons.Left Then
            drag = True
            posX = Cursor.Position.X - Me.Left
            posY = Cursor.Position.Y - Me.Top
        End If
        If e.Button = Windows.Forms.MouseButtons.Left Then
            If onBorderRight Then movingRight = True Else movingRight = False
            If onBorderLeft Then movingLeft = True Else movingLeft = False
            If onBorderTop Then movingTop = True Else movingTop = False
            If onBorderBottom Then movingBottom = True Else movingBottom = False
            If onCornerTopRight Then movingCornerTopRight = True Else movingCornerTopRight = False
            If onCornerTopLeft Then movingCornerTopLeft = True Else movingCornerTopLeft = False
            If onCornerBottomRight Then movingCornerBottomRight = True Else movingCornerBottomRight = False
            If onCornerBottomLeft Then movingCornerBottomLeft = True Else movingCornerBottomLeft = False
        End If
    End Sub

    Private Sub BottomPanel_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles BottomPanel.MouseMove
        'start resize
        If Not (onFullScreen Or maximized Or Not allowResize) Then

            If Me.Width <= minimumWidth Then Me.Width = (minimumWidth + 5) : on_MinimumSize = True
            If Me.Height <= minimumHeight Then Me.Height = (minimumHeight + 5) : on_MinimumSize = True
            If on_MinimumSize Then stopResizer() Else startResizer()


            If (Cursor.Position.X > (Me.Location.X + Me.Width) - borderDiameter) _
                And (Cursor.Position.Y > (Me.Location.Y + borderSpace)) _
                And (Cursor.Position.Y < ((Me.Location.Y + Me.Height) - borderSpace)) Then
                Me.Cursor = Cursors.SizeWE
                onBorderRight = True

            ElseIf (Cursor.Position.X < (Me.Location.X + borderDiameter)) _
                And (Cursor.Position.Y > (Me.Location.Y + borderSpace)) _
                And (Cursor.Position.Y < ((Me.Location.Y + Me.Height) - borderSpace)) Then
                Me.Cursor = Cursors.SizeWE
                onBorderLeft = True

            ElseIf (Cursor.Position.Y < (Me.Location.Y + borderDiameter)) _
                And (Cursor.Position.X > (Me.Location.X + borderSpace)) _
                And (Cursor.Position.X < ((Me.Location.X + Me.Width) - borderSpace)) Then
                Me.Cursor = Cursors.SizeNS
                onBorderTop = True

            ElseIf (Cursor.Position.Y > ((Me.Location.Y + Me.Height) - borderDiameter)) _
                And (Cursor.Position.X > (Me.Location.X + borderSpace)) _
                And (Cursor.Position.X < ((Me.Location.X + Me.Width) - borderSpace)) Then
                Me.Cursor = Cursors.SizeNS
                onBorderBottom = True

            ElseIf (Cursor.Position.X = ((Me.Location.X + Me.Width) - 1)) _
                And (Cursor.Position.Y = Me.Location.Y) Then
                Me.Cursor = Cursors.SizeNESW
                onCornerTopRight = True

            ElseIf (Cursor.Position.X = Me.Location.X) _
                And (Cursor.Position.Y = Me.Location.Y) Then
                Me.Cursor = Cursors.SizeNWSE
                onCornerTopLeft = True

            ElseIf (Cursor.Position.X = ((Me.Location.X + Me.Width) - 1)) _
                And (Cursor.Position.Y = ((Me.Location.Y + Me.Height) - 1)) Then
                Me.Cursor = Cursors.SizeNWSE
                onCornerBottomRight = True

            ElseIf (Cursor.Position.X = Me.Location.X) _
                And (Cursor.Position.Y = ((Me.Location.Y + Me.Height) - 1)) Then
                Me.Cursor = Cursors.SizeNESW
                onCornerBottomLeft = True

            Else
                onBorderRight = False
                onBorderLeft = False
                onBorderTop = False
                onBorderBottom = False
                onCornerTopRight = False
                onCornerTopLeft = False
                onCornerBottomRight = False
                onCornerBottomLeft = False
                Me.Cursor = Cursors.Default
                If drag Then
                    Me.Top = Cursor.Position.Y - posY
                    Me.Left = Cursor.Position.X - posX
                End If

            End If

        Else
            Me.Cursor = Cursors.Default
            If drag Then
                Me.Top = Cursor.Position.Y - posY
                Me.Left = Cursor.Position.X - posX
            End If
            Me.Cursor = Cursors.Default
        End If

    End Sub

    Private Sub BottomPanel_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles BottomPanel.MouseUp
        drag = False
        stopResizer()
    End Sub

    Private Sub TopPanel_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TopPanel.MouseDoubleClick
        If allowResize Then
            If e.Button = MouseButtons.Left Then
                If maximized Then
                    Me.WindowState = FormWindowState.Normal
                    maximized = False
                Else
                    Me.WindowState = FormWindowState.Maximized
                    maximized = True
                End If
            End If
        Else
            If Me.TopMost Then
                Me.TopMost = False
            Else
                Me.TopMost = True
            End If
        End If
    End Sub

    Private Sub TopPanel_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles TopPanel.MouseMove
        If drag Then
            Me.Top = Cursor.Position.Y - posY
            Me.Left = Cursor.Position.X - posX
        End If
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub TopPanel_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles TopPanel.MouseUp
        drag = False
    End Sub

    Private Sub Main_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseDown
        If e.Button = Windows.Forms.MouseButtons.Left Then
            If onBorderRight Then movingRight = True Else movingRight = False
            If onBorderLeft Then movingLeft = True Else movingLeft = False
            If onBorderTop Then movingTop = True Else movingTop = False
            If onBorderBottom Then movingBottom = True Else movingBottom = False
            If onCornerTopRight Then movingCornerTopRight = True Else movingCornerTopRight = False
            If onCornerTopLeft Then movingCornerTopLeft = True Else movingCornerTopLeft = False
            If onCornerBottomRight Then movingCornerBottomRight = True Else movingCornerBottomRight = False
            If onCornerBottomLeft Then movingCornerBottomLeft = True Else movingCornerBottomLeft = False
        End If
    End Sub

    Private Sub Main_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseMove
        If onFullScreen Or maximized Or Not allowResize Then Exit Sub

        If Me.Width <= minimumWidth Then Me.Width = (minimumWidth + 5) : on_MinimumSize = True
        If Me.Height <= minimumHeight Then Me.Height = (minimumHeight + 5) : on_MinimumSize = True
        If on_MinimumSize Then stopResizer() Else startResizer()


        If (Cursor.Position.X > (Me.Location.X + Me.Width) - borderDiameter) _
            And (Cursor.Position.Y > (Me.Location.Y + borderSpace)) _
            And (Cursor.Position.Y < ((Me.Location.Y + Me.Height) - borderSpace)) Then
            Me.Cursor = Cursors.SizeWE
            onBorderRight = True

        ElseIf (Cursor.Position.X < (Me.Location.X + borderDiameter)) _
            And (Cursor.Position.Y > (Me.Location.Y + borderSpace)) _
            And (Cursor.Position.Y < ((Me.Location.Y + Me.Height) - borderSpace)) Then
            Me.Cursor = Cursors.SizeWE
            onBorderLeft = True

        ElseIf (Cursor.Position.Y < (Me.Location.Y + borderDiameter)) _
            And (Cursor.Position.X > (Me.Location.X + borderSpace)) _
            And (Cursor.Position.X < ((Me.Location.X + Me.Width) - borderSpace)) Then
            Me.Cursor = Cursors.SizeNS
            onBorderTop = True

        ElseIf (Cursor.Position.Y > ((Me.Location.Y + Me.Height) - borderDiameter)) _
            And (Cursor.Position.X > (Me.Location.X + borderSpace)) _
            And (Cursor.Position.X < ((Me.Location.X + Me.Width) - borderSpace)) Then
            Me.Cursor = Cursors.SizeNS
            onBorderBottom = True

        ElseIf (Cursor.Position.X = ((Me.Location.X + Me.Width) - 1)) _
            And (Cursor.Position.Y = Me.Location.Y) Then
            Me.Cursor = Cursors.SizeNESW
            onCornerTopRight = True

        ElseIf (Cursor.Position.X = Me.Location.X) _
            And (Cursor.Position.Y = Me.Location.Y) Then
            Me.Cursor = Cursors.SizeNWSE
            onCornerTopLeft = True

        ElseIf (Cursor.Position.X = ((Me.Location.X + Me.Width) - 1)) _
            And (Cursor.Position.Y = ((Me.Location.Y + Me.Height) - 1)) Then
            Me.Cursor = Cursors.SizeNWSE
            onCornerBottomRight = True

        ElseIf (Cursor.Position.X = Me.Location.X) _
            And (Cursor.Position.Y = ((Me.Location.Y + Me.Height) - 1)) Then
            Me.Cursor = Cursors.SizeNESW
            onCornerBottomLeft = True

        Else
            onBorderRight = False
            onBorderLeft = False
            onBorderTop = False
            onBorderBottom = False
            onCornerTopRight = False
            onCornerTopLeft = False
            onCornerBottomRight = False
            onCornerBottomLeft = False
            Me.Cursor = Cursors.Default
        End If
    End Sub

    Private Sub Main_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseUp
        stopResizer()
    End Sub

    'functions
    Private Sub startResizer()
        Select Case True

            Case movingRight
                Me.Width = (Cursor.Position.X - Me.Location.X)

            Case movingLeft
                Me.Width = ((Me.Width + Me.Location.X) - Cursor.Position.X)
                Me.Location = New Point(Cursor.Position.X, Me.Location.Y)

            Case movingTop
                Me.Height = ((Me.Height + Me.Location.Y) - Cursor.Position.Y)
                Me.Location = New Point(Me.Location.X, Cursor.Position.Y)

            Case movingBottom
                Me.Height = (Cursor.Position.Y - Me.Location.Y)

            Case movingCornerTopRight
                Me.Width = (Cursor.Position.X - Me.Location.X)
                Me.Height = ((Me.Location.Y - Cursor.Position.Y) + Me.Height)
                Me.Location = New Point(Me.Location.X, Cursor.Position.Y)

            Case movingCornerTopLeft
                Me.Width = ((Me.Width + Me.Location.X) - Cursor.Position.X)
                Me.Location = New Point(Cursor.Position.X, Me.Location.Y)
                Me.Height = ((Me.Height + Me.Location.Y) - Cursor.Position.Y)
                Me.Location = New Point(Me.Location.X, Cursor.Position.Y)

            Case movingCornerBottomRight
                Me.Size = New Point((Cursor.Position.X - Me.Location.X), (Cursor.Position.Y - Me.Location.Y))

            Case movingCornerBottomLeft
                Me.Width = ((Me.Width + Me.Location.X) - Cursor.Position.X)
                Me.Height = (Cursor.Position.Y - Me.Location.Y)
                Me.Location = New Point(Cursor.Position.X, Me.Location.Y)

        End Select
    End Sub

    Private Sub stopResizer()
        movingRight = False
        movingLeft = False
        movingTop = False
        movingBottom = False
        movingCornerTopRight = False
        movingCornerTopLeft = False
        movingCornerBottomRight = False
        movingCornerBottomLeft = False
        Me.Cursor = Cursors.Default
        Threading.Thread.Sleep(300)
        on_MinimumSize = False
    End Sub


    Private Sub Btn_Exit_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Exit.Click
        Close()
    End Sub

    Private Sub Btn_Save_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Save.Click
        Try
            'Jobings Table
            JobOrdersBindingSource.EndEdit()
            JobOrdersTableAdapter.Update(_IW_JobOrdersDataSet.JobOrders)
            MessageBox.Show("Data Saved!", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show("Error occured while saving!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Close()
        End Try
    End Sub

    Private Sub Btn_Clear_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Clear.Click
        JONumberTextBox.Clear()
        CustomerTextBox.Clear()
        TypeTextBox.Clear()
        QuantityTextBox.Clear()
        ColorsComboBox.Text = ""
        InstructionsTextBox.Clear()
        OutputComboBox.Text = ""
    End Sub
End Class