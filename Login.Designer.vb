﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Login
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Login))
        Me.TopPanel = New System.Windows.Forms.Panel()
        Me.Top_Title = New System.Windows.Forms.Label()
        Me.Btn_Minimize = New System.Windows.Forms.PictureBox()
        Me.Btn_Maximize = New System.Windows.Forms.PictureBox()
        Me.Btn_Exit = New System.Windows.Forms.PictureBox()
        Me.BottomPanel = New System.Windows.Forms.Panel()
        Me.Copyright = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Txt_name = New System.Windows.Forms.TextBox()
        Me.Txt_Pass = New System.Windows.Forms.TextBox()
        Me.Pic_Logo = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.UsersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me._IW_JobOrdersDataSet = New $safeprojectname$._IW_JobOrdersDataSet()
        Me.UsersTableAdapter = New $safeprojectname$._IW_JobOrdersDataSetTableAdapters.UsersTableAdapter()
        Me.TableAdapterManager = New $safeprojectname$._IW_JobOrdersDataSetTableAdapters.TableAdapterManager()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.FullNameTextBox = New System.Windows.Forms.TextBox()
        Me.PasswordTextBox = New System.Windows.Forms.TextBox()
        Me.ModeTextBox = New System.Windows.Forms.TextBox()
        Me.IDTextBox = New System.Windows.Forms.TextBox()
        Me.UsernameTextBox = New System.Windows.Forms.TextBox()
        Me.JOcountTextBox = New System.Windows.Forms.TextBox()
        Me.TopPanel.SuspendLayout()
        CType(Me.Btn_Minimize, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Btn_Maximize, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Btn_Exit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BottomPanel.SuspendLayout()
        CType(Me.Pic_Logo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._IW_JobOrdersDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TopPanel
        '
        Me.TopPanel.BackColor = System.Drawing.Color.Black
        Me.TopPanel.Controls.Add(Me.Top_Title)
        Me.TopPanel.Controls.Add(Me.Btn_Minimize)
        Me.TopPanel.Controls.Add(Me.Btn_Maximize)
        Me.TopPanel.Controls.Add(Me.Btn_Exit)
        Me.TopPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me.TopPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopPanel.Name = "TopPanel"
        Me.TopPanel.Size = New System.Drawing.Size(459, 25)
        Me.TopPanel.TabIndex = 48
        '
        'Top_Title
        '
        Me.Top_Title.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.Top_Title.AutoSize = True
        Me.Top_Title.BackColor = System.Drawing.Color.Black
        Me.Top_Title.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.Top_Title.Location = New System.Drawing.Point(175, 7)
        Me.Top_Title.Name = "Top_Title"
        Me.Top_Title.Size = New System.Drawing.Size(127, 13)
        Me.Top_Title.TabIndex = 51
        Me.Top_Title.Text = "Welcome to Image World"
        Me.Top_Title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Btn_Minimize
        '
        Me.Btn_Minimize.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Minimize.ErrorImage = Nothing
        Me.Btn_Minimize.Image = CType(resources.GetObject("Btn_Minimize.Image"), System.Drawing.Image)
        Me.Btn_Minimize.InitialImage = CType(resources.GetObject("Btn_Minimize.InitialImage"), System.Drawing.Image)
        Me.Btn_Minimize.Location = New System.Drawing.Point(49, 3)
        Me.Btn_Minimize.Name = "Btn_Minimize"
        Me.Btn_Minimize.Size = New System.Drawing.Size(20, 20)
        Me.Btn_Minimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Minimize.TabIndex = 50
        Me.Btn_Minimize.TabStop = False
        Me.Btn_Minimize.Visible = False
        '
        'Btn_Maximize
        '
        Me.Btn_Maximize.BackColor = System.Drawing.Color.Transparent
        Me.Btn_Maximize.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Maximize.ErrorImage = Nothing
        Me.Btn_Maximize.Image = CType(resources.GetObject("Btn_Maximize.Image"), System.Drawing.Image)
        Me.Btn_Maximize.InitialImage = CType(resources.GetObject("Btn_Maximize.InitialImage"), System.Drawing.Image)
        Me.Btn_Maximize.Location = New System.Drawing.Point(29, 3)
        Me.Btn_Maximize.Name = "Btn_Maximize"
        Me.Btn_Maximize.Size = New System.Drawing.Size(20, 20)
        Me.Btn_Maximize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Maximize.TabIndex = 49
        Me.Btn_Maximize.TabStop = False
        Me.Btn_Maximize.Visible = False
        '
        'Btn_Exit
        '
        Me.Btn_Exit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Exit.ErrorImage = Nothing
        Me.Btn_Exit.Image = CType(resources.GetObject("Btn_Exit.Image"), System.Drawing.Image)
        Me.Btn_Exit.InitialImage = CType(resources.GetObject("Btn_Exit.InitialImage"), System.Drawing.Image)
        Me.Btn_Exit.Location = New System.Drawing.Point(9, 3)
        Me.Btn_Exit.Name = "Btn_Exit"
        Me.Btn_Exit.Size = New System.Drawing.Size(20, 20)
        Me.Btn_Exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Exit.TabIndex = 48
        Me.Btn_Exit.TabStop = False
        '
        'BottomPanel
        '
        Me.BottomPanel.BackColor = System.Drawing.Color.Black
        Me.BottomPanel.Controls.Add(Me.Copyright)
        Me.BottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BottomPanel.Location = New System.Drawing.Point(0, 104)
        Me.BottomPanel.Name = "BottomPanel"
        Me.BottomPanel.Size = New System.Drawing.Size(459, 20)
        Me.BottomPanel.TabIndex = 54
        '
        'Copyright
        '
        Me.Copyright.AutoSize = True
        Me.Copyright.BackColor = System.Drawing.Color.Transparent
        Me.Copyright.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Copyright.ForeColor = System.Drawing.Color.DimGray
        Me.Copyright.Location = New System.Drawing.Point(7, 3)
        Me.Copyright.Name = "Copyright"
        Me.Copyright.Size = New System.Drawing.Size(171, 13)
        Me.Copyright.TabIndex = 31
        Me.Copyright.Text = "© 2017 Image World Digital System"
        '
        'Timer1
        '
        '
        'Txt_name
        '
        Me.Txt_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_name.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.Txt_name.Location = New System.Drawing.Point(143, 42)
        Me.Txt_name.Name = "Txt_name"
        Me.Txt_name.Size = New System.Drawing.Size(290, 20)
        Me.Txt_name.TabIndex = 1
        Me.Txt_name.Text = "username"
        Me.Txt_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Txt_Pass
        '
        Me.Txt_Pass.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_Pass.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.Txt_Pass.Location = New System.Drawing.Point(143, 71)
        Me.Txt_Pass.Name = "Txt_Pass"
        Me.Txt_Pass.PasswordChar = Global.Microsoft.VisualBasic.ChrW(8226)
        Me.Txt_Pass.Size = New System.Drawing.Size(290, 20)
        Me.Txt_Pass.TabIndex = 2
        Me.Txt_Pass.Text = "password"
        Me.Txt_Pass.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Pic_Logo
        '
        Me.Pic_Logo.Image = CType(resources.GetObject("Pic_Logo.Image"), System.Drawing.Image)
        Me.Pic_Logo.InitialImage = CType(resources.GetObject("Pic_Logo.InitialImage"), System.Drawing.Image)
        Me.Pic_Logo.Location = New System.Drawing.Point(22, 30)
        Me.Pic_Logo.Name = "Pic_Logo"
        Me.Pic_Logo.Size = New System.Drawing.Size(70, 70)
        Me.Pic_Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Pic_Logo.TabIndex = 58
        Me.Pic_Logo.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.InitialImage = CType(resources.GetObject("PictureBox1.InitialImage"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(117, 42)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 59
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.InitialImage = CType(resources.GetObject("PictureBox2.InitialImage"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(117, 71)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 60
        Me.PictureBox2.TabStop = False
        '
        'UsersBindingSource
        '
        Me.UsersBindingSource.DataMember = "Users"
        Me.UsersBindingSource.DataSource = Me._IW_JobOrdersDataSet
        '
        '_IW_JobOrdersDataSet
        '
        Me._IW_JobOrdersDataSet.DataSetName = "_IW_JobOrdersDataSet"
        Me._IW_JobOrdersDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'UsersTableAdapter
        '
        Me.UsersTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.EmailTableAdapter = Nothing
        Me.TableAdapterManager.JobOrdersTableAdapter = Nothing
        Me.TableAdapterManager.SettingTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = $safeprojectname$._IW_JobOrdersDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UsersTableAdapter = Me.UsersTableAdapter
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(546, 31)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(18, 13)
        Me.Label2.TabIndex = 68
        Me.Label2.Text = "ID"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(533, 91)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 13)
        Me.Label3.TabIndex = 69
        Me.Label3.Text = "Uname"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(648, 31)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(37, 13)
        Me.Label4.TabIndex = 70
        Me.Label4.Text = "Upass"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(644, 91)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(39, 13)
        Me.Label5.TabIndex = 71
        Me.Label5.Text = "Fname"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(745, 31)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(47, 13)
        Me.Label6.TabIndex = 72
        Me.Label6.Text = "Total JO"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(756, 91)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(34, 13)
        Me.Label7.TabIndex = 73
        Me.Label7.Text = "Mode"
        '
        'FullNameTextBox
        '
        Me.FullNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UsersBindingSource, "FullName", True))
        Me.FullNameTextBox.Location = New System.Drawing.Point(618, 68)
        Me.FullNameTextBox.Name = "FullNameTextBox"
        Me.FullNameTextBox.Size = New System.Drawing.Size(100, 20)
        Me.FullNameTextBox.TabIndex = 77
        '
        'PasswordTextBox
        '
        Me.PasswordTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UsersBindingSource, "Password", True))
        Me.PasswordTextBox.Location = New System.Drawing.Point(618, 46)
        Me.PasswordTextBox.Name = "PasswordTextBox"
        Me.PasswordTextBox.Size = New System.Drawing.Size(100, 20)
        Me.PasswordTextBox.TabIndex = 80
        '
        'ModeTextBox
        '
        Me.ModeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UsersBindingSource, "Mode", True))
        Me.ModeTextBox.Location = New System.Drawing.Point(724, 68)
        Me.ModeTextBox.Name = "ModeTextBox"
        Me.ModeTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ModeTextBox.TabIndex = 81
        '
        'IDTextBox
        '
        Me.IDTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UsersBindingSource, "ID", True))
        Me.IDTextBox.Location = New System.Drawing.Point(512, 46)
        Me.IDTextBox.Name = "IDTextBox"
        Me.IDTextBox.Size = New System.Drawing.Size(100, 20)
        Me.IDTextBox.TabIndex = 82
        '
        'UsernameTextBox
        '
        Me.UsernameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UsersBindingSource, "Username", True))
        Me.UsernameTextBox.Location = New System.Drawing.Point(512, 68)
        Me.UsernameTextBox.Name = "UsernameTextBox"
        Me.UsernameTextBox.Size = New System.Drawing.Size(100, 20)
        Me.UsernameTextBox.TabIndex = 83
        '
        'JOcountTextBox
        '
        Me.JOcountTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UsersBindingSource, "JOcount", True))
        Me.JOcountTextBox.Location = New System.Drawing.Point(724, 46)
        Me.JOcountTextBox.Name = "JOcountTextBox"
        Me.JOcountTextBox.Size = New System.Drawing.Size(100, 20)
        Me.JOcountTextBox.TabIndex = 84
        '
        'Login
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(459, 124)
        Me.Controls.Add(Me.JOcountTextBox)
        Me.Controls.Add(Me.UsernameTextBox)
        Me.Controls.Add(Me.IDTextBox)
        Me.Controls.Add(Me.ModeTextBox)
        Me.Controls.Add(Me.PasswordTextBox)
        Me.Controls.Add(Me.FullNameTextBox)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Pic_Logo)
        Me.Controls.Add(Me.Txt_Pass)
        Me.Controls.Add(Me.Txt_name)
        Me.Controls.Add(Me.BottomPanel)
        Me.Controls.Add(Me.TopPanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Login"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Image World"
        Me.TopMost = True
        Me.TopPanel.ResumeLayout(False)
        Me.TopPanel.PerformLayout()
        CType(Me.Btn_Minimize, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Btn_Maximize, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Btn_Exit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BottomPanel.ResumeLayout(False)
        Me.BottomPanel.PerformLayout()
        CType(Me.Pic_Logo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._IW_JobOrdersDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TopPanel As System.Windows.Forms.Panel
    Friend WithEvents Btn_Maximize As System.Windows.Forms.PictureBox
    Friend WithEvents Btn_Exit As System.Windows.Forms.PictureBox
    Friend WithEvents BottomPanel As System.Windows.Forms.Panel
    Friend WithEvents Copyright As System.Windows.Forms.Label
    Friend WithEvents _IW_JobOrdersDataSet As $safeprojectname$._IW_JobOrdersDataSet
    Friend WithEvents UsersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents UsersTableAdapter As $safeprojectname$._IW_JobOrdersDataSetTableAdapters.UsersTableAdapter
    Friend WithEvents TableAdapterManager As $safeprojectname$._IW_JobOrdersDataSetTableAdapters.TableAdapterManager
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Txt_name As System.Windows.Forms.TextBox
    Friend WithEvents Txt_Pass As System.Windows.Forms.TextBox
    Friend WithEvents Pic_Logo As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Top_Title As System.Windows.Forms.Label
    Friend WithEvents Btn_Minimize As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents FullNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PasswordTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ModeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents IDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents UsernameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents JOcountTextBox As System.Windows.Forms.TextBox
End Class
