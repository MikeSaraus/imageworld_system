﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SetupDB
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SetupDB))
        Me.DBSet = New System.Windows.Forms.TextBox()
        Me.Save = New System.Windows.Forms.Button()
        Me.Pic = New System.Windows.Forms.PictureBox()
        Me.TopPanel = New System.Windows.Forms.Panel()
        Me.Btn_Logo = New System.Windows.Forms.PictureBox()
        Me.Top_Title = New System.Windows.Forms.Label()
        Me.BottomPanel = New System.Windows.Forms.Panel()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.Btn_Folder = New System.Windows.Forms.Button()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        CType(Me.Pic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TopPanel.SuspendLayout()
        CType(Me.Btn_Logo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DBSet
        '
        Me.DBSet.ForeColor = System.Drawing.Color.DarkRed
        Me.DBSet.Location = New System.Drawing.Point(74, 38)
        Me.DBSet.Name = "DBSet"
        Me.DBSet.Size = New System.Drawing.Size(296, 20)
        Me.DBSet.TabIndex = 1
        Me.DBSet.Text = "Drive:\Folder_Source"
        Me.DBSet.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Save
        '
        Me.Save.BackColor = System.Drawing.Color.Black
        Me.Save.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Save.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Save.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Save.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.Save.Location = New System.Drawing.Point(74, 65)
        Me.Save.Name = "Save"
        Me.Save.Size = New System.Drawing.Size(296, 19)
        Me.Save.TabIndex = 67
        Me.Save.Text = "Continue"
        Me.Save.UseVisualStyleBackColor = False
        '
        'Pic
        '
        Me.Pic.BackColor = System.Drawing.SystemColors.Control
        Me.Pic.Image = CType(resources.GetObject("Pic.Image"), System.Drawing.Image)
        Me.Pic.Location = New System.Drawing.Point(11, 32)
        Me.Pic.Name = "Pic"
        Me.Pic.Size = New System.Drawing.Size(55, 57)
        Me.Pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Pic.TabIndex = 66
        Me.Pic.TabStop = False
        '
        'TopPanel
        '
        Me.TopPanel.BackColor = System.Drawing.Color.Black
        Me.TopPanel.Controls.Add(Me.Btn_Logo)
        Me.TopPanel.Controls.Add(Me.Top_Title)
        Me.TopPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me.TopPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopPanel.Name = "TopPanel"
        Me.TopPanel.Size = New System.Drawing.Size(385, 25)
        Me.TopPanel.TabIndex = 65
        '
        'Btn_Logo
        '
        Me.Btn_Logo.BackColor = System.Drawing.Color.Black
        Me.Btn_Logo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Logo.ErrorImage = Nothing
        Me.Btn_Logo.Image = CType(resources.GetObject("Btn_Logo.Image"), System.Drawing.Image)
        Me.Btn_Logo.InitialImage = CType(resources.GetObject("Btn_Logo.InitialImage"), System.Drawing.Image)
        Me.Btn_Logo.Location = New System.Drawing.Point(8, 6)
        Me.Btn_Logo.Name = "Btn_Logo"
        Me.Btn_Logo.Size = New System.Drawing.Size(80, 21)
        Me.Btn_Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Logo.TabIndex = 52
        Me.Btn_Logo.TabStop = False
        '
        'Top_Title
        '
        Me.Top_Title.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.Top_Title.AutoSize = True
        Me.Top_Title.BackColor = System.Drawing.Color.Black
        Me.Top_Title.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.Top_Title.Location = New System.Drawing.Point(289, 6)
        Me.Top_Title.Name = "Top_Title"
        Me.Top_Title.Size = New System.Drawing.Size(87, 13)
        Me.Top_Title.TabIndex = 51
        Me.Top_Title.Text = "Developer Setup"
        Me.Top_Title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BottomPanel
        '
        Me.BottomPanel.BackColor = System.Drawing.Color.Black
        Me.BottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BottomPanel.Location = New System.Drawing.Point(0, 96)
        Me.BottomPanel.Name = "BottomPanel"
        Me.BottomPanel.Size = New System.Drawing.Size(385, 5)
        Me.BottomPanel.TabIndex = 68
        '
        'Timer1
        '
        '
        'FolderBrowserDialog1
        '
        Me.FolderBrowserDialog1.ShowNewFolderButton = False
        '
        'Btn_Folder
        '
        Me.Btn_Folder.BackColor = System.Drawing.Color.Gainsboro
        Me.Btn_Folder.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Folder.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Btn_Folder.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Folder.ForeColor = System.Drawing.Color.DarkRed
        Me.Btn_Folder.Location = New System.Drawing.Point(75, 38)
        Me.Btn_Folder.Name = "Btn_Folder"
        Me.Btn_Folder.Size = New System.Drawing.Size(68, 20)
        Me.Btn_Folder.TabIndex = 69
        Me.Btn_Folder.Text = "Browse"
        Me.Btn_Folder.UseVisualStyleBackColor = False
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Checked = True
        Me.CheckBox1.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox1.ForeColor = System.Drawing.Color.Red
        Me.CheckBox1.Location = New System.Drawing.Point(353, 41)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox1.TabIndex = 70
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'SetupDB
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(385, 101)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.Btn_Folder)
        Me.Controls.Add(Me.BottomPanel)
        Me.Controls.Add(Me.DBSet)
        Me.Controls.Add(Me.Save)
        Me.Controls.Add(Me.Pic)
        Me.Controls.Add(Me.TopPanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "SetupDB"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Setup $safeprojectname$"
        CType(Me.Pic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TopPanel.ResumeLayout(False)
        Me.TopPanel.PerformLayout()
        CType(Me.Btn_Logo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DBSet As System.Windows.Forms.TextBox
    Friend WithEvents Save As System.Windows.Forms.Button
    Friend WithEvents Pic As System.Windows.Forms.PictureBox
    Friend WithEvents TopPanel As System.Windows.Forms.Panel
    Friend WithEvents Top_Title As System.Windows.Forms.Label
    Friend WithEvents Btn_Logo As System.Windows.Forms.PictureBox
    Friend WithEvents BottomPanel As System.Windows.Forms.Panel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents Btn_Folder As System.Windows.Forms.Button
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
End Class
