﻿Imports System.Security.Principal
Imports System.IO
Imports System.Text.RegularExpressions

Public Class Main

    Private WithEvents timer As New System.Timers.Timer
    Public isRound As Boolean = False
    Dim dbSource1 As String = "Z:\Database"
    Dim updateDB As Boolean = True

#Region "Iniatials and Varialbles"

    Public dPCPic As String = "C:\ProgramData\Microsoft\User Account Pictures\user.bmp"
    Public proPic As String
    Private newPic As String
    Public proChange As Boolean = False
    Public infoWindow As New List(Of AInfo)
    Public sendMail As New List(Of EMail)
    Public infoFilType As String
    Dim filerTotal As Integer = 0
    Dim filerX() As String
    Private IsFormBeingDragged As Boolean = False
    Private MouseDownX As Integer
    Private MouseDownY As Integer
    Dim LeavingX As Boolean = False
    Dim AllowedEnter As Boolean = False
    Dim CustomToolTip As System.Windows.Forms.ToolTip = New System.Windows.Forms.ToolTip()
    Dim Total_JO As Integer 'Total Number of Records in database
    Public chatActive As Boolean = False
    Public settingActive As Boolean = False

    Dim seachActive As Boolean = False
    Dim allowResize As Boolean = True
    Dim identity = WindowsIdentity.GetCurrent()
    Dim principal = New WindowsPrincipal(identity)
    Dim superUser As Boolean = principal.IsInRole(WindowsBuiltInRole.Administrator)
    Public filterUsr As String = ""
    Dim oldFilter As String = ""
#End Region

#Region "Disable Exit Ways"
    Protected Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            Const CS_NOCLOSE As Integer = &H200
            cp.ClassStyle = cp.ClassStyle Or CS_NOCLOSE
            Return cp
        End Get
    End Property
#End Region

#Region "Base64"
    Public Function base64Encode(ByVal sData As String) As String

        Try
            Dim encData_Byte As Byte() = New Byte(sData.Length - 1) {}
            encData_Byte = System.Text.Encoding.UTF8.GetBytes(sData)
            Dim encodedData As String = Convert.ToBase64String(encData_Byte)
            Return (encodedData)

        Catch ex As Exception

            Throw (New Exception("Error is base64Encode" & ex.Message))

        End Try


    End Function

    Public Function base64Decode(ByVal sData As String) As String

        Dim encoder As New System.Text.UTF8Encoding()
        Dim utf8Decode As System.Text.Decoder = encoder.GetDecoder()
        Dim todecode_byte As Byte() = Convert.FromBase64String(sData)
        Dim charCount As Integer = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length)
        Dim decoded_char As Char() = New Char(charCount - 1) {}
        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0)
        Dim result As String = New [String](decoded_char)
        Return result

    End Function
#End Region

    Public Function cleanString(str As String) As String
        On Error GoTo ErrorX
        str = Regex.Replace(str, "[^A-Za-z0-9_.\-/ ]", "")
        Return str
ErrorX:
        Return "False"
    End Function

#Region "Enter KeyManipulated"
    'Enter Key
    Protected Overrides Function ProcessCmdKey(ByRef msg As System.Windows.Forms.Message, _
                                           ByVal keyData As System.Windows.Forms.Keys) _
                                           As Boolean

        If msg.WParam.ToInt32() = CInt(Keys.Enter) Then
            If (Not AllowedEnter) Then
                SendKeys.Send("{Tab}")
                Return True
            End If

        End If
        Return MyBase.ProcessCmdKey(msg, keyData)
    End Function
#End Region

#Region "Rounded"
    Private Function RoundedRec(ByVal X As Integer, ByVal Y As Integer, ByVal Width As Integer, ByVal Height As Integer) As System.Drawing.Drawing2D.GraphicsPath
        ' Make and Draw a path.
        Dim graphics_path As New System.Drawing.Drawing2D.GraphicsPath
        graphics_path.AddLine(X + 10, Y, X + Width, Y) 'add the Top line to the path

        'Top Right corner        
        Dim tr() As Point = { _
        New Point(X + Width, Y), _
        New Point((X + Width) + 4, Y + 2), _
        New Point((X + Width) + 8, Y + 6), _
        New Point((X + Width) + 10, Y + 10)}

        graphics_path.AddCurve(tr)  'Add the Top right curve to the path

        'Bottom right corner 
        Dim br() As Point = { _
        New Point((X + Width) + 10, Y + Height), _
        New Point((X + Width) + 8, (Y + Height) + 4), _
        New Point((X + Width) + 4, (Y + Height) + 8), _
        New Point(X + Width, (Y + Height) + 10)}

        graphics_path.AddCurve(br)  'Add the Bottom right curve to the path

        'Bottom left corner
        Dim bl() As Point = { _
        New Point(X + 10, (Y + Height) + 10), _
        New Point(X + 6, (Y + Height) + 8), _
        New Point(X + 2, (Y + Height) + 4), _
        New Point(X, Y + Height)}

        graphics_path.AddCurve(bl)  'Add the Bottom left curve to the path

        'Top left corner
        Dim tl() As Point = { _
        New Point(X, Y + 10), _
        New Point(X + 2, Y + 6), _
        New Point(X + 6, Y + 2), _
        New Point(X + 10, Y)}

        graphics_path.AddCurve(tl)  'add the Top left curve to the path

        Return graphics_path

    End Function
#End Region

    Private Sub InstructionsTextBox_LostFocus(sender As Object, e As System.EventArgs) Handles InstructionsTextBox.LostFocus
        AllowedEnter = False
    End Sub

    Private Sub InstructionsTextBox_TextChanged(sender As System.Object, e As System.EventArgs) Handles InstructionsTextBox.TextChanged
        AllowedEnter = True
    End Sub

#Region "Users Information"

    Public userID As String = Login.IDTextBox.Text
    Public userName As String = Login.UsernameTextBox.Text
    Public userPass As String = Login.PasswordTextBox.Text
    Public fullName As String = Login.FullNameTextBox.Text
    Public Mode As String = Login.ModeTextBox.Text
    Public JOcount As String = Login.JOcountTextBox.Text

#End Region

#Region "Main Window and Top Panel Code"
    Dim posX As Integer
    Dim posY As Integer
    Dim drag As Boolean

    'resize
    Dim onFullScreen As Boolean
    Dim maximized As Boolean
    Dim on_MinimumSize As Boolean
    Dim minimumWidth As Short = 350
    Dim minimumHeight As Short = 26
    Dim borderSpace As Short = 20
    Dim borderDiameter As Short = 3

    Dim onBorderRight As Boolean
    Dim onBorderLeft As Boolean
    Dim onBorderTop As Boolean
    Dim onBorderBottom As Boolean
    Dim onCornerTopRight As Boolean
    Dim onCornerTopLeft As Boolean
    Dim onCornerBottomRight As Boolean
    Dim onCornerBottomLeft As Boolean

    Dim movingRight As Boolean
    Dim movingLeft As Boolean
    Dim movingTop As Boolean
    Dim movingBottom As Boolean
    Dim movingCornerTopRight As Boolean
    Dim movingCornerTopLeft As Boolean
    Dim movingCornerBottomRight As Boolean
    Dim movingCornerBottomLeft As Boolean

    Private Sub Main_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing

        CloseWindows()
        CloseMails()
        ChangePass.Close()
        If chatActive Then Message.Close()
        If settingActive Then ChangePass.Close()
        Login.Close()

    End Sub

    Private Sub Main_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'Set interval to 1 minute
        timer.Interval = 60000

        'Synchronize with current form, or else an error will occur when trying to
        'update the UI from within the elapsed event
        timer.SynchronizingObject = Me

        If AutoStartTextBox.Text = "Yes" Then
            autoStartX(True)
        End If
        Btn_x.Visible = False
        proPic = SetupDB.setUpFolder & "\Profile.jpg"
        If System.IO.File.Exists(proPic) Then
            E_Pic.ImageLocation = proPic
        ElseIf System.IO.File.Exists(dPCPic) Then
            E_Pic.ImageLocation = dPCPic
        End If
        'TODO: This line of code loads data into the '_IW_JobOrdersDataSet.Setting' table. You can move, or remove it, as needed.
        Me.SettingTableAdapter.Fill(Me._IW_JobOrdersDataSet.Setting)
        If Mode = "Administrator" Then
            DBlocate.Visible = True
            AutoStartCheckBox.Visible = True
            AdmTxt.Text = "Admin Mode Enabled"
            Btn_Reset_Db.Visible = True
        End If
        'TODO: This line of code loads data into the '_IW_JobOrdersDataSet.Users' table. You can move, or remove it, as needed.
        Me.UsersTableAdapter.Fill(Me._IW_JobOrdersDataSet.Users)
        'TODO: This line of code loads data into the '_IW_JobOrdersDataSet.JobOrders' table. You can move, or remove it, as needed.
        allowResize = False
        Try
            Me.JobOrdersTableAdapter.Fill(Me._IW_JobOrdersDataSet.JobOrders)
        Catch ex As Exception
            MessageBox.Show("Database Error: Maybe Open or Not Accessible.", "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        TimerCountTotalJO.Start()
        Timer_Refresh.Start()
        'E_Pic.Region = New Region(RoundedRec(0, 0, E_Pic.Width - (E_Pic.Width / 2), E_Pic.Height - (E_Pic.Width / 2)))

        E_Name.Text = fullName
        E_Mode.Text = Mode

    End Sub

    Private Sub TopPanel_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles TopPanel.MouseDown
        If e.Button = MouseButtons.Left Then
            drag = True
            posX = Cursor.Position.X - Me.Left
            posY = Cursor.Position.Y - Me.Top
        End If
    End Sub

    Private Sub Top_Title_MouseDoubleClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Top_Title.MouseDoubleClick
        If allowResize Then
            If e.Button = MouseButtons.Left Then
                If maximized Then
                    Me.WindowState = FormWindowState.Normal
                    maximized = False
                Else
                    Me.WindowState = FormWindowState.Maximized
                    maximized = True
                End If
            End If
        Else
            If Me.TopMost Then
                Me.TopMost = False
            Else
                Me.TopMost = True
            End If
        End If
    End Sub

    Private Sub Top_Title_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Top_Title.MouseUp
        drag = False
    End Sub

    Private Sub Top_Title_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Top_Title.MouseDown
        If e.Button = MouseButtons.Left Then
            drag = True
            posX = Cursor.Position.X - Me.Left
            posY = Cursor.Position.Y - Me.Top
        End If
    End Sub

    Private Sub Top_Title_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Top_Title.MouseMove
        If drag Then
            Me.Top = Cursor.Position.Y - posY
            Me.Left = Cursor.Position.X - posX
        End If
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub Copyright_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Copyright.MouseDown
        If e.Button = MouseButtons.Left Then
            drag = True
            posX = Cursor.Position.X - Me.Left
            posY = Cursor.Position.Y - Me.Top
        End If
    End Sub

    Private Sub Copyright_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Copyright.MouseMove
        If drag Then
            Me.Top = Cursor.Position.Y - posY
            Me.Left = Cursor.Position.X - posX
        End If
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub Copyright_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Copyright.MouseUp
        drag = False
    End Sub

    Private Sub BottomPanel_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles BottomPanel.MouseDown
        If e.Button = MouseButtons.Left Then
            drag = True
            posX = Cursor.Position.X - Me.Left
            posY = Cursor.Position.Y - Me.Top
        End If
        If e.Button = Windows.Forms.MouseButtons.Left Then
            If onBorderRight Then movingRight = True Else movingRight = False
            If onBorderLeft Then movingLeft = True Else movingLeft = False
            If onBorderTop Then movingTop = True Else movingTop = False
            If onBorderBottom Then movingBottom = True Else movingBottom = False
            If onCornerTopRight Then movingCornerTopRight = True Else movingCornerTopRight = False
            If onCornerTopLeft Then movingCornerTopLeft = True Else movingCornerTopLeft = False
            If onCornerBottomRight Then movingCornerBottomRight = True Else movingCornerBottomRight = False
            If onCornerBottomLeft Then movingCornerBottomLeft = True Else movingCornerBottomLeft = False
        End If
    End Sub

    Private Sub BottomPanel_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles BottomPanel.MouseMove
        'start resize
        If Not (onFullScreen Or maximized Or Not allowResize) Then

            If Me.Width <= minimumWidth Then Me.Width = (minimumWidth + 5) : on_MinimumSize = True
            If Me.Height <= minimumHeight Then Me.Height = (minimumHeight + 5) : on_MinimumSize = True
            If on_MinimumSize Then stopResizer() Else startResizer()


            If (Cursor.Position.X > (Me.Location.X + Me.Width) - borderDiameter) _
                And (Cursor.Position.Y > (Me.Location.Y + borderSpace)) _
                And (Cursor.Position.Y < ((Me.Location.Y + Me.Height) - borderSpace)) Then
                Me.Cursor = Cursors.SizeWE
                onBorderRight = True

            ElseIf (Cursor.Position.X < (Me.Location.X + borderDiameter)) _
                And (Cursor.Position.Y > (Me.Location.Y + borderSpace)) _
                And (Cursor.Position.Y < ((Me.Location.Y + Me.Height) - borderSpace)) Then
                Me.Cursor = Cursors.SizeWE
                onBorderLeft = True

            ElseIf (Cursor.Position.Y < (Me.Location.Y + borderDiameter)) _
                And (Cursor.Position.X > (Me.Location.X + borderSpace)) _
                And (Cursor.Position.X < ((Me.Location.X + Me.Width) - borderSpace)) Then
                Me.Cursor = Cursors.SizeNS
                onBorderTop = True

            ElseIf (Cursor.Position.Y > ((Me.Location.Y + Me.Height) - borderDiameter)) _
                And (Cursor.Position.X > (Me.Location.X + borderSpace)) _
                And (Cursor.Position.X < ((Me.Location.X + Me.Width) - borderSpace)) Then
                Me.Cursor = Cursors.SizeNS
                onBorderBottom = True

            ElseIf (Cursor.Position.X = ((Me.Location.X + Me.Width) - 1)) _
                And (Cursor.Position.Y = Me.Location.Y) Then
                Me.Cursor = Cursors.SizeNESW
                onCornerTopRight = True

            ElseIf (Cursor.Position.X = Me.Location.X) _
                And (Cursor.Position.Y = Me.Location.Y) Then
                Me.Cursor = Cursors.SizeNWSE
                onCornerTopLeft = True

            ElseIf (Cursor.Position.X = ((Me.Location.X + Me.Width) - 1)) _
                And (Cursor.Position.Y = ((Me.Location.Y + Me.Height) - 1)) Then
                Me.Cursor = Cursors.SizeNWSE
                onCornerBottomRight = True

            ElseIf (Cursor.Position.X = Me.Location.X) _
                And (Cursor.Position.Y = ((Me.Location.Y + Me.Height) - 1)) Then
                Me.Cursor = Cursors.SizeNESW
                onCornerBottomLeft = True

            Else
                onBorderRight = False
                onBorderLeft = False
                onBorderTop = False
                onBorderBottom = False
                onCornerTopRight = False
                onCornerTopLeft = False
                onCornerBottomRight = False
                onCornerBottomLeft = False
                Me.Cursor = Cursors.Default
                If drag Then
                    Me.Top = Cursor.Position.Y - posY
                    Me.Left = Cursor.Position.X - posX
                End If

            End If

        Else
            Me.Cursor = Cursors.Default
            If drag Then
                Me.Top = Cursor.Position.Y - posY
                Me.Left = Cursor.Position.X - posX
            End If
            Me.Cursor = Cursors.Default
        End If

    End Sub

    Private Sub BottomPanel_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles BottomPanel.MouseUp
        drag = False
        stopResizer()
    End Sub

    Private Sub TopPanel_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TopPanel.MouseDoubleClick
        If allowResize Then
            If e.Button = MouseButtons.Left Then
                If maximized Then
                    Me.WindowState = FormWindowState.Normal
                    maximized = False
                Else
                    Me.WindowState = FormWindowState.Maximized
                    maximized = True
                End If
            End If
        Else
            If Me.TopMost Then
                Me.TopMost = False
            Else
                Me.TopMost = True
            End If
        End If
    End Sub

    Private Sub TopPanel_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles TopPanel.MouseMove
        If drag Then
            Me.Top = Cursor.Position.Y - posY
            Me.Left = Cursor.Position.X - posX
        End If
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub TopPanel_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles TopPanel.MouseUp
        drag = False
    End Sub

    Private Sub Main_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseDown
        If e.Button = Windows.Forms.MouseButtons.Left Then
            If onBorderRight Then movingRight = True Else movingRight = False
            If onBorderLeft Then movingLeft = True Else movingLeft = False
            If onBorderTop Then movingTop = True Else movingTop = False
            If onBorderBottom Then movingBottom = True Else movingBottom = False
            If onCornerTopRight Then movingCornerTopRight = True Else movingCornerTopRight = False
            If onCornerTopLeft Then movingCornerTopLeft = True Else movingCornerTopLeft = False
            If onCornerBottomRight Then movingCornerBottomRight = True Else movingCornerBottomRight = False
            If onCornerBottomLeft Then movingCornerBottomLeft = True Else movingCornerBottomLeft = False
        End If
    End Sub

    Private Sub Main_MouseHover(sender As Object, e As System.EventArgs) Handles Me.MouseHover
        Btn_x.Visible = False
    End Sub

    Private Sub Main_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseMove
        If onFullScreen Or maximized Or Not allowResize Then Exit Sub

        If Me.Width <= minimumWidth Then Me.Width = (minimumWidth + 5) : on_MinimumSize = True
        If Me.Height <= minimumHeight Then Me.Height = (minimumHeight + 5) : on_MinimumSize = True
        If on_MinimumSize Then stopResizer() Else startResizer()


        If (Cursor.Position.X > (Me.Location.X + Me.Width) - borderDiameter) _
            And (Cursor.Position.Y > (Me.Location.Y + borderSpace)) _
            And (Cursor.Position.Y < ((Me.Location.Y + Me.Height) - borderSpace)) Then
            Me.Cursor = Cursors.SizeWE
            onBorderRight = True

        ElseIf (Cursor.Position.X < (Me.Location.X + borderDiameter)) _
            And (Cursor.Position.Y > (Me.Location.Y + borderSpace)) _
            And (Cursor.Position.Y < ((Me.Location.Y + Me.Height) - borderSpace)) Then
            Me.Cursor = Cursors.SizeWE
            onBorderLeft = True

        ElseIf (Cursor.Position.Y < (Me.Location.Y + borderDiameter)) _
            And (Cursor.Position.X > (Me.Location.X + borderSpace)) _
            And (Cursor.Position.X < ((Me.Location.X + Me.Width) - borderSpace)) Then
            Me.Cursor = Cursors.SizeNS
            onBorderTop = True

        ElseIf (Cursor.Position.Y > ((Me.Location.Y + Me.Height) - borderDiameter)) _
            And (Cursor.Position.X > (Me.Location.X + borderSpace)) _
            And (Cursor.Position.X < ((Me.Location.X + Me.Width) - borderSpace)) Then
            Me.Cursor = Cursors.SizeNS
            onBorderBottom = True

        ElseIf (Cursor.Position.X = ((Me.Location.X + Me.Width) - 1)) _
            And (Cursor.Position.Y = Me.Location.Y) Then
            Me.Cursor = Cursors.SizeNESW
            onCornerTopRight = True

        ElseIf (Cursor.Position.X = Me.Location.X) _
            And (Cursor.Position.Y = Me.Location.Y) Then
            Me.Cursor = Cursors.SizeNWSE
            onCornerTopLeft = True

        ElseIf (Cursor.Position.X = ((Me.Location.X + Me.Width) - 1)) _
            And (Cursor.Position.Y = ((Me.Location.Y + Me.Height) - 1)) Then
            Me.Cursor = Cursors.SizeNWSE
            onCornerBottomRight = True

        ElseIf (Cursor.Position.X = Me.Location.X) _
            And (Cursor.Position.Y = ((Me.Location.Y + Me.Height) - 1)) Then
            Me.Cursor = Cursors.SizeNESW
            onCornerBottomLeft = True

        Else
            onBorderRight = False
            onBorderLeft = False
            onBorderTop = False
            onBorderBottom = False
            onCornerTopRight = False
            onCornerTopLeft = False
            onCornerBottomRight = False
            onCornerBottomLeft = False
            Me.Cursor = Cursors.Default
        End If
    End Sub

    Private Sub Main_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseUp
        stopResizer()
    End Sub

    'functions
    Private Sub startResizer()
        Select Case True

            Case movingRight
                Me.Width = (Cursor.Position.X - Me.Location.X)

            Case movingLeft
                Me.Width = ((Me.Width + Me.Location.X) - Cursor.Position.X)
                Me.Location = New Point(Cursor.Position.X, Me.Location.Y)

            Case movingTop
                Me.Height = ((Me.Height + Me.Location.Y) - Cursor.Position.Y)
                Me.Location = New Point(Me.Location.X, Cursor.Position.Y)

            Case movingBottom
                Me.Height = (Cursor.Position.Y - Me.Location.Y)

            Case movingCornerTopRight
                Me.Width = (Cursor.Position.X - Me.Location.X)
                Me.Height = ((Me.Location.Y - Cursor.Position.Y) + Me.Height)
                Me.Location = New Point(Me.Location.X, Cursor.Position.Y)

            Case movingCornerTopLeft
                Me.Width = ((Me.Width + Me.Location.X) - Cursor.Position.X)
                Me.Location = New Point(Cursor.Position.X, Me.Location.Y)
                Me.Height = ((Me.Height + Me.Location.Y) - Cursor.Position.Y)
                Me.Location = New Point(Me.Location.X, Cursor.Position.Y)

            Case movingCornerBottomRight
                Me.Size = New Point((Cursor.Position.X - Me.Location.X), (Cursor.Position.Y - Me.Location.Y))

            Case movingCornerBottomLeft
                Me.Width = ((Me.Width + Me.Location.X) - Cursor.Position.X)
                Me.Height = (Cursor.Position.Y - Me.Location.Y)
                Me.Location = New Point(Cursor.Position.X, Me.Location.Y)

        End Select
    End Sub

    Private Sub stopResizer()
        movingRight = False
        movingLeft = False
        movingTop = False
        movingBottom = False
        movingCornerTopRight = False
        movingCornerTopLeft = False
        movingCornerBottomRight = False
        movingCornerBottomLeft = False
        Me.Cursor = Cursors.Default
        Threading.Thread.Sleep(300)
        on_MinimumSize = False
    End Sub
#End Region

    Private Sub TimerCountTotalJO_Tick(sender As System.Object, e As System.EventArgs) Handles TimerCountTotalJO.Tick
        If filterUsr <> "" Then
            Btn_DWeek.Visible = True
            Btn_WIP.Visible = True
        Else
            Btn_DWeek.Visible = False
            Btn_WIP.Visible = False
        End If

        'profilePicture Reload
        If proChange Then
            E_Pic.ImageLocation = proPic
            proChange = False
        End If

        If Not superUser Then
            Dim su As Integer = MessageBox.Show("System required administrative privilage.", "Superuser Required", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
            If su = DialogResult.Cancel Then
                Close()
            ElseIf su = DialogResult.Yes Then
                'restart
                Application.Restart()
            End If
        End If

        Total_JO = JobOrdersBindingSource.Count
        Label_Total_JO.Text = Total_JO

        If Not seachActive Then
            SearchBox.Text = "Search..."
            SearchBox.ForeColor = Color.DimGray
        End If

        'rounding borders include
        Try
            If isRound Then
                Dim p As New Drawing2D.GraphicsPath()
                p.StartFigure()
                p.AddArc(New Rectangle(0, 0, 20, 20), 180, 90)
                p.AddLine(20, 0, Me.Width - 20, 0)
                p.AddArc(New Rectangle(Me.Width - 20, 0, 20, 20), -90, 90)
                p.AddLine(Me.Width, 20, Me.Width, Me.Height - 20)
                p.AddArc(New Rectangle(Me.Width - 20, Me.Height - 20, 20, 20), 0, 90)
                p.AddLine(Me.Width - 20, Me.Height, 20, Me.Height)
                p.AddArc(New Rectangle(0, Me.Height - 20, 20, 20), 90, 90)
                p.CloseFigure()
                Me.Region = New Region(p)
            Else
                Me.Region = Nothing
            End If
        Catch ex As Exception

        End Try
        If JONumberLabel2.Text = "" Then
            JONumberLabel2.Text = "0"
        End If
    End Sub

    Private Sub Btn_First_Click(sender As System.Object, e As System.EventArgs) Handles Btn_First.Click
        JobOrdersBindingSource.MoveFirst()
    End Sub

    Private Sub Btn_Previews_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Previews.Click
        JobOrdersBindingSource.MovePrevious()
    End Sub

    Private Sub Btn_Next_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Next.Click
        JobOrdersBindingSource.MoveNext()
    End Sub

    Private Sub Btn_Last_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Last.Click
        JobOrdersBindingSource.MoveLast()
    End Sub

    Private Sub ColorsComboBox_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles ColorsComboBox.KeyDown
        If e.KeyCode = Keys.NumPad1 Then
            ColorsComboBox.Text = " – One Color"
        ElseIf e.KeyCode = Keys.NumPad4 Then
            ColorsComboBox.Text = " – Full Colors"
        End If
    End Sub

    Private Sub OutputComboBox_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles OutputComboBox.KeyDown
        If e.KeyCode = Keys.C Then
            OutputComboBox.Text = "TP – Computer to Plate"
        ElseIf e.KeyCode = Keys.D Then
            OutputComboBox.Text = "G – Digital Printing"
        ElseIf e.KeyCode = Keys.P Then
            OutputComboBox.Text = "VC"
        End If
    End Sub

    Private Sub QuantityTextBox_GotFocus(sender As Object, e As System.EventArgs) Handles QuantityTextBox.GotFocus
        CustomToolTip.SetToolTip(QuantityTextBox, "Input number")
    End Sub

    Private Sub QuantityTextBox_MouseHover(sender As Object, e As System.EventArgs) Handles QuantityTextBox.MouseHover
        CustomToolTip.SetToolTip(QuantityTextBox, "Input number")
    End Sub

#Region "Deleting 'Disabled"
    'Deleting is Disabled
    'Private Sub Delete_Click(sender As System.Object, e As System.EventArgs) Handles Delete.Click
    '    Dim result As Integer = MessageBox.Show("Note: Removed record will no longer recovered.", "Are you sure?", MessageBoxButtons.YesNo)
    '    If result = DialogResult.No Then
    '        'MessageBox.Show("No pressed")
    '    ElseIf result = DialogResult.Yes Then
    '        JobOrdersBindingSource.RemoveCurrent()
    '        Try
    '            JobOrdersBindingSource.EndEdit()
    '            JobOrdersTableAdapter.Update(_IW_JobOrdersDataSet.JobOrders)
    '            MessageBox.Show("Data Removed")
    '        Catch ex As Exception
    '            MessageBox.Show("Error Occured while deleting record")
    '        End Try
    '    End If
    'End Sub
#End Region

    Private Sub Btn_Minimize_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Minimize.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub Btn_Maximize_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Maximize.Click
        If allowResize Then
            If maximized Then
                maximized = False
                Me.WindowState = FormWindowState.Normal
            Else
                maximized = True
                Me.WindowState = FormWindowState.Maximized
            End If
        End If
    End Sub

    Private Sub Btn_Exit_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Exit.Click
        Dim NewLogout As New Logout
        NewLogout.Show()
        'Logout.Show()
    End Sub

    Private Sub SearchBox_GotFocus(sender As Object, e As System.EventArgs) Handles SearchBox.GotFocus
        seachActive = True
    End Sub

    Private Sub SearchBox_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles SearchBox.KeyDown
        If cleanString(SearchBox.Text) = "Search..." Then
            SearchBox.Text = ""
        End If
        If e.KeyCode = Keys.Enter Then
            SearchDatabase(True)
        End If
        seachActive = True
    End Sub

    Private Sub SearchBox_LostFocus(sender As Object, e As System.EventArgs) Handles SearchBox.LostFocus
        AllowedEnter = False
        seachActive = False
    End Sub

    Private Sub SearchBox_TextChanged(sender As System.Object, e As System.EventArgs) Handles SearchBox.TextChanged
        AllowedEnter = True
        SearchDatabase(False)
        If cleanString(SearchBox.Text) = "" Then
            SearchBox.ForeColor = Color.DeepSkyBlue
            JobOrdersBindingSource.Filter = oldFilter
        End If
        seachActive = True
    End Sub

    Private Sub SearchDatabase(showAlert As Boolean)
        On Error GoTo SearchErr

        If cleanString(SearchBox.Text) = "" Then
            Exit Sub
        Else

            Dim searchFor As String = cleanString(SearchBox.Text.ToString)

            If Not filterUsr = "" Then
                JobOrdersBindingSource.Filter = "(Artist LIKE '" & filterUsr & "') AND ((Convert(ID, 'System.String') LIKE '" & searchFor & "')" & _
                    "OR (Convert(JONumber, 'System.String') LIKE '" & searchFor & "') OR (Customer LIKE '" & searchFor & "')" & _
                    "OR (Type LIKE '" & searchFor & "') OR (Convert(Quantity, 'System.String') LIKE '" & searchFor & "')" & _
                    "OR (Unit LIKE '" & searchFor & "') OR (Status LIKE '" & searchFor & "')" & _
                    "OR (Colors LIKE '" & searchFor & "') OR (Convert(DateRecieved, 'System.String') LIKE '" & searchFor & "')" & _
                    "OR (Convert(DateRequired, 'System.String') LIKE '" & searchFor & "') OR (Convert(OutDate, 'System.String') LIKE '" & searchFor & "')" & _
                    "OR (Output LIKE '" & searchFor & "') OR (Instructions LIKE '" & searchFor & "'))"
            Else
                JobOrdersBindingSource.Filter = "(Convert(ID, 'System.String') LIKE '" & searchFor & "')" & _
                    "OR (Convert(JONumber, 'System.String') LIKE '" & searchFor & "') OR (Customer LIKE '" & searchFor & "')" & _
                    "OR (Type LIKE '" & searchFor & "') OR (Convert(Quantity, 'System.String') LIKE '" & searchFor & "')" & _
                    "OR (Unit LIKE '" & searchFor & "') OR (Status LIKE '" & searchFor & "')" & _
                    "OR (Colors LIKE '" & searchFor & "') OR (Convert(DateRecieved, 'System.String') LIKE '" & searchFor & "')" & _
                    "OR (Convert(DateRequired, 'System.String') LIKE '" & searchFor & "') OR (Convert(OutDate, 'System.String') LIKE '" & searchFor & "')" & _
                    "OR (Output LIKE '" & searchFor & "') OR (Instructions LIKE '" & searchFor & "') OR (Artist LIKE '" & searchFor & "')"
            End If


            If JobOrdersBindingSource.Count <> 0 Then

                SearchBox.ForeColor = Color.DeepSkyBlue

            Else

                If Not oldFilter = "" Then
                    'use Old Filter
                Else
                    JobOrdersBindingSource.Filter = Nothing
                End If

                SearchBox.ForeColor = Color.Red

                If (showAlert) Then
                    MessageBox.Show("No record find for " & searchFor & ", try another keyword. ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If

            End If

        End If

ErrExit:
        Exit Sub
SearchErr:
        MessageBox.Show("Error Number: " & Err.Number & vbNewLine & _
                        "Error Discription: " & Err.Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Resume ErrExit

    End Sub

    Private Sub QuantityTextBox_TextChanged(sender As System.Object, e As System.EventArgs) Handles QuantityTextBox.TextChanged
        CustomToolTip.SetToolTip(QuantityTextBox, "Input number")
    End Sub

    Private Sub Btn_Maximize_MouseHover(sender As Object, e As System.EventArgs) Handles Btn_Maximize.MouseHover
        If Not allowResize Then
            Btn_Maximize.Cursor = Cursors.No
        End If
    End Sub

    Private Sub Btn_Exit_MouseHover(sender As Object, e As System.EventArgs) Handles Btn_Exit.MouseHover
        CustomToolTip.SetToolTip(Btn_Exit, "Logout")
    End Sub

    Private Sub JONumberTextBox_GotFocus(sender As Object, e As System.EventArgs) Handles JONumberTextBox.GotFocus
        CustomToolTip.SetToolTip(JONumberTextBox, "Input number")
    End Sub

    Private Sub JONumberTextBox_MouseHover(sender As Object, e As System.EventArgs) Handles JONumberTextBox.MouseHover
        CustomToolTip.SetToolTip(JONumberTextBox, "Input number")
    End Sub

    Private Sub JONumberTextBox_TextChanged(sender As System.Object, e As System.EventArgs) Handles JONumberTextBox.TextChanged
        CustomToolTip.SetToolTip(JONumberTextBox, "Input number")
    End Sub

    Public Sub filterArtistJOTotal(artist As String)
        Login.UsersBindingSource.Filter = "(Username LIKE '" & artist & "')"
        Login.JOcountTextBox.Text = JobOrdersBindingSource.Count
        Try
            Login.UsersBindingSource.EndEdit()
            Login.UsersTableAdapter.Update(Login._IW_JobOrdersDataSet.Users)
        Catch ex As Exception
            'error just nothing
        End Try
        With Chart1
            .Series(0).Points.Clear()
            .DataSource = ""
            .DataSource = UsersBindingSource
            .Series(0).XValueMember = "Username"
            .Series(0).YValueMembers = "JOcount"
        End With
    End Sub

    Private Sub Jayson_Data_MouseHover(sender As Object, e As System.EventArgs) Handles Jayson_Data.MouseHover
        If JobOrdersBindingSource.Filter <> "(Artist LIKE 'Jayson')" And (SearchBox.Text <> "" Or SearchBox.Text <> "Search...") Then
            JobOrdersBindingSource.Filter = "(Artist LIKE 'Jayson')"
            filterArtistJOTotal("Jayson")
            oldFilter = "Artist LIKE 'Jayson'"
            filterUsr = "Jayson"
        End If
    End Sub

    Private Sub John_Data_MouseHover(sender As Object, e As System.EventArgs) Handles John_Data.MouseHover
        If JobOrdersBindingSource.Filter <> "(Artist LIKE 'John')" And (SearchBox.Text <> "" Or SearchBox.Text <> "Search...") Then
            JobOrdersBindingSource.Filter = "(Artist LIKE 'John')"
            filterArtistJOTotal("John")
            oldFilter = "Artist LIKE 'John'"
            filterUsr = "John"
        End If
    End Sub

    Private Sub March_Data_MouseHover(sender As Object, e As System.EventArgs) Handles March_Data.MouseHover
        If JobOrdersBindingSource.Filter <> "(Artist LIKE 'March')" And (SearchBox.Text <> "" Or SearchBox.Text <> "Search...") Then
            JobOrdersBindingSource.Filter = "(Artist LIKE 'March')"
            filterArtistJOTotal("March")
            oldFilter = "Artist LIKE 'March'"
            filterUsr = "March"
        End If
    End Sub

    Private Sub Marn_Data_MouseHover(sender As Object, e As System.EventArgs) Handles March_Data.MouseHover
        If JobOrdersBindingSource.Filter <> "(Artist LIKE 'Marn')" And (SearchBox.Text <> "" Or SearchBox.Text <> "Search...") Then
            JobOrdersBindingSource.Filter = "(Artist LIKE 'Marn')"
            filterArtistJOTotal("Marn")
            oldFilter = "Artist LIKE 'Marn'"
            filterUsr = "Marn"
        End If
    End Sub

    Private Sub Mike_Data_MouseHover(sender As Object, e As System.EventArgs) Handles Mike_Data.MouseHover
        If JobOrdersBindingSource.Filter <> "(Artist LIKE 'Mike')" And (SearchBox.Text <> "" Or SearchBox.Text <> "Search...") Then
            JobOrdersBindingSource.Filter = "(Artist LIKE 'Mike')"
            filterArtistJOTotal("Mike")
            oldFilter = "Artist LIKE 'Mike'"
            filterUsr = "Mike"
        End If
    End Sub

    Private Sub Ayeng_Data_MouseHover(sender As Object, e As System.EventArgs) Handles Ayeng_Data.MouseHover
        If JobOrdersBindingSource.Filter <> "(Artist LIKE 'Ayeng')" And (SearchBox.Text <> "" Or SearchBox.Text <> "Search...") Then
            JobOrdersBindingSource.Filter = "(Artist LIKE 'Ayeng')"
            filterArtistJOTotal("Ayeng")
            oldFilter = "Artist LIKE 'Ayeng'"
            filterUsr = "Ayeng"
        End If
    End Sub

    Private Sub TabControls_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles TabContorls.SelectedIndexChanged
        Try
            If (SearchBox.Text <> "" Or SearchBox.Text <> "Search...") Then

                If TabContorls.SelectedTab Is View1 Or TabContorls.SelectedTab Is View2 Then
                    JobOrdersBindingSource.Filter = Nothing
                    filterUsr = ""
                ElseIf TabContorls.SelectedTab Is Jayson_Tab Then
                    JobOrdersBindingSource.Filter = "(Artist LIKE 'Jayson')"
                    filterArtistJOTotal("Jayson")
                    oldFilter = "Artist LIKE 'Jayson'"
                    filterUsr = "Jayson"
                ElseIf TabContorls.SelectedTab Is John_Tab Then
                    JobOrdersBindingSource.Filter = "(Artist LIKE 'John')"
                    filterArtistJOTotal("John")
                    oldFilter = "Artist LIKE 'John'"
                    filterUsr = "John"
                ElseIf TabContorls.SelectedTab Is March_Tab Then
                    JobOrdersBindingSource.Filter = "(Artist LIKE 'March')"
                    filterArtistJOTotal("March")
                    oldFilter = "Artist LIKE 'March'"
                    filterUsr = "March"
                ElseIf TabContorls.SelectedTab Is Marn_Tab Then
                    JobOrdersBindingSource.Filter = "(Artist LIKE 'Marn')"
                    filterArtistJOTotal("Marn")
                    oldFilter = "Artist LIKE 'Marn'"
                    filterUsr = "Marn"
                ElseIf TabContorls.SelectedTab Is Mike_Tab Then
                    JobOrdersBindingSource.Filter = "(Artist LIKE 'Mike')"
                    filterArtistJOTotal("Mike")
                    oldFilter = "Artist LIKE 'Mike'"
                    filterUsr = "Mike"
                ElseIf TabContorls.SelectedTab Is Ayeng_Tab Then
                    JobOrdersBindingSource.Filter = "(Artist LIKE 'Ayeng')"
                    filterArtistJOTotal("Ayeng")
                    oldFilter = "Artist LIKE 'Ayeng'"
                    filterUsr = "Ayeng"
                ElseIf TabContorls.SelectedTab Is dWeek_Tab Then
                    Dim Nweek As String = (DateTime.Now.DayOfWeek + 1)
                    Dim Nmonth As Integer = (DateTime.Now.Month())
                    Dim Nday As Integer = (DateTime.Now.Day() - Nweek)
                    Dim Nyear As Integer = (DateTime.Now.Year)
                    Dim last7Days As String = Nmonth.ToString & "/" & Nday.ToString & "/" & Nyear.ToString
                    Dim nowDate As String = DateTime.Now.ToString("MM/dd/yyyy")
                    JobOrdersBindingSource.Filter = "(DateRecieved >= #" & last7Days & "# AND DateRecieved <= #" & nowDate & "#)"
                    oldFilter = Nothing
                    filterUsr = ""
                ElseIf TabContorls.SelectedTab Is Tab_WIP Then
                    filterWIP({""})
                    filterUsr = ""
                    oldFilter = Nothing
                Else
                    If Not filterUsr = "" Then
                        JobOrdersBindingSource.Filter = Nothing
                        filterUsr = ""
                    End If
                End If
            End If

        Catch ex As Exception
            'error
        End Try
    End Sub

    Private Sub Save_Click(sender As System.Object, e As System.EventArgs) Handles Save.Click
        Try
            'Jobings Table
            JobOrdersBindingSource.EndEdit()
            JobOrdersTableAdapter.Update(_IW_JobOrdersDataSet.JobOrders)
            With Chart1
                .Series(0).Points.Clear()
                .DataSource = ""
                .DataSource = UsersBindingSource
                .Series(0).XValueMember = "Username"
                .Series(0).YValueMembers = "JOcount"
            End With
            MessageBox.Show("Data Saved!", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show("Error occured while saving!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Save_GotFocus(sender As Object, e As System.EventArgs) Handles Save.GotFocus
        AllowedEnter = True
    End Sub

    Private Sub Save_LostFocus(sender As Object, e As System.EventArgs) Handles Save.LostFocus
        AllowedEnter = False
    End Sub

    Private Sub Add_Click(sender As System.Object, e As System.EventArgs) Handles Add.Click
        JobOrdersBindingSource.AddNew()
        'IDLabel.Text = Total_JO + 1
        JONumberTextBox.Focus()
        DateRecievedDateTimePicker.Value = DateTime.Now
        DateRequiredDateTimePicker.Value = DateTime.Now
        'OutDateDateTimePicker.Value = DateTime.Now
    End Sub

    Private Sub DBlocate_GotFocus(sender As Object, e As System.EventArgs) Handles DBlocate.GotFocus
        DBlocate.Text = ""
        AllowedEnter = True
    End Sub

    Private Sub DBlocate_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles DBlocate.KeyDown
        If e.KeyCode = Keys.Enter Then
            dbSource1 = DBlocate.Text
            'save new databaseLocation
            Dim dotConfig As String = "<?xml version='1.0' encoding='utf-8' ?>" & vbNewLine & _
"<configuration>" & vbNewLine & _
vbTab & "<configSections>" & vbNewLine & _
vbTab & "</configSections>" & vbNewLine & _
vbTab & "<connectionStrings>" & vbNewLine & _
    vbTab & vbTab & "<add name='$safeprojectname$.My.MySettings.IW_JobOrdersConnectionString'" & vbNewLine & _
        vbTab & vbTab & vbTab & "connectionString='Provider=Microsoft.ACE.OLEDB.12.0;Data Source= " & dbSource1 & "\IW-JobOrders.accdb'" & vbNewLine & _
        vbTab & vbTab & vbTab & "providerName='System.Data.OleDb'/>" & vbNewLine & _
vbTab & "</connectionStrings>" & vbNewLine & _
vbTab & "<startup>" & vbNewLine & _
    vbTab & vbTab & "<supportedRuntime version='v4.0' sku='.NETFramework,Version=v4.0,Profile=Client'/>" & vbNewLine & _
vbTab & "</startup>" & vbNewLine & _
"</configuration>"

            '---try to save---

            If System.IO.File.Exists(SetupDB.setUpFolder & "\" & Login.dataName) = True Then
                'File exist
            Else
                'File not exist
            End If
            'save anyway

            Dim objWriter As New System.IO.StreamWriter(SetupDB.setUpFolder & "\" & Login.dataName)

            objWriter.Write(dotConfig)
            objWriter.Close()
            'settings generated
            MessageBox.Show("Database location change.", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information)
            DBlocate.Text = "Database Saved!"
        End If
    End Sub

    Private Sub DBlocate_LostFocus(sender As Object, e As System.EventArgs) Handles DBlocate.LostFocus
        SettingBindingSource.Filter = "(DBLocation LIKE '')"
        If SettingBindingSource.Count <> 0 Then
            DBlocate.Text = DBLocationTextBox.Text
        ElseIf DBlocate.Text = "" Then
            DBlocate.Text = "Z:\Database"
        End If
        AllowedEnter = False
    End Sub

    Private Sub DBlocate_MouseHover(sender As Object, e As System.EventArgs) Handles DBlocate.MouseHover
        CustomToolTip.SetToolTip(DBlocate, "New Database Location")
    End Sub

    Private Sub Btn_Messenger_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Messenger.Click
        chatActive = True
        settingActive = True
        Message.Show()
    End Sub

    Private Sub Btn_Messenger_MouseHover(sender As Object, e As System.EventArgs) Handles Btn_Messenger.MouseHover
        CustomToolTip.SetToolTip(Btn_Messenger, "Messenger")
    End Sub

    Private Sub Btn_Settings_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Settings.Click
        ChangePass.Show()
        'NewChangePass.Show()
    End Sub

    Private Sub Btn_Settings_MouseHover(sender As Object, e As System.EventArgs) Handles Btn_Settings.MouseHover
        CustomToolTip.SetToolTip(Btn_Settings, "Settings")
    End Sub

    Private Sub dWeek_Data_MouseHover(sender As Object, e As System.EventArgs) Handles dWeek_Data.MouseHover
        If (filerTotal <= 0) Then
            Dim Nweek As String = (DateTime.Now.DayOfWeek + 1)
            Dim Nmonth As Integer = (DateTime.Now.Month())
            Dim Nday As Integer = (DateTime.Now.Day() - Nweek)
            Dim Nyear As Integer = (DateTime.Now.Year)
            Dim last7Days As String = Nmonth.ToString & "/" & Nday.ToString & "/" & Nyear.ToString
            Dim nowDate As String = DateTime.Now.ToString("dd/MM/yyyy")
            If JobOrdersBindingSource.Filter <> "(DateRecieved >= #" & last7Days & "# AND DateRecieved <= #" & nowDate & "#)" And (SearchBox.Text <> "" Or SearchBox.Text <> "Search...") Then
                JobOrdersBindingSource.Filter = "(DateRecieved >= #" & last7Days & "# AND DateRecieved <= #" & nowDate & "#)"
                oldFilter = Nothing
            End If
        End If
    End Sub

    Public Sub filterWeek(Optional ByVal name() As String = Nothing)
        Dim Nweek As String = (DateTime.Now.DayOfWeek + 1)
        Dim Nmonth As Integer = (DateTime.Now.Month())
        Dim Nday As Integer = (DateTime.Now.Day() - Nweek)
        Dim Nyear As Integer = (DateTime.Now.Year)
        Dim last7Days As String = Nmonth.ToString & "/" & Nday.ToString & "/" & Nyear.ToString
        Dim nowDate As String = DateTime.Now.ToString("MM/dd/yyyy")
        Dim fils As String = " AND ("
        Dim names = name.Count - 1
        While names >= 0
            If names = 0 Then
                fils = fils & "(Artist LIKE '" & name(names) & "'))"
            Else
                fils = fils & "(Artist LIKE '" & name(names) & "') OR "
            End If
            names = names - 1
        End While

        JobOrdersBindingSource.Filter = "(DateRecieved >= #" & last7Days & "# AND DateRecieved <= #" & nowDate & "#)" & fils & ""
        oldFilter = Nothing
    End Sub


    Public Sub filterWIP(Optional ByVal name() As String = Nothing)
        Dim fils As String = " AND ("
        Dim names = name.Count - 1
        If name(0) <> "" Then
            While names >= 0
                If names = 0 Then
                    fils = fils & "(Artist LIKE '" & name(names) & "'))"
                Else
                    fils = fils & "(Artist LIKE '" & name(names) & "') OR "
                End If
                names = names - 1
            End While
        Else
            fils = ""
        End If

        JobOrdersBindingSource.Filter = "((Status <> 'Done') OR (Status LIKE '')) " & fils & ""
        oldFilter = Nothing
    End Sub

    Private Sub AutoStartCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles AutoStartCheckBox.TextChanged
        If AutoStartCheckBox.Checked Then
            autoStartX(True)
            AutoStartTextBox.Text = "Yes"
        Else
            autoStartX(False)
            AutoStartTextBox.Text = "No"
        End If
        Try
            'Jobings Table
            SettingBindingSource.EndEdit()
            SettingTableAdapter.Update(_IW_JobOrdersDataSet.Setting)
        Catch ex As Exception
            'errorDB
            MessageBox.Show("Error.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Public Sub autoStartX(AddRemove As Boolean)
        If AddRemove Then 'add
            My.Computer.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True).SetValue(Application.ProductName, Application.ExecutablePath)
        Else 'remove
            My.Computer.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True).DeleteValue(Application.ProductName)
        End If
    End Sub

    Private Sub WIP_Data_MouseHover(sender As Object, e As System.EventArgs) Handles WIP_Data.MouseHover
        If JobOrdersBindingSource.Filter <> "(Status <> 'Done')" Then
            filterWIP({""})
        End If
    End Sub

    Private Sub OpenWindow(own As String, type As String)
        Dim NewWindow As New AInfo
        infoWindow.Add(NewWindow)
        NewWindow.Show()
        If type = "week" Then
            NewWindow.filterWeek({own})
            NewWindow.Top_Title.Text = own & " — Recieved this Week"
        ElseIf type = "wip" Then
            NewWindow.filterWIP({own})
            NewWindow.Top_Title.Text = own & " — Work in Process"
        Else
            NewWindow.JobOrdersBindingSource.Filter = Nothing
            NewWindow.Top_Title.Text = "Information"
        End If
        infoFilType = type
    End Sub

    Private Sub OpenWindows(ByVal Count As Int32)
        For i = 1 To Count
            OpenWindow(i, "")
        Next
    End Sub

    Private Sub CloseWindows()
        For Each Window In infoWindow
            Window.Close()
            Window.Dispose()
        Next
        infoWindow.Clear()
    End Sub

    Private Sub Btn_WIP_Click(sender As System.Object, e As System.EventArgs) Handles Btn_WIP.Click
        OpenWindow(filterUsr, "wip")
    End Sub

    Private Sub Btn_DWeek_Click(sender As System.Object, e As System.EventArgs) Handles Btn_DWeek.Click
        OpenWindow(filterUsr, "week")
    End Sub

    Private Sub CheckEdge_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEdge.CheckedChanged
        If CheckEdge.Checked Then
            isRound = True
        Else
            isRound = False
        End If
    End Sub

    Private Sub Btn_Reset_Db_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Reset_Db.Click
        If System.IO.File.Exists(SetupDB.setUpFolder & "\" & SetupDB.setFile) Or System.IO.File.Exists(SetupDB.setUpFolder & "\" & Login.dataName) Then
            Try
                My.Computer.FileSystem.DeleteFile(SetupDB.setUpFolder & "\" & SetupDB.setFile)
                My.Computer.FileSystem.DeleteFile(SetupDB.setUpFolder & "\" & Login.dataName)
            Catch ex As Exception
                MessageBox.Show("Can't reset.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End If
        Try
            Login.TopMost = False
            Login.Visible = False
            Login.Close()
            SetupDB.TopMost = False
            SetupDB.Visible = False
            SetupDB.Close()
            Application.Restart()
        Catch ex As Exception
            MessageBox.Show("Reset complete! Restart the program.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub Btn_Reset_MouseHover(sender As Object, e As System.EventArgs) Handles Btn_Reset_Db.MouseHover
        CustomToolTip.SetToolTip(Btn_Reset_Db, "Generalist")
    End Sub

    Private Sub Btn_Email_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Email.Click
        Dim NewMail As New EMail
        sendMail.Add(NewMail)
        NewMail.Show()
    End Sub

    Private Sub CloseMails()
        For Each Window In sendMail
            Window.Close()
            Window.Dispose()
        Next
        sendMail.Clear()
    End Sub

    Private Sub Btn_Email_MouseHover(sender As Object, e As System.EventArgs) Handles Btn_Email.MouseHover
        CustomToolTip.SetToolTip(Btn_Email, "Send Email")
    End Sub

    Private Sub E_Pic_MouseDoubleClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles E_Pic.MouseDoubleClick
        With OpenProFile
            .Title = "Open New Profile Picture"
            .Filter = "Pictures |*.jpg"
            .ShowDialog()
        End With
    End Sub

    Private Sub OpenProFile_FileOk(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles OpenProFile.FileOk
        newPic = OpenProFile.FileName
        Try
            System.IO.File.Copy(newPic, proPic, True)
            proChange = True
        Catch ex As Exception
            MessageBox.Show("Upload failed.", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub E_Pic_MouseHover(sender As Object, e As System.EventArgs) Handles E_Pic.MouseHover
        CustomToolTip.SetToolTip(E_Pic, "1:1 Profile")
        Btn_x.Visible = True
    End Sub

    Private Sub Btn_x_Click(sender As System.Object, e As System.EventArgs) Handles Btn_x.Click
        Try
            My.Computer.FileSystem.DeleteFile(SetupDB.setUpFolder & "\Profile.jpg")
            E_Pic.ImageLocation = dPCPic
        Catch ex As Exception
            MessageBox.Show("Can't remove.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Btn_x_MouseHover(sender As Object, e As System.EventArgs) Handles Btn_x.MouseHover
        Btn_x.ForeColor = Color.Red
    End Sub

    Private Sub Btn_x_MouseLeave(sender As Object, e As System.EventArgs) Handles Btn_x.MouseLeave
        Btn_x.ForeColor = Color.Gray
    End Sub

    Private Sub Timer_Refresh_Tick(sender As System.Object, e As System.EventArgs) Handles Timer_Refresh.Tick
        If updateDB Then
            If Top_Error.Visible = True Then
                Top_Error.Visible = False
            End If
            Try
                'Update Joborders
                JobOrdersBindingSource.EndEdit()
                JobOrdersTableAdapter.Update(_IW_JobOrdersDataSet.JobOrders)
            Catch ex As Exception
                Top_Error.Visible = True
            End Try
        End If
    End Sub

End Class
