﻿Public Class Logout

    Dim CustomToolTip As System.Windows.Forms.ToolTip = New System.Windows.Forms.ToolTip()
    Dim topUP As Boolean = False

    Private Sub outKey_Click(sender As System.Object, e As System.EventArgs) Handles outKey.Click
        'Dim result As Integer = MessageBox.Show("Save Changes and Logout?", "Logout", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button3)
        'If result = DialogResult.Cancel Then
        '    'MessageBox.Show("Canceled")

        'ElseIf result = DialogResult.No Then
        '    'MessageBox.Show("No pressed")

        Login.Show() 'more secure and less bug
        Main.Close()
        Application.Restart()
        Close()

        'ElseIf result = DialogResult.Yes Then
        '    Try
        '        Main.JobOrdersBindingSource.EndEdit()
        '        Main.JobOrdersTableAdapter.Update(Main._IW_JobOrdersDataSet.JobOrders)
        '        'MessageBox.Show("Data Saved")
        '        Login.UsersBindingSource.Filter = Nothing
        '        Login.Show()
        '        Main.Close()
        '        Close()
        '    Catch ex As Exception
        '        MessageBox.Show("Error Occured when saving.")
        '    End Try
        'End If
    End Sub

    Private Sub Logout_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If topUP Then
            Main.TopMost = True
        End If
        Main.Enabled = True
    End Sub

    Private Sub Logout_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Close()
        End If
    End Sub

    Private Sub Save_Click(sender As System.Object, e As System.EventArgs) Handles Save.Click
        Try
            Main.JobOrdersBindingSource.EndEdit()
            Main.JobOrdersTableAdapter.Update(Main._IW_JobOrdersDataSet.JobOrders)
            'MessageBox.Show("Data Saved")
            Login.Show()
            Main.Close()
            Close()
        Catch ex As Exception
            MessageBox.Show("Error Occured when saving.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub outKey_MouseHover(sender As Object, e As System.EventArgs) Handles outKey.MouseHover
        CustomToolTip.SetToolTip(outKey, "Logout")
    End Sub

    Private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick
        'rounding borders include
        If Main.Enabled = False Then
            If Main.MouseButtons = Windows.Forms.MouseButtons.Right Then
                Close()
            End If
        End If
        Dim p As New Drawing2D.GraphicsPath()
        p.StartFigure()
        p.AddArc(New Rectangle(0, 0, 20, 20), 180, 90)
        p.AddLine(20, 0, Me.Width - 20, 0)
        p.AddArc(New Rectangle(Me.Width - 20, 0, 20, 20), -90, 90)
        p.AddLine(Me.Width, 20, Me.Width, Me.Height - 20)
        p.AddArc(New Rectangle(Me.Width - 20, Me.Height - 20, 20, 20), 0, 90)
        p.AddLine(Me.Width - 20, Me.Height, 20, Me.Height)
        p.AddArc(New Rectangle(0, Me.Height - 20, 20, 20), 90, 90)
        p.CloseFigure()
        Me.Region = New Region(p)
    End Sub

    Private Sub Logout_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Timer1.Start()
        If Main.TopMost Then
            Main.TopMost = False
            topUP = True
        End If
        Main.Enabled = False
    End Sub

    Private Sub TopPanel_MouseClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles TopPanel.MouseClick
        Close()
    End Sub

    Private Sub Top_Title_Click(sender As System.Object, e As System.EventArgs) Handles Top_Title.Click
        Close()
    End Sub

    Private Sub Txt_Note_Click(sender As System.Object, e As System.EventArgs) Handles Txt_Note.Click
        Close()
    End Sub

    Private Sub BottomPanel_MouseClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles BottomPanel.MouseClick
        Close()
    End Sub

    Private Sub Copyright_Click(sender As System.Object, e As System.EventArgs) Handles Copyright.Click
        Close()
    End Sub

    Private Sub Logout_MouseClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseClick
        Close()
    End Sub

End Class