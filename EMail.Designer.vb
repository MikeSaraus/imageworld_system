﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EMail
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EMail))
        Me.BackgroundWorkerSendMail = New System.ComponentModel.BackgroundWorker()
        Me.OpenFileDialogAttachment = New System.Windows.Forms.OpenFileDialog()
        Me.BottomPanel = New System.Windows.Forms.Panel()
        Me.Btn_Logo = New System.Windows.Forms.PictureBox()
        Me.Copyright = New System.Windows.Forms.Label()
        Me.TopPanel = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Btn_Exit = New System.Windows.Forms.PictureBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Top_Title = New System.Windows.Forms.Label()
        Me.destination = New System.Windows.Forms.TextBox()
        Me.subject = New System.Windows.Forms.TextBox()
        Me.attach = New System.Windows.Forms.TextBox()
        Me.body = New System.Windows.Forms.TextBox()
        Me.Btn_Send = New System.Windows.Forms.Button()
        Me.Btn_File = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Btn_Reset = New System.Windows.Forms.Button()
        Me.Pic = New System.Windows.Forms.PictureBox()
        Me.EmailTableAdapter = New $safeprojectname$._IW_JobOrdersDataSetTableAdapters.EmailTableAdapter()
        Me.TableAdapterManager = New $safeprojectname$._IW_JobOrdersDataSetTableAdapters.TableAdapterManager()
        Me.EmailBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me._IW_JobOrdersDataSet = New $safeprojectname$._IW_JobOrdersDataSet()
        Me.EmailTextBox = New System.Windows.Forms.TextBox()
        Me.PasswordTextBox = New System.Windows.Forms.TextBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SNameTextBox = New System.Windows.Forms.TextBox()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.RectangleShape1 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.TransferProgressBar = New System.Windows.Forms.ProgressBar()
        Me.BottomPanel.SuspendLayout()
        CType(Me.Btn_Logo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TopPanel.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Btn_Exit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Pic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmailBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._IW_JobOrdersDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BackgroundWorkerSendMail
        '
        '
        'OpenFileDialogAttachment
        '
        Me.OpenFileDialogAttachment.Multiselect = True
        Me.OpenFileDialogAttachment.RestoreDirectory = True
        Me.OpenFileDialogAttachment.ShowReadOnly = True
        Me.OpenFileDialogAttachment.SupportMultiDottedExtensions = True
        '
        'BottomPanel
        '
        Me.BottomPanel.BackColor = System.Drawing.Color.Black
        Me.BottomPanel.Controls.Add(Me.Btn_Logo)
        Me.BottomPanel.Controls.Add(Me.Copyright)
        Me.BottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BottomPanel.Location = New System.Drawing.Point(0, 232)
        Me.BottomPanel.Name = "BottomPanel"
        Me.BottomPanel.Size = New System.Drawing.Size(335, 15)
        Me.BottomPanel.TabIndex = 72
        '
        'Btn_Logo
        '
        Me.Btn_Logo.BackColor = System.Drawing.Color.Black
        Me.Btn_Logo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Logo.ErrorImage = Nothing
        Me.Btn_Logo.Image = CType(resources.GetObject("Btn_Logo.Image"), System.Drawing.Image)
        Me.Btn_Logo.InitialImage = CType(resources.GetObject("Btn_Logo.InitialImage"), System.Drawing.Image)
        Me.Btn_Logo.Location = New System.Drawing.Point(258, 0)
        Me.Btn_Logo.Name = "Btn_Logo"
        Me.Btn_Logo.Size = New System.Drawing.Size(71, 20)
        Me.Btn_Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Logo.TabIndex = 53
        Me.Btn_Logo.TabStop = False
        '
        'Copyright
        '
        Me.Copyright.AutoSize = True
        Me.Copyright.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Copyright.ForeColor = System.Drawing.Color.DimGray
        Me.Copyright.Location = New System.Drawing.Point(0, 1)
        Me.Copyright.Name = "Copyright"
        Me.Copyright.Size = New System.Drawing.Size(44, 13)
        Me.Copyright.TabIndex = 0
        Me.Copyright.Text = "@ 2017"
        '
        'TopPanel
        '
        Me.TopPanel.BackColor = System.Drawing.Color.Black
        Me.TopPanel.Controls.Add(Me.PictureBox1)
        Me.TopPanel.Controls.Add(Me.Btn_Exit)
        Me.TopPanel.Controls.Add(Me.Label5)
        Me.TopPanel.Controls.Add(Me.Top_Title)
        Me.TopPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me.TopPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopPanel.Name = "TopPanel"
        Me.TopPanel.Size = New System.Drawing.Size(335, 25)
        Me.TopPanel.TabIndex = 71
        '
        'PictureBox1
        '
        Me.PictureBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox1.ErrorImage = Nothing
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.InitialImage = CType(resources.GetObject("PictureBox1.InitialImage"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(4, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 53
        Me.PictureBox1.TabStop = False
        '
        'Btn_Exit
        '
        Me.Btn_Exit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Exit.ErrorImage = Nothing
        Me.Btn_Exit.Image = CType(resources.GetObject("Btn_Exit.Image"), System.Drawing.Image)
        Me.Btn_Exit.InitialImage = CType(resources.GetObject("Btn_Exit.InitialImage"), System.Drawing.Image)
        Me.Btn_Exit.Location = New System.Drawing.Point(308, 3)
        Me.Btn_Exit.Name = "Btn_Exit"
        Me.Btn_Exit.Size = New System.Drawing.Size(20, 20)
        Me.Btn_Exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Exit.TabIndex = 52
        Me.Btn_Exit.TabStop = False
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Red
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Snow
        Me.Label5.Location = New System.Drawing.Point(132, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(97, 25)
        Me.Label5.TabIndex = 81
        Me.Label5.Text = "Admin Mode"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label5.Visible = False
        '
        'Top_Title
        '
        Me.Top_Title.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.Top_Title.AutoSize = True
        Me.Top_Title.BackColor = System.Drawing.Color.Black
        Me.Top_Title.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.Top_Title.Location = New System.Drawing.Point(28, 7)
        Me.Top_Title.Name = "Top_Title"
        Me.Top_Title.Size = New System.Drawing.Size(60, 13)
        Me.Top_Title.TabIndex = 51
        Me.Top_Title.Text = "Send Email"
        Me.Top_Title.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'destination
        '
        Me.destination.Location = New System.Drawing.Point(99, 37)
        Me.destination.Name = "destination"
        Me.destination.Size = New System.Drawing.Size(223, 20)
        Me.destination.TabIndex = 1
        '
        'subject
        '
        Me.subject.Location = New System.Drawing.Point(99, 63)
        Me.subject.Name = "subject"
        Me.subject.Size = New System.Drawing.Size(223, 20)
        Me.subject.TabIndex = 2
        '
        'attach
        '
        Me.attach.BackColor = System.Drawing.Color.White
        Me.attach.Location = New System.Drawing.Point(99, 89)
        Me.attach.Name = "attach"
        Me.attach.ReadOnly = True
        Me.attach.Size = New System.Drawing.Size(223, 20)
        Me.attach.TabIndex = 3
        '
        'body
        '
        Me.body.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.body.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList
        Me.body.Location = New System.Drawing.Point(99, 115)
        Me.body.Multiline = True
        Me.body.Name = "body"
        Me.body.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.body.Size = New System.Drawing.Size(223, 66)
        Me.body.TabIndex = 4
        Me.body.Text = "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "--Mike Angelo Saraus"
        '
        'Btn_Send
        '
        Me.Btn_Send.BackColor = System.Drawing.Color.SteelBlue
        Me.Btn_Send.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Btn_Send.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Send.ForeColor = System.Drawing.Color.Snow
        Me.Btn_Send.Location = New System.Drawing.Point(268, 189)
        Me.Btn_Send.Name = "Btn_Send"
        Me.Btn_Send.Size = New System.Drawing.Size(53, 21)
        Me.Btn_Send.TabIndex = 5
        Me.Btn_Send.Text = "Send"
        Me.Btn_Send.UseVisualStyleBackColor = False
        '
        'Btn_File
        '
        Me.Btn_File.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Btn_File.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.Btn_File.Location = New System.Drawing.Point(295, 89)
        Me.Btn_File.Name = "Btn_File"
        Me.Btn_File.Size = New System.Drawing.Size(27, 20)
        Me.Btn_File.TabIndex = 3
        Me.Btn_File.Text = "....."
        Me.Btn_File.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(21, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(20, 13)
        Me.Label1.TabIndex = 73
        Me.Label1.Text = "To"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(22, 66)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 13)
        Me.Label2.TabIndex = 74
        Me.Label2.Text = "Subject"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(23, 92)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(67, 13)
        Me.Label3.TabIndex = 75
        Me.Label3.Text = "Attachement"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(25, 118)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(50, 13)
        Me.Label4.TabIndex = 76
        Me.Label4.Text = "Message"
        '
        'Btn_Reset
        '
        Me.Btn_Reset.BackColor = System.Drawing.Color.SteelBlue
        Me.Btn_Reset.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Btn_Reset.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Reset.ForeColor = System.Drawing.Color.Snow
        Me.Btn_Reset.Location = New System.Drawing.Point(210, 189)
        Me.Btn_Reset.Name = "Btn_Reset"
        Me.Btn_Reset.Size = New System.Drawing.Size(53, 21)
        Me.Btn_Reset.TabIndex = 6
        Me.Btn_Reset.Text = "Clear"
        Me.Btn_Reset.UseVisualStyleBackColor = False
        '
        'Pic
        '
        Me.Pic.BackColor = System.Drawing.SystemColors.Control
        Me.Pic.Image = CType(resources.GetObject("Pic.Image"), System.Drawing.Image)
        Me.Pic.Location = New System.Drawing.Point(26, 136)
        Me.Pic.Name = "Pic"
        Me.Pic.Size = New System.Drawing.Size(40, 45)
        Me.Pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Pic.TabIndex = 80
        Me.Pic.TabStop = False
        Me.Pic.Visible = False
        '
        'EmailTableAdapter
        '
        Me.EmailTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.EmailTableAdapter = Me.EmailTableAdapter
        Me.TableAdapterManager.JobOrdersTableAdapter = Nothing
        Me.TableAdapterManager.SettingTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = $safeprojectname$._IW_JobOrdersDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UsersTableAdapter = Nothing
        '
        'EmailBindingSource
        '
        Me.EmailBindingSource.DataMember = "Email"
        Me.EmailBindingSource.DataSource = Me._IW_JobOrdersDataSet
        '
        '_IW_JobOrdersDataSet
        '
        Me._IW_JobOrdersDataSet.DataSetName = "_IW_JobOrdersDataSet"
        Me._IW_JobOrdersDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'EmailTextBox
        '
        Me.EmailTextBox.BackColor = System.Drawing.Color.Snow
        Me.EmailTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmailBindingSource, "Email", True))
        Me.EmailTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EmailTextBox.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.EmailTextBox.Location = New System.Drawing.Point(8, 212)
        Me.EmailTextBox.Name = "EmailTextBox"
        Me.EmailTextBox.Size = New System.Drawing.Size(150, 18)
        Me.EmailTextBox.TabIndex = 11
        Me.EmailTextBox.Text = "Email"
        '
        'PasswordTextBox
        '
        Me.PasswordTextBox.BackColor = System.Drawing.Color.Snow
        Me.PasswordTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmailBindingSource, "Password", True))
        Me.PasswordTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PasswordTextBox.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.PasswordTextBox.Location = New System.Drawing.Point(172, 212)
        Me.PasswordTextBox.Name = "PasswordTextBox"
        Me.PasswordTextBox.Size = New System.Drawing.Size(150, 18)
        Me.PasswordTextBox.TabIndex = 12
        Me.PasswordTextBox.Text = "Password"
        '
        'Timer1
        '
        '
        'SNameTextBox
        '
        Me.SNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EmailBindingSource, "SName", True))
        Me.SNameTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SNameTextBox.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.SNameTextBox.Location = New System.Drawing.Point(8, 190)
        Me.SNameTextBox.Name = "SNameTextBox"
        Me.SNameTextBox.Size = New System.Drawing.Size(190, 18)
        Me.SNameTextBox.TabIndex = 10
        Me.SNameTextBox.Text = "Sender Name"
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.RectangleShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(335, 247)
        Me.ShapeContainer1.TabIndex = 84
        Me.ShapeContainer1.TabStop = False
        '
        'RectangleShape1
        '
        Me.RectangleShape1.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.RectangleShape1.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.RectangleShape1.BorderColor = System.Drawing.Color.Transparent
        Me.RectangleShape1.Location = New System.Drawing.Point(-2, 185)
        Me.RectangleShape1.Name = "RectangleShape1"
        Me.RectangleShape1.Size = New System.Drawing.Size(343, 55)
        Me.RectangleShape1.Visible = False
        '
        'TransferProgressBar
        '
        Me.TransferProgressBar.Location = New System.Drawing.Point(8, 212)
        Me.TransferProgressBar.Name = "TransferProgressBar"
        Me.TransferProgressBar.Size = New System.Drawing.Size(313, 18)
        Me.TransferProgressBar.Step = 1
        Me.TransferProgressBar.TabIndex = 85
        Me.TransferProgressBar.Value = 1
        Me.TransferProgressBar.Visible = False
        '
        'EMail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(335, 247)
        Me.Controls.Add(Me.TransferProgressBar)
        Me.Controls.Add(Me.SNameTextBox)
        Me.Controls.Add(Me.PasswordTextBox)
        Me.Controls.Add(Me.EmailTextBox)
        Me.Controls.Add(Me.Btn_Reset)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Btn_File)
        Me.Controls.Add(Me.Btn_Send)
        Me.Controls.Add(Me.body)
        Me.Controls.Add(Me.attach)
        Me.Controls.Add(Me.subject)
        Me.Controls.Add(Me.destination)
        Me.Controls.Add(Me.BottomPanel)
        Me.Controls.Add(Me.TopPanel)
        Me.Controls.Add(Me.Pic)
        Me.Controls.Add(Me.ShapeContainer1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "EMail"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Send Email"
        Me.BottomPanel.ResumeLayout(False)
        Me.BottomPanel.PerformLayout()
        CType(Me.Btn_Logo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TopPanel.ResumeLayout(False)
        Me.TopPanel.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Btn_Exit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Pic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmailBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._IW_JobOrdersDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BackgroundWorkerSendMail As System.ComponentModel.BackgroundWorker
    Friend WithEvents OpenFileDialogAttachment As System.Windows.Forms.OpenFileDialog
    Friend WithEvents BottomPanel As System.Windows.Forms.Panel
    Friend WithEvents Btn_Logo As System.Windows.Forms.PictureBox
    Friend WithEvents Copyright As System.Windows.Forms.Label
    Friend WithEvents TopPanel As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Btn_Exit As System.Windows.Forms.PictureBox
    Friend WithEvents Top_Title As System.Windows.Forms.Label
    Friend WithEvents destination As System.Windows.Forms.TextBox
    Friend WithEvents subject As System.Windows.Forms.TextBox
    Friend WithEvents attach As System.Windows.Forms.TextBox
    Friend WithEvents body As System.Windows.Forms.TextBox
    Friend WithEvents Btn_Send As System.Windows.Forms.Button
    Friend WithEvents Btn_File As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Btn_Reset As System.Windows.Forms.Button
    Friend WithEvents _IW_JobOrdersDataSet As $safeprojectname$._IW_JobOrdersDataSet
    Friend WithEvents EmailBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EmailTableAdapter As $safeprojectname$._IW_JobOrdersDataSetTableAdapters.EmailTableAdapter
    Friend WithEvents TableAdapterManager As $safeprojectname$._IW_JobOrdersDataSetTableAdapters.TableAdapterManager
    Friend WithEvents Pic As System.Windows.Forms.PictureBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents EmailTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PasswordTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents SNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents RectangleShape1 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents TransferProgressBar As System.Windows.Forms.ProgressBar
End Class
