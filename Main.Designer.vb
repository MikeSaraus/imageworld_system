﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OutDateLabel As System.Windows.Forms.Label
        Dim ArtistLabel As System.Windows.Forms.Label
        Dim InstructionsLabel As System.Windows.Forms.Label
        Dim OutputLabel As System.Windows.Forms.Label
        Dim ColorsLabel As System.Windows.Forms.Label
        Dim StatusLabel As System.Windows.Forms.Label
        Dim UnitLabel As System.Windows.Forms.Label
        Dim QuantityLabel As System.Windows.Forms.Label
        Dim TypeLabel As System.Windows.Forms.Label
        Dim CustomerLabel As System.Windows.Forms.Label
        Dim DBLocationLabel As System.Windows.Forms.Label
        Dim AutoStartLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim DataPoint1 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(0.0R, 0.0R)
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.TimerCountTotalJO = New System.Windows.Forms.Timer(Me.components)
        Me.Label_Total_JO = New System.Windows.Forms.Label()
        Me.TopPanel = New System.Windows.Forms.Panel()
        Me.Top_Error = New System.Windows.Forms.Label()
        Me.Btn_Logo = New System.Windows.Forms.PictureBox()
        Me.Top_Title = New System.Windows.Forms.Label()
        Me.Btn_Minimize = New System.Windows.Forms.PictureBox()
        Me.Btn_Maximize = New System.Windows.Forms.PictureBox()
        Me.Btn_Exit = New System.Windows.Forms.PictureBox()
        Me.MainTitle = New System.Windows.Forms.Label()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.RectangleShape3 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.RectangleShape2 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.RectangleShape1 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.Logo = New System.Windows.Forms.PictureBox()
        Me.Btn_First = New System.Windows.Forms.PictureBox()
        Me.Btn_Previews = New System.Windows.Forms.PictureBox()
        Me.Btn_Next = New System.Windows.Forms.PictureBox()
        Me.Btn_Last = New System.Windows.Forms.PictureBox()
        Me.Copyright = New System.Windows.Forms.Label()
        Me.BottomPanel = New System.Windows.Forms.Panel()
        Me.SearchBox = New System.Windows.Forms.TextBox()
        Me.E_Name = New System.Windows.Forms.Label()
        Me.E_Pic = New System.Windows.Forms.PictureBox()
        Me.E_Mode = New System.Windows.Forms.Label()
        Me.TabContorls = New System.Windows.Forms.TabControl()
        Me.View1 = New System.Windows.Forms.TabPage()
        Me.Chart1 = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.JobOrdersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me._IW_JobOrdersDataSet = New $safeprojectname$._IW_JobOrdersDataSet()
        Me.OutDateDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.ArtistTextBox = New System.Windows.Forms.TextBox()
        Me.JONumberTextBox = New System.Windows.Forms.TextBox()
        Me.InstructionsTextBox = New System.Windows.Forms.TextBox()
        Me.OutputComboBox = New System.Windows.Forms.ComboBox()
        Me.DateRequiredLabel = New System.Windows.Forms.Label()
        Me.DateRequiredDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.DateRecievedLabel = New System.Windows.Forms.Label()
        Me.DateRecievedDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.ColorsComboBox = New System.Windows.Forms.ComboBox()
        Me.StatusTextBox = New System.Windows.Forms.TextBox()
        Me.UnitTextBox = New System.Windows.Forms.TextBox()
        Me.QuantityTextBox = New System.Windows.Forms.TextBox()
        Me.TypeTextBox = New System.Windows.Forms.TextBox()
        Me.CustomerTextBox = New System.Windows.Forms.TextBox()
        Me.JONumberLabel = New System.Windows.Forms.Label()
        Me.View2 = New System.Windows.Forms.TabPage()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn85 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn86 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn87 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn88 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn89 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn90 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn91 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn92 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn93 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn94 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn95 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn96 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Artist = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ayeng_Tab = New System.Windows.Forms.TabPage()
        Me.Ayeng_Data = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn28 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn29 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn42 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn43 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn56 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn57 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn70 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn71 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn72 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Jayson_Tab = New System.Windows.Forms.TabPage()
        Me.Jayson_Data = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.John_Tab = New System.Windows.Forms.TabPage()
        Me.John_Data = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn27 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.March_Tab = New System.Windows.Forms.TabPage()
        Me.March_Data = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn30 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn31 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn32 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn33 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn34 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn35 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn36 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn37 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn38 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn39 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn40 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn41 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Marn_Tab = New System.Windows.Forms.TabPage()
        Me.Marn_Data = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn44 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn45 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn46 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn47 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn48 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn49 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn50 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn51 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn52 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn53 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn54 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn55 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Mike_Tab = New System.Windows.Forms.TabPage()
        Me.Mike_Data = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn58 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn59 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn60 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn61 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn62 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn63 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn64 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn65 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn66 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn67 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn68 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn69 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dWeek_Tab = New System.Windows.Forms.TabPage()
        Me.dWeek_Data = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn73 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn74 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn75 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn76 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn77 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn78 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn79 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn80 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn81 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn82 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn83 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn84 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tab_WIP = New System.Windows.Forms.TabPage()
        Me.WIP_Data = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn97 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn98 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn99 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn100 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn101 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn102 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn103 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn104 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn105 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn106 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn107 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn108 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UsersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Add = New System.Windows.Forms.PictureBox()
        Me.Save = New System.Windows.Forms.PictureBox()
        Me.JONumberLabel2 = New System.Windows.Forms.Label()
        Me.Timer_Refresh = New System.Windows.Forms.Timer(Me.components)
        Me.Btn_Messenger = New System.Windows.Forms.PictureBox()
        Me.Btn_Settings = New System.Windows.Forms.PictureBox()
        Me.DBlocate = New System.Windows.Forms.TextBox()
        Me.JobOrdersTableAdapter = New $safeprojectname$._IW_JobOrdersDataSetTableAdapters.JobOrdersTableAdapter()
        Me.TableAdapterManager = New $safeprojectname$._IW_JobOrdersDataSetTableAdapters.TableAdapterManager()
        Me.UsersTableAdapter = New $safeprojectname$._IW_JobOrdersDataSetTableAdapters.UsersTableAdapter()
        Me.SettingTableAdapter = New $safeprojectname$._IW_JobOrdersDataSetTableAdapters.SettingTableAdapter()
        Me.DBLocationTextBox = New System.Windows.Forms.TextBox()
        Me.SettingBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AutoStartTextBox = New System.Windows.Forms.TextBox()
        Me.AutoStartCheckBox = New System.Windows.Forms.CheckBox()
        Me.Btn_Email = New System.Windows.Forms.PictureBox()
        Me.AdmTxt = New System.Windows.Forms.Label()
        Me.Btn_WIP = New System.Windows.Forms.Button()
        Me.Btn_DWeek = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CheckEdge = New System.Windows.Forms.CheckBox()
        Me.IWJobOrdersDataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Btn_Reset_Db = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.OpenProFile = New System.Windows.Forms.OpenFileDialog()
        Me.Btn_x = New System.Windows.Forms.Label()
        Me.JobOrdersBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        OutDateLabel = New System.Windows.Forms.Label()
        ArtistLabel = New System.Windows.Forms.Label()
        InstructionsLabel = New System.Windows.Forms.Label()
        OutputLabel = New System.Windows.Forms.Label()
        ColorsLabel = New System.Windows.Forms.Label()
        StatusLabel = New System.Windows.Forms.Label()
        UnitLabel = New System.Windows.Forms.Label()
        QuantityLabel = New System.Windows.Forms.Label()
        TypeLabel = New System.Windows.Forms.Label()
        CustomerLabel = New System.Windows.Forms.Label()
        DBLocationLabel = New System.Windows.Forms.Label()
        AutoStartLabel = New System.Windows.Forms.Label()
        Me.TopPanel.SuspendLayout()
        CType(Me.Btn_Logo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Btn_Minimize, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Btn_Maximize, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Btn_Exit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Btn_First, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Btn_Previews, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Btn_Next, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Btn_Last, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BottomPanel.SuspendLayout()
        CType(Me.E_Pic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabContorls.SuspendLayout()
        Me.View1.SuspendLayout()
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JobOrdersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._IW_JobOrdersDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.View2.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Ayeng_Tab.SuspendLayout()
        CType(Me.Ayeng_Data, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Jayson_Tab.SuspendLayout()
        CType(Me.Jayson_Data, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.John_Tab.SuspendLayout()
        CType(Me.John_Data, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.March_Tab.SuspendLayout()
        CType(Me.March_Data, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Marn_Tab.SuspendLayout()
        CType(Me.Marn_Data, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Mike_Tab.SuspendLayout()
        CType(Me.Mike_Data, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.dWeek_Tab.SuspendLayout()
        CType(Me.dWeek_Data, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Tab_WIP.SuspendLayout()
        CType(Me.WIP_Data, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Add, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Save, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Btn_Messenger, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Btn_Settings, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SettingBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Btn_Email, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.IWJobOrdersDataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JobOrdersBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OutDateLabel
        '
        OutDateLabel.AutoSize = True
        OutDateLabel.Location = New System.Drawing.Point(108, 319)
        OutDateLabel.Name = "OutDateLabel"
        OutDateLabel.Size = New System.Drawing.Size(50, 13)
        OutDateLabel.TabIndex = 47
        OutDateLabel.Text = "Out Date"
        '
        'ArtistLabel
        '
        ArtistLabel.AutoSize = True
        ArtistLabel.Location = New System.Drawing.Point(128, 294)
        ArtistLabel.Name = "ArtistLabel"
        ArtistLabel.Size = New System.Drawing.Size(30, 13)
        ArtistLabel.TabIndex = 45
        ArtistLabel.Text = "Artist"
        '
        'InstructionsLabel
        '
        InstructionsLabel.AutoSize = True
        InstructionsLabel.Location = New System.Drawing.Point(97, 228)
        InstructionsLabel.Name = "InstructionsLabel"
        InstructionsLabel.Size = New System.Drawing.Size(61, 13)
        InstructionsLabel.TabIndex = 48
        InstructionsLabel.Text = "Instructions"
        '
        'OutputLabel
        '
        OutputLabel.AutoSize = True
        OutputLabel.Location = New System.Drawing.Point(118, 267)
        OutputLabel.Name = "OutputLabel"
        OutputLabel.Size = New System.Drawing.Size(39, 13)
        OutputLabel.TabIndex = 72
        OutputLabel.Text = "Output"
        '
        'ColorsLabel
        '
        ColorsLabel.AutoSize = True
        ColorsLabel.Location = New System.Drawing.Point(121, 147)
        ColorsLabel.Name = "ColorsLabel"
        ColorsLabel.Size = New System.Drawing.Size(36, 13)
        ColorsLabel.TabIndex = 69
        ColorsLabel.Text = "Colors"
        '
        'StatusLabel
        '
        StatusLabel.AutoSize = True
        StatusLabel.Location = New System.Drawing.Point(120, 121)
        StatusLabel.Name = "StatusLabel"
        StatusLabel.Size = New System.Drawing.Size(37, 13)
        StatusLabel.TabIndex = 68
        StatusLabel.Text = "Status"
        '
        'UnitLabel
        '
        UnitLabel.AutoSize = True
        UnitLabel.Location = New System.Drawing.Point(305, 95)
        UnitLabel.Name = "UnitLabel"
        UnitLabel.Size = New System.Drawing.Size(26, 13)
        UnitLabel.TabIndex = 67
        UnitLabel.Text = "Unit"
        '
        'QuantityLabel
        '
        QuantityLabel.AutoSize = True
        QuantityLabel.Location = New System.Drawing.Point(110, 95)
        QuantityLabel.Name = "QuantityLabel"
        QuantityLabel.Size = New System.Drawing.Size(46, 13)
        QuantityLabel.TabIndex = 66
        QuantityLabel.Text = "Quantity"
        '
        'TypeLabel
        '
        TypeLabel.AutoSize = True
        TypeLabel.Location = New System.Drawing.Point(94, 69)
        TypeLabel.Name = "TypeLabel"
        TypeLabel.Size = New System.Drawing.Size(63, 13)
        TypeLabel.TabIndex = 65
        TypeLabel.Text = "Type of Job"
        '
        'CustomerLabel
        '
        CustomerLabel.AutoSize = True
        CustomerLabel.Location = New System.Drawing.Point(105, 43)
        CustomerLabel.Name = "CustomerLabel"
        CustomerLabel.Size = New System.Drawing.Size(51, 13)
        CustomerLabel.TabIndex = 64
        CustomerLabel.Text = "Customer"
        '
        'DBLocationLabel
        '
        DBLocationLabel.AutoSize = True
        DBLocationLabel.Location = New System.Drawing.Point(1339, 529)
        DBLocationLabel.Name = "DBLocationLabel"
        DBLocationLabel.Size = New System.Drawing.Size(66, 13)
        DBLocationLabel.TabIndex = 0
        DBLocationLabel.Text = "DBLocation:"
        '
        'AutoStartLabel
        '
        AutoStartLabel.AutoSize = True
        AutoStartLabel.Location = New System.Drawing.Point(1339, 504)
        AutoStartLabel.Name = "AutoStartLabel"
        AutoStartLabel.Size = New System.Drawing.Size(57, 13)
        AutoStartLabel.TabIndex = 0
        AutoStartLabel.Text = "Auto Start:"
        '
        'TimerCountTotalJO
        '
        '
        'Label_Total_JO
        '
        Me.Label_Total_JO.BackColor = System.Drawing.Color.Yellow
        Me.Label_Total_JO.Location = New System.Drawing.Point(1166, 72)
        Me.Label_Total_JO.Name = "Label_Total_JO"
        Me.Label_Total_JO.Size = New System.Drawing.Size(60, 35)
        Me.Label_Total_JO.TabIndex = 30
        Me.Label_Total_JO.Text = "0"
        Me.Label_Total_JO.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'TopPanel
        '
        Me.TopPanel.BackColor = System.Drawing.Color.Black
        Me.TopPanel.Controls.Add(Me.Top_Error)
        Me.TopPanel.Controls.Add(Me.Btn_Logo)
        Me.TopPanel.Controls.Add(Me.Top_Title)
        Me.TopPanel.Controls.Add(Me.Btn_Minimize)
        Me.TopPanel.Controls.Add(Me.Btn_Maximize)
        Me.TopPanel.Controls.Add(Me.Btn_Exit)
        Me.TopPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me.TopPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopPanel.Name = "TopPanel"
        Me.TopPanel.Size = New System.Drawing.Size(1255, 25)
        Me.TopPanel.TabIndex = 47
        '
        'Top_Error
        '
        Me.Top_Error.AutoSize = True
        Me.Top_Error.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Top_Error.ForeColor = System.Drawing.Color.Red
        Me.Top_Error.Location = New System.Drawing.Point(1196, 6)
        Me.Top_Error.Name = "Top_Error"
        Me.Top_Error.Size = New System.Drawing.Size(29, 13)
        Me.Top_Error.TabIndex = 55
        Me.Top_Error.Text = "Error"
        Me.Top_Error.Visible = False
        '
        'Btn_Logo
        '
        Me.Btn_Logo.BackColor = System.Drawing.Color.Black
        Me.Btn_Logo.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.Btn_Logo.ErrorImage = Nothing
        Me.Btn_Logo.Image = CType(resources.GetObject("Btn_Logo.Image"), System.Drawing.Image)
        Me.Btn_Logo.InitialImage = CType(resources.GetObject("Btn_Logo.InitialImage"), System.Drawing.Image)
        Me.Btn_Logo.Location = New System.Drawing.Point(1235, 5)
        Me.Btn_Logo.Name = "Btn_Logo"
        Me.Btn_Logo.Size = New System.Drawing.Size(15, 15)
        Me.Btn_Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Logo.TabIndex = 54
        Me.Btn_Logo.TabStop = False
        Me.Btn_Logo.UseWaitCursor = True
        '
        'Top_Title
        '
        Me.Top_Title.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.Top_Title.AutoSize = True
        Me.Top_Title.BackColor = System.Drawing.Color.Black
        Me.Top_Title.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.Top_Title.Location = New System.Drawing.Point(606, 6)
        Me.Top_Title.Name = "Top_Title"
        Me.Top_Title.Size = New System.Drawing.Size(10, 13)
        Me.Top_Title.TabIndex = 52
        Me.Top_Title.Text = " "
        Me.Top_Title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Btn_Minimize
        '
        Me.Btn_Minimize.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Minimize.ErrorImage = Nothing
        Me.Btn_Minimize.Image = CType(resources.GetObject("Btn_Minimize.Image"), System.Drawing.Image)
        Me.Btn_Minimize.InitialImage = CType(resources.GetObject("Btn_Minimize.InitialImage"), System.Drawing.Image)
        Me.Btn_Minimize.Location = New System.Drawing.Point(49, 3)
        Me.Btn_Minimize.Name = "Btn_Minimize"
        Me.Btn_Minimize.Size = New System.Drawing.Size(20, 20)
        Me.Btn_Minimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Minimize.TabIndex = 50
        Me.Btn_Minimize.TabStop = False
        '
        'Btn_Maximize
        '
        Me.Btn_Maximize.BackColor = System.Drawing.Color.Transparent
        Me.Btn_Maximize.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Maximize.ErrorImage = Nothing
        Me.Btn_Maximize.Image = CType(resources.GetObject("Btn_Maximize.Image"), System.Drawing.Image)
        Me.Btn_Maximize.InitialImage = CType(resources.GetObject("Btn_Maximize.InitialImage"), System.Drawing.Image)
        Me.Btn_Maximize.Location = New System.Drawing.Point(29, 3)
        Me.Btn_Maximize.Name = "Btn_Maximize"
        Me.Btn_Maximize.Size = New System.Drawing.Size(20, 20)
        Me.Btn_Maximize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Maximize.TabIndex = 49
        Me.Btn_Maximize.TabStop = False
        '
        'Btn_Exit
        '
        Me.Btn_Exit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Exit.ErrorImage = Nothing
        Me.Btn_Exit.Image = CType(resources.GetObject("Btn_Exit.Image"), System.Drawing.Image)
        Me.Btn_Exit.InitialImage = CType(resources.GetObject("Btn_Exit.InitialImage"), System.Drawing.Image)
        Me.Btn_Exit.Location = New System.Drawing.Point(9, 3)
        Me.Btn_Exit.Name = "Btn_Exit"
        Me.Btn_Exit.Size = New System.Drawing.Size(20, 20)
        Me.Btn_Exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Exit.TabIndex = 48
        Me.Btn_Exit.TabStop = False
        '
        'MainTitle
        '
        Me.MainTitle.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.MainTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MainTitle.ForeColor = System.Drawing.Color.Yellow
        Me.MainTitle.Image = CType(resources.GetObject("MainTitle.Image"), System.Drawing.Image)
        Me.MainTitle.Location = New System.Drawing.Point(224, 33)
        Me.MainTitle.Name = "MainTitle"
        Me.MainTitle.Size = New System.Drawing.Size(1002, 39)
        Me.MainTitle.TabIndex = 51
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.RectangleShape3, Me.RectangleShape2, Me.RectangleShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(1255, 552)
        Me.ShapeContainer1.TabIndex = 52
        Me.ShapeContainer1.TabStop = False
        '
        'RectangleShape3
        '
        Me.RectangleShape3.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.RectangleShape3.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.RectangleShape3.BorderColor = System.Drawing.Color.Transparent
        Me.RectangleShape3.Enabled = False
        Me.RectangleShape3.Location = New System.Drawing.Point(27, 232)
        Me.RectangleShape3.Name = "RectangleShape3"
        Me.RectangleShape3.Size = New System.Drawing.Size(170, 303)
        '
        'RectangleShape2
        '
        Me.RectangleShape2.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.RectangleShape2.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.RectangleShape2.BorderColor = System.Drawing.Color.Transparent
        Me.RectangleShape2.Enabled = False
        Me.RectangleShape2.Location = New System.Drawing.Point(224, 33)
        Me.RectangleShape2.Name = "RectangleShape2"
        Me.RectangleShape2.Size = New System.Drawing.Size(1002, 40)
        '
        'RectangleShape1
        '
        Me.RectangleShape1.Location = New System.Drawing.Point(0, 0)
        Me.RectangleShape1.Name = "RectangleShape1"
        Me.RectangleShape1.Size = New System.Drawing.Size(75, 23)
        '
        'Logo
        '
        Me.Logo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Logo.ErrorImage = Nothing
        Me.Logo.Image = CType(resources.GetObject("Logo.Image"), System.Drawing.Image)
        Me.Logo.InitialImage = CType(resources.GetObject("Logo.InitialImage"), System.Drawing.Image)
        Me.Logo.Location = New System.Drawing.Point(39, 49)
        Me.Logo.Name = "Logo"
        Me.Logo.Size = New System.Drawing.Size(150, 150)
        Me.Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Logo.TabIndex = 51
        Me.Logo.TabStop = False
        '
        'Btn_First
        '
        Me.Btn_First.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_First.ErrorImage = Nothing
        Me.Btn_First.Image = CType(resources.GetObject("Btn_First.Image"), System.Drawing.Image)
        Me.Btn_First.Location = New System.Drawing.Point(382, 87)
        Me.Btn_First.Name = "Btn_First"
        Me.Btn_First.Size = New System.Drawing.Size(20, 20)
        Me.Btn_First.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_First.TabIndex = 54
        Me.Btn_First.TabStop = False
        '
        'Btn_Previews
        '
        Me.Btn_Previews.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Previews.ErrorImage = Nothing
        Me.Btn_Previews.Image = CType(resources.GetObject("Btn_Previews.Image"), System.Drawing.Image)
        Me.Btn_Previews.Location = New System.Drawing.Point(410, 87)
        Me.Btn_Previews.Name = "Btn_Previews"
        Me.Btn_Previews.Size = New System.Drawing.Size(20, 20)
        Me.Btn_Previews.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Previews.TabIndex = 55
        Me.Btn_Previews.TabStop = False
        '
        'Btn_Next
        '
        Me.Btn_Next.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Next.ErrorImage = Nothing
        Me.Btn_Next.Image = CType(resources.GetObject("Btn_Next.Image"), System.Drawing.Image)
        Me.Btn_Next.Location = New System.Drawing.Point(1015, 87)
        Me.Btn_Next.Name = "Btn_Next"
        Me.Btn_Next.Size = New System.Drawing.Size(20, 20)
        Me.Btn_Next.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Next.TabIndex = 56
        Me.Btn_Next.TabStop = False
        '
        'Btn_Last
        '
        Me.Btn_Last.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Last.ErrorImage = Nothing
        Me.Btn_Last.Image = CType(resources.GetObject("Btn_Last.Image"), System.Drawing.Image)
        Me.Btn_Last.Location = New System.Drawing.Point(1039, 87)
        Me.Btn_Last.Name = "Btn_Last"
        Me.Btn_Last.Size = New System.Drawing.Size(20, 20)
        Me.Btn_Last.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Last.TabIndex = 57
        Me.Btn_Last.TabStop = False
        '
        'Copyright
        '
        Me.Copyright.AutoSize = True
        Me.Copyright.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Copyright.ForeColor = System.Drawing.Color.DimGray
        Me.Copyright.Location = New System.Drawing.Point(7, 6)
        Me.Copyright.Name = "Copyright"
        Me.Copyright.Size = New System.Drawing.Size(171, 13)
        Me.Copyright.TabIndex = 31
        Me.Copyright.Text = "© 2017 Image World Digital System"
        '
        'BottomPanel
        '
        Me.BottomPanel.BackColor = System.Drawing.Color.Black
        Me.BottomPanel.Controls.Add(Me.Copyright)
        Me.BottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BottomPanel.Location = New System.Drawing.Point(0, 527)
        Me.BottomPanel.Name = "BottomPanel"
        Me.BottomPanel.Size = New System.Drawing.Size(1255, 25)
        Me.BottomPanel.TabIndex = 53
        '
        'SearchBox
        '
        Me.SearchBox.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.SearchBox.Location = New System.Drawing.Point(437, 87)
        Me.SearchBox.Name = "SearchBox"
        Me.SearchBox.Size = New System.Drawing.Size(572, 20)
        Me.SearchBox.TabIndex = 15
        Me.SearchBox.Text = "Search..."
        Me.SearchBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E_Name
        '
        Me.E_Name.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.E_Name.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.E_Name.ForeColor = System.Drawing.Color.Yellow
        Me.E_Name.Location = New System.Drawing.Point(27, 338)
        Me.E_Name.Name = "E_Name"
        Me.E_Name.Size = New System.Drawing.Size(168, 25)
        Me.E_Name.TabIndex = 59
        Me.E_Name.Text = "Mike Angelo Saraus"
        Me.E_Name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'E_Pic
        '
        Me.E_Pic.Cursor = System.Windows.Forms.Cursors.Help
        Me.E_Pic.Image = CType(resources.GetObject("E_Pic.Image"), System.Drawing.Image)
        Me.E_Pic.InitialImage = CType(resources.GetObject("E_Pic.InitialImage"), System.Drawing.Image)
        Me.E_Pic.Location = New System.Drawing.Point(73, 260)
        Me.E_Pic.Name = "E_Pic"
        Me.E_Pic.Size = New System.Drawing.Size(75, 75)
        Me.E_Pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.E_Pic.TabIndex = 60
        Me.E_Pic.TabStop = False
        '
        'E_Mode
        '
        Me.E_Mode.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.E_Mode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.E_Mode.ForeColor = System.Drawing.Color.Black
        Me.E_Mode.Location = New System.Drawing.Point(29, 361)
        Me.E_Mode.Name = "E_Mode"
        Me.E_Mode.Size = New System.Drawing.Size(165, 16)
        Me.E_Mode.TabIndex = 61
        Me.E_Mode.Text = "Generalist"
        Me.E_Mode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TabContorls
        '
        Me.TabContorls.Controls.Add(Me.View1)
        Me.TabContorls.Controls.Add(Me.View2)
        Me.TabContorls.Controls.Add(Me.Ayeng_Tab)
        Me.TabContorls.Controls.Add(Me.Jayson_Tab)
        Me.TabContorls.Controls.Add(Me.John_Tab)
        Me.TabContorls.Controls.Add(Me.March_Tab)
        Me.TabContorls.Controls.Add(Me.Marn_Tab)
        Me.TabContorls.Controls.Add(Me.Mike_Tab)
        Me.TabContorls.Controls.Add(Me.dWeek_Tab)
        Me.TabContorls.Controls.Add(Me.Tab_WIP)
        Me.TabContorls.Location = New System.Drawing.Point(224, 119)
        Me.TabContorls.Name = "TabContorls"
        Me.TabContorls.SelectedIndex = 0
        Me.TabContorls.Size = New System.Drawing.Size(1002, 375)
        Me.TabContorls.TabIndex = 62
        '
        'View1
        '
        Me.View1.BackColor = System.Drawing.SystemColors.Control
        Me.View1.Controls.Add(Me.Chart1)
        Me.View1.Controls.Add(OutDateLabel)
        Me.View1.Controls.Add(Me.OutDateDateTimePicker)
        Me.View1.Controls.Add(ArtistLabel)
        Me.View1.Controls.Add(Me.ArtistTextBox)
        Me.View1.Controls.Add(Me.JONumberTextBox)
        Me.View1.Controls.Add(InstructionsLabel)
        Me.View1.Controls.Add(Me.InstructionsTextBox)
        Me.View1.Controls.Add(OutputLabel)
        Me.View1.Controls.Add(Me.OutputComboBox)
        Me.View1.Controls.Add(Me.DateRequiredLabel)
        Me.View1.Controls.Add(Me.DateRequiredDateTimePicker)
        Me.View1.Controls.Add(Me.DateRecievedLabel)
        Me.View1.Controls.Add(Me.DateRecievedDateTimePicker)
        Me.View1.Controls.Add(ColorsLabel)
        Me.View1.Controls.Add(Me.ColorsComboBox)
        Me.View1.Controls.Add(StatusLabel)
        Me.View1.Controls.Add(Me.StatusTextBox)
        Me.View1.Controls.Add(UnitLabel)
        Me.View1.Controls.Add(Me.UnitTextBox)
        Me.View1.Controls.Add(QuantityLabel)
        Me.View1.Controls.Add(Me.QuantityTextBox)
        Me.View1.Controls.Add(TypeLabel)
        Me.View1.Controls.Add(Me.TypeTextBox)
        Me.View1.Controls.Add(CustomerLabel)
        Me.View1.Controls.Add(Me.CustomerTextBox)
        Me.View1.Controls.Add(Me.JONumberLabel)
        Me.View1.Location = New System.Drawing.Point(4, 22)
        Me.View1.Name = "View1"
        Me.View1.Padding = New System.Windows.Forms.Padding(3)
        Me.View1.Size = New System.Drawing.Size(994, 349)
        Me.View1.TabIndex = 0
        Me.View1.Text = "Add New"
        '
        'Chart1
        '
        ChartArea1.Name = "ChartArea1"
        Me.Chart1.ChartAreas.Add(ChartArea1)
        Me.Chart1.DataSource = Me.UsersBindingSource
        Legend1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Legend1.Name = "Legend1"
        Me.Chart1.Legends.Add(Legend1)
        Me.Chart1.Location = New System.Drawing.Point(409, 15)
        Me.Chart1.Name = "Chart1"
        Series1.ChartArea = "ChartArea1"
        Series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut
        Series1.Legend = "Legend1"
        Series1.Name = "Series1"
        Series1.Points.Add(DataPoint1)
        Series1.XValueMember = "Username"
        Series1.YValueMembers = "JOcount"
        Me.Chart1.Series.Add(Series1)
        Me.Chart1.Size = New System.Drawing.Size(473, 320)
        Me.Chart1.SuppressExceptions = True
        Me.Chart1.TabIndex = 73
        Me.Chart1.Text = "Chart1"
        '
        'JobOrdersBindingSource
        '
        Me.JobOrdersBindingSource.DataMember = "JobOrders"
        Me.JobOrdersBindingSource.DataSource = Me._IW_JobOrdersDataSet
        '
        '_IW_JobOrdersDataSet
        '
        Me._IW_JobOrdersDataSet.DataSetName = "_IW_JobOrdersDataSet"
        Me._IW_JobOrdersDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'OutDateDateTimePicker
        '
        Me.OutDateDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.JobOrdersBindingSource, "OutDate", True))
        Me.OutDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.OutDateDateTimePicker.Location = New System.Drawing.Point(199, 315)
        Me.OutDateDateTimePicker.Name = "OutDateDateTimePicker"
        Me.OutDateDateTimePicker.Size = New System.Drawing.Size(185, 20)
        Me.OutDateDateTimePicker.TabIndex = 14
        '
        'ArtistTextBox
        '
        Me.ArtistTextBox.AutoCompleteCustomSource.AddRange(New String() {"Mike", "March", "Jayson", "Marn", "John", "Ayeng"})
        Me.ArtistTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.ArtistTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.ArtistTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "Artist", True))
        Me.ArtistTextBox.Location = New System.Drawing.Point(199, 289)
        Me.ArtistTextBox.Name = "ArtistTextBox"
        Me.ArtistTextBox.Size = New System.Drawing.Size(185, 20)
        Me.ArtistTextBox.TabIndex = 13
        '
        'JONumberTextBox
        '
        Me.JONumberTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "JONumber", True))
        Me.JONumberTextBox.Location = New System.Drawing.Point(199, 15)
        Me.JONumberTextBox.Name = "JONumberTextBox"
        Me.JONumberTextBox.Size = New System.Drawing.Size(185, 20)
        Me.JONumberTextBox.TabIndex = 2
        '
        'InstructionsTextBox
        '
        Me.InstructionsTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.InstructionsTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList
        Me.InstructionsTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "Instructions", True))
        Me.InstructionsTextBox.Location = New System.Drawing.Point(199, 222)
        Me.InstructionsTextBox.Multiline = True
        Me.InstructionsTextBox.Name = "InstructionsTextBox"
        Me.InstructionsTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.InstructionsTextBox.Size = New System.Drawing.Size(185, 33)
        Me.InstructionsTextBox.TabIndex = 11
        '
        'OutputComboBox
        '
        Me.OutputComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.OutputComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.OutputComboBox.BackColor = System.Drawing.Color.White
        Me.OutputComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "Output", True))
        Me.OutputComboBox.FormattingEnabled = True
        Me.OutputComboBox.Items.AddRange(New Object() {"CTP – Computer to Plate", "DG – Digital Printing", "Duplo", "PVC"})
        Me.OutputComboBox.Location = New System.Drawing.Point(199, 262)
        Me.OutputComboBox.Name = "OutputComboBox"
        Me.OutputComboBox.Size = New System.Drawing.Size(185, 21)
        Me.OutputComboBox.TabIndex = 12
        '
        'DateRequiredLabel
        '
        Me.DateRequiredLabel.AutoSize = True
        Me.DateRequiredLabel.Location = New System.Drawing.Point(86, 200)
        Me.DateRequiredLabel.Name = "DateRequiredLabel"
        Me.DateRequiredLabel.Size = New System.Drawing.Size(73, 13)
        Me.DateRequiredLabel.TabIndex = 71
        Me.DateRequiredLabel.Text = "DateRequired"
        '
        'DateRequiredDateTimePicker
        '
        Me.DateRequiredDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.JobOrdersBindingSource, "DateRequired", True))
        Me.DateRequiredDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateRequiredDateTimePicker.Location = New System.Drawing.Point(199, 196)
        Me.DateRequiredDateTimePicker.MaxDate = New Date(2050, 12, 31, 0, 0, 0, 0)
        Me.DateRequiredDateTimePicker.MinDate = New Date(2000, 12, 31, 0, 0, 0, 0)
        Me.DateRequiredDateTimePicker.Name = "DateRequiredDateTimePicker"
        Me.DateRequiredDateTimePicker.Size = New System.Drawing.Size(185, 20)
        Me.DateRequiredDateTimePicker.TabIndex = 10
        Me.DateRequiredDateTimePicker.Value = New Date(2017, 9, 14, 12, 21, 5, 0)
        '
        'DateRecievedLabel
        '
        Me.DateRecievedLabel.AutoSize = True
        Me.DateRecievedLabel.Location = New System.Drawing.Point(83, 176)
        Me.DateRecievedLabel.Name = "DateRecievedLabel"
        Me.DateRecievedLabel.Size = New System.Drawing.Size(76, 13)
        Me.DateRecievedLabel.TabIndex = 70
        Me.DateRecievedLabel.Text = "DateRecieved"
        '
        'DateRecievedDateTimePicker
        '
        Me.DateRecievedDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.JobOrdersBindingSource, "DateRecieved", True))
        Me.DateRecievedDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateRecievedDateTimePicker.Location = New System.Drawing.Point(199, 171)
        Me.DateRecievedDateTimePicker.MaxDate = New Date(2050, 12, 31, 0, 0, 0, 0)
        Me.DateRecievedDateTimePicker.MinDate = New Date(2000, 12, 31, 0, 0, 0, 0)
        Me.DateRecievedDateTimePicker.Name = "DateRecievedDateTimePicker"
        Me.DateRecievedDateTimePicker.Size = New System.Drawing.Size(185, 20)
        Me.DateRecievedDateTimePicker.TabIndex = 9
        Me.DateRecievedDateTimePicker.Value = New Date(2017, 9, 14, 0, 0, 0, 0)
        '
        'ColorsComboBox
        '
        Me.ColorsComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.ColorsComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ColorsComboBox.BackColor = System.Drawing.Color.White
        Me.ColorsComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "Colors", True))
        Me.ColorsComboBox.FormattingEnabled = True
        Me.ColorsComboBox.Items.AddRange(New Object() {"1 – One Color", "4 – Full Colors"})
        Me.ColorsComboBox.Location = New System.Drawing.Point(199, 144)
        Me.ColorsComboBox.Name = "ColorsComboBox"
        Me.ColorsComboBox.Size = New System.Drawing.Size(185, 21)
        Me.ColorsComboBox.TabIndex = 8
        '
        'StatusTextBox
        '
        Me.StatusTextBox.AutoCompleteCustomSource.AddRange(New String() {"Done", "Waiting for Files", "Email Sent", "For Approval", "For Layout"})
        Me.StatusTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.StatusTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.StatusTextBox.BackColor = System.Drawing.Color.White
        Me.StatusTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "Status", True))
        Me.StatusTextBox.Location = New System.Drawing.Point(199, 118)
        Me.StatusTextBox.Name = "StatusTextBox"
        Me.StatusTextBox.Size = New System.Drawing.Size(185, 20)
        Me.StatusTextBox.TabIndex = 7
        '
        'UnitTextBox
        '
        Me.UnitTextBox.AutoCompleteCustomSource.AddRange(New String() {"Copies", "Box/es", "Book/s", "Pad/s", "Pc/s"})
        Me.UnitTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.UnitTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.UnitTextBox.BackColor = System.Drawing.Color.White
        Me.UnitTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "Unit", True))
        Me.UnitTextBox.Location = New System.Drawing.Point(338, 92)
        Me.UnitTextBox.Name = "UnitTextBox"
        Me.UnitTextBox.Size = New System.Drawing.Size(46, 20)
        Me.UnitTextBox.TabIndex = 6
        '
        'QuantityTextBox
        '
        Me.QuantityTextBox.BackColor = System.Drawing.Color.White
        Me.QuantityTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "Quantity", True))
        Me.QuantityTextBox.Location = New System.Drawing.Point(199, 92)
        Me.QuantityTextBox.Name = "QuantityTextBox"
        Me.QuantityTextBox.Size = New System.Drawing.Size(100, 20)
        Me.QuantityTextBox.TabIndex = 5
        '
        'TypeTextBox
        '
        Me.TypeTextBox.AutoCompleteCustomSource.AddRange(New String() {"Calling Card", "Book Publication", "Handbook", "Folio", "Offset", "Duplo", "Reciept", "Poster", "Brochure", "PVC ID", "ID", "Magazines", "Sovenier Program"})
        Me.TypeTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.TypeTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.TypeTextBox.BackColor = System.Drawing.Color.White
        Me.TypeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "Type", True))
        Me.TypeTextBox.Location = New System.Drawing.Point(199, 66)
        Me.TypeTextBox.Name = "TypeTextBox"
        Me.TypeTextBox.Size = New System.Drawing.Size(185, 20)
        Me.TypeTextBox.TabIndex = 4
        '
        'CustomerTextBox
        '
        Me.CustomerTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CustomerTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.CustomerTextBox.BackColor = System.Drawing.Color.White
        Me.CustomerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "Customer", True))
        Me.CustomerTextBox.Location = New System.Drawing.Point(199, 40)
        Me.CustomerTextBox.Name = "CustomerTextBox"
        Me.CustomerTextBox.Size = New System.Drawing.Size(185, 20)
        Me.CustomerTextBox.TabIndex = 3
        '
        'JONumberLabel
        '
        Me.JONumberLabel.AutoSize = True
        Me.JONumberLabel.Location = New System.Drawing.Point(64, 18)
        Me.JONumberLabel.Name = "JONumberLabel"
        Me.JONumberLabel.Size = New System.Drawing.Size(93, 13)
        Me.JONumberLabel.TabIndex = 63
        Me.JONumberLabel.Text = "Job Order Number"
        '
        'View2
        '
        Me.View2.Controls.Add(Me.DataGridView1)
        Me.View2.Location = New System.Drawing.Point(4, 22)
        Me.View2.Name = "View2"
        Me.View2.Padding = New System.Windows.Forms.Padding(3)
        Me.View2.Size = New System.Drawing.Size(994, 349)
        Me.View2.TabIndex = 1
        Me.View2.Text = "View Table"
        Me.View2.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.DataGridView1.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken
        Me.DataGridView1.ColumnHeadersHeight = 20
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn85, Me.DataGridViewTextBoxColumn86, Me.DataGridViewTextBoxColumn87, Me.DataGridViewTextBoxColumn88, Me.DataGridViewTextBoxColumn89, Me.DataGridViewTextBoxColumn90, Me.DataGridViewTextBoxColumn91, Me.DataGridViewTextBoxColumn92, Me.DataGridViewTextBoxColumn93, Me.DataGridViewTextBoxColumn94, Me.DataGridViewTextBoxColumn95, Me.DataGridViewTextBoxColumn96, Me.Artist})
        Me.DataGridView1.DataSource = Me.JobOrdersBindingSource
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(3, 3)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowHeadersWidth = 10
        Me.DataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.DataGridView1.Size = New System.Drawing.Size(988, 343)
        Me.DataGridView1.TabIndex = 4
        '
        'DataGridViewTextBoxColumn85
        '
        Me.DataGridViewTextBoxColumn85.DataPropertyName = "JONumber"
        Me.DataGridViewTextBoxColumn85.HeaderText = "JONumber"
        Me.DataGridViewTextBoxColumn85.Name = "DataGridViewTextBoxColumn85"
        '
        'DataGridViewTextBoxColumn86
        '
        Me.DataGridViewTextBoxColumn86.DataPropertyName = "Customer"
        Me.DataGridViewTextBoxColumn86.HeaderText = "Customer"
        Me.DataGridViewTextBoxColumn86.Name = "DataGridViewTextBoxColumn86"
        '
        'DataGridViewTextBoxColumn87
        '
        Me.DataGridViewTextBoxColumn87.DataPropertyName = "Type"
        Me.DataGridViewTextBoxColumn87.HeaderText = "Type"
        Me.DataGridViewTextBoxColumn87.Name = "DataGridViewTextBoxColumn87"
        '
        'DataGridViewTextBoxColumn88
        '
        Me.DataGridViewTextBoxColumn88.DataPropertyName = "Quantity"
        Me.DataGridViewTextBoxColumn88.HeaderText = "Quantity"
        Me.DataGridViewTextBoxColumn88.Name = "DataGridViewTextBoxColumn88"
        '
        'DataGridViewTextBoxColumn89
        '
        Me.DataGridViewTextBoxColumn89.DataPropertyName = "Unit"
        Me.DataGridViewTextBoxColumn89.HeaderText = "Unit"
        Me.DataGridViewTextBoxColumn89.Name = "DataGridViewTextBoxColumn89"
        '
        'DataGridViewTextBoxColumn90
        '
        Me.DataGridViewTextBoxColumn90.DataPropertyName = "Status"
        Me.DataGridViewTextBoxColumn90.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn90.Name = "DataGridViewTextBoxColumn90"
        '
        'DataGridViewTextBoxColumn91
        '
        Me.DataGridViewTextBoxColumn91.DataPropertyName = "Colors"
        Me.DataGridViewTextBoxColumn91.HeaderText = "Colors"
        Me.DataGridViewTextBoxColumn91.Name = "DataGridViewTextBoxColumn91"
        '
        'DataGridViewTextBoxColumn92
        '
        Me.DataGridViewTextBoxColumn92.DataPropertyName = "DateRecieved"
        Me.DataGridViewTextBoxColumn92.HeaderText = "DateRecieved"
        Me.DataGridViewTextBoxColumn92.Name = "DataGridViewTextBoxColumn92"
        '
        'DataGridViewTextBoxColumn93
        '
        Me.DataGridViewTextBoxColumn93.DataPropertyName = "DateRequired"
        Me.DataGridViewTextBoxColumn93.HeaderText = "DateRequired"
        Me.DataGridViewTextBoxColumn93.Name = "DataGridViewTextBoxColumn93"
        '
        'DataGridViewTextBoxColumn94
        '
        Me.DataGridViewTextBoxColumn94.DataPropertyName = "OutDate"
        Me.DataGridViewTextBoxColumn94.HeaderText = "OutDate"
        Me.DataGridViewTextBoxColumn94.Name = "DataGridViewTextBoxColumn94"
        '
        'DataGridViewTextBoxColumn95
        '
        Me.DataGridViewTextBoxColumn95.DataPropertyName = "Output"
        Me.DataGridViewTextBoxColumn95.HeaderText = "Output"
        Me.DataGridViewTextBoxColumn95.Name = "DataGridViewTextBoxColumn95"
        '
        'DataGridViewTextBoxColumn96
        '
        Me.DataGridViewTextBoxColumn96.DataPropertyName = "Instructions"
        Me.DataGridViewTextBoxColumn96.HeaderText = "Instructions"
        Me.DataGridViewTextBoxColumn96.Name = "DataGridViewTextBoxColumn96"
        '
        'Artist
        '
        Me.Artist.DataPropertyName = "Artist"
        Me.Artist.HeaderText = "Artist"
        Me.Artist.Name = "Artist"
        '
        'Ayeng_Tab
        '
        Me.Ayeng_Tab.Controls.Add(Me.Ayeng_Data)
        Me.Ayeng_Tab.Location = New System.Drawing.Point(4, 22)
        Me.Ayeng_Tab.Name = "Ayeng_Tab"
        Me.Ayeng_Tab.Padding = New System.Windows.Forms.Padding(3)
        Me.Ayeng_Tab.Size = New System.Drawing.Size(994, 349)
        Me.Ayeng_Tab.TabIndex = 7
        Me.Ayeng_Tab.Text = "Ayeng"
        Me.Ayeng_Tab.UseVisualStyleBackColor = True
        '
        'Ayeng_Data
        '
        Me.Ayeng_Data.AllowUserToDeleteRows = False
        Me.Ayeng_Data.AllowUserToOrderColumns = True
        Me.Ayeng_Data.AutoGenerateColumns = False
        Me.Ayeng_Data.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.Ayeng_Data.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.Ayeng_Data.BackgroundColor = System.Drawing.SystemColors.Control
        Me.Ayeng_Data.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken
        Me.Ayeng_Data.ColumnHeadersHeight = 20
        Me.Ayeng_Data.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn14, Me.DataGridViewTextBoxColumn15, Me.DataGridViewTextBoxColumn28, Me.DataGridViewTextBoxColumn29, Me.DataGridViewTextBoxColumn42, Me.DataGridViewTextBoxColumn43, Me.DataGridViewTextBoxColumn56, Me.DataGridViewTextBoxColumn57, Me.DataGridViewTextBoxColumn70, Me.DataGridViewTextBoxColumn71, Me.DataGridViewTextBoxColumn72})
        Me.Ayeng_Data.DataSource = Me.JobOrdersBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Ayeng_Data.DefaultCellStyle = DataGridViewCellStyle2
        Me.Ayeng_Data.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Ayeng_Data.Location = New System.Drawing.Point(3, 3)
        Me.Ayeng_Data.Name = "Ayeng_Data"
        Me.Ayeng_Data.RowHeadersWidth = 10
        Me.Ayeng_Data.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.Ayeng_Data.Size = New System.Drawing.Size(988, 343)
        Me.Ayeng_Data.TabIndex = 4
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "JONumber"
        Me.DataGridViewTextBoxColumn1.HeaderText = "JONumber"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.DataPropertyName = "Customer"
        Me.DataGridViewTextBoxColumn14.HeaderText = "Customer"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.DataPropertyName = "Type"
        Me.DataGridViewTextBoxColumn15.HeaderText = "Type"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        '
        'DataGridViewTextBoxColumn28
        '
        Me.DataGridViewTextBoxColumn28.DataPropertyName = "Quantity"
        Me.DataGridViewTextBoxColumn28.HeaderText = "Quantity"
        Me.DataGridViewTextBoxColumn28.Name = "DataGridViewTextBoxColumn28"
        '
        'DataGridViewTextBoxColumn29
        '
        Me.DataGridViewTextBoxColumn29.DataPropertyName = "Unit"
        Me.DataGridViewTextBoxColumn29.HeaderText = "Unit"
        Me.DataGridViewTextBoxColumn29.Name = "DataGridViewTextBoxColumn29"
        '
        'DataGridViewTextBoxColumn42
        '
        Me.DataGridViewTextBoxColumn42.DataPropertyName = "Status"
        Me.DataGridViewTextBoxColumn42.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn42.Name = "DataGridViewTextBoxColumn42"
        '
        'DataGridViewTextBoxColumn43
        '
        Me.DataGridViewTextBoxColumn43.DataPropertyName = "Colors"
        Me.DataGridViewTextBoxColumn43.HeaderText = "Colors"
        Me.DataGridViewTextBoxColumn43.Name = "DataGridViewTextBoxColumn43"
        '
        'DataGridViewTextBoxColumn56
        '
        Me.DataGridViewTextBoxColumn56.DataPropertyName = "DateRecieved"
        Me.DataGridViewTextBoxColumn56.HeaderText = "DateRecieved"
        Me.DataGridViewTextBoxColumn56.Name = "DataGridViewTextBoxColumn56"
        '
        'DataGridViewTextBoxColumn57
        '
        Me.DataGridViewTextBoxColumn57.DataPropertyName = "DateRequired"
        Me.DataGridViewTextBoxColumn57.HeaderText = "DateRequired"
        Me.DataGridViewTextBoxColumn57.Name = "DataGridViewTextBoxColumn57"
        '
        'DataGridViewTextBoxColumn70
        '
        Me.DataGridViewTextBoxColumn70.DataPropertyName = "OutDate"
        Me.DataGridViewTextBoxColumn70.HeaderText = "OutDate"
        Me.DataGridViewTextBoxColumn70.Name = "DataGridViewTextBoxColumn70"
        '
        'DataGridViewTextBoxColumn71
        '
        Me.DataGridViewTextBoxColumn71.DataPropertyName = "Output"
        Me.DataGridViewTextBoxColumn71.HeaderText = "Output"
        Me.DataGridViewTextBoxColumn71.Name = "DataGridViewTextBoxColumn71"
        '
        'DataGridViewTextBoxColumn72
        '
        Me.DataGridViewTextBoxColumn72.DataPropertyName = "Instructions"
        Me.DataGridViewTextBoxColumn72.HeaderText = "Instructions"
        Me.DataGridViewTextBoxColumn72.Name = "DataGridViewTextBoxColumn72"
        '
        'Jayson_Tab
        '
        Me.Jayson_Tab.Controls.Add(Me.Jayson_Data)
        Me.Jayson_Tab.Location = New System.Drawing.Point(4, 22)
        Me.Jayson_Tab.Name = "Jayson_Tab"
        Me.Jayson_Tab.Padding = New System.Windows.Forms.Padding(3)
        Me.Jayson_Tab.Size = New System.Drawing.Size(994, 349)
        Me.Jayson_Tab.TabIndex = 2
        Me.Jayson_Tab.Text = "Jayson"
        Me.Jayson_Tab.UseVisualStyleBackColor = True
        '
        'Jayson_Data
        '
        Me.Jayson_Data.AllowUserToDeleteRows = False
        Me.Jayson_Data.AllowUserToOrderColumns = True
        Me.Jayson_Data.AutoGenerateColumns = False
        Me.Jayson_Data.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.Jayson_Data.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.Jayson_Data.BackgroundColor = System.Drawing.SystemColors.Control
        Me.Jayson_Data.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken
        Me.Jayson_Data.ColumnHeadersHeight = 20
        Me.Jayson_Data.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13})
        Me.Jayson_Data.DataSource = Me.JobOrdersBindingSource
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Jayson_Data.DefaultCellStyle = DataGridViewCellStyle3
        Me.Jayson_Data.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Jayson_Data.Location = New System.Drawing.Point(3, 3)
        Me.Jayson_Data.Name = "Jayson_Data"
        Me.Jayson_Data.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.Jayson_Data.RowHeadersWidth = 10
        Me.Jayson_Data.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.Jayson_Data.Size = New System.Drawing.Size(988, 343)
        Me.Jayson_Data.TabIndex = 4
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "JONumber"
        Me.DataGridViewTextBoxColumn2.HeaderText = "JONumber"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Customer"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Customer"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Type"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Type"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Quantity"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Quantity"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Unit"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Unit"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "Status"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Colors"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Colors"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "DateRecieved"
        Me.DataGridViewTextBoxColumn9.HeaderText = "DateRecieved"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "DateRequired"
        Me.DataGridViewTextBoxColumn10.HeaderText = "DateRequired"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "OutDate"
        Me.DataGridViewTextBoxColumn11.HeaderText = "OutDate"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "Output"
        Me.DataGridViewTextBoxColumn12.HeaderText = "Output"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "Instructions"
        Me.DataGridViewTextBoxColumn13.HeaderText = "Instructions"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        '
        'John_Tab
        '
        Me.John_Tab.Controls.Add(Me.John_Data)
        Me.John_Tab.Location = New System.Drawing.Point(4, 22)
        Me.John_Tab.Name = "John_Tab"
        Me.John_Tab.Padding = New System.Windows.Forms.Padding(3)
        Me.John_Tab.Size = New System.Drawing.Size(994, 349)
        Me.John_Tab.TabIndex = 3
        Me.John_Tab.Text = "John"
        Me.John_Tab.UseVisualStyleBackColor = True
        '
        'John_Data
        '
        Me.John_Data.AllowUserToDeleteRows = False
        Me.John_Data.AllowUserToOrderColumns = True
        Me.John_Data.AutoGenerateColumns = False
        Me.John_Data.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.John_Data.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.John_Data.BackgroundColor = System.Drawing.SystemColors.Control
        Me.John_Data.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken
        Me.John_Data.ColumnHeadersHeight = 20
        Me.John_Data.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn16, Me.DataGridViewTextBoxColumn17, Me.DataGridViewTextBoxColumn18, Me.DataGridViewTextBoxColumn19, Me.DataGridViewTextBoxColumn20, Me.DataGridViewTextBoxColumn21, Me.DataGridViewTextBoxColumn22, Me.DataGridViewTextBoxColumn23, Me.DataGridViewTextBoxColumn24, Me.DataGridViewTextBoxColumn25, Me.DataGridViewTextBoxColumn26, Me.DataGridViewTextBoxColumn27})
        Me.John_Data.DataSource = Me.JobOrdersBindingSource
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.John_Data.DefaultCellStyle = DataGridViewCellStyle4
        Me.John_Data.Dock = System.Windows.Forms.DockStyle.Fill
        Me.John_Data.Location = New System.Drawing.Point(3, 3)
        Me.John_Data.Name = "John_Data"
        Me.John_Data.RowHeadersWidth = 10
        Me.John_Data.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.John_Data.Size = New System.Drawing.Size(988, 343)
        Me.John_Data.TabIndex = 4
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.DataPropertyName = "JONumber"
        Me.DataGridViewTextBoxColumn16.HeaderText = "JONumber"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.DataPropertyName = "Customer"
        Me.DataGridViewTextBoxColumn17.HeaderText = "Customer"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.DataPropertyName = "Type"
        Me.DataGridViewTextBoxColumn18.HeaderText = "Type"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.DataPropertyName = "Quantity"
        Me.DataGridViewTextBoxColumn19.HeaderText = "Quantity"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.DataPropertyName = "Unit"
        Me.DataGridViewTextBoxColumn20.HeaderText = "Unit"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.DataPropertyName = "Status"
        Me.DataGridViewTextBoxColumn21.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.DataPropertyName = "Colors"
        Me.DataGridViewTextBoxColumn22.HeaderText = "Colors"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.DataPropertyName = "DateRecieved"
        Me.DataGridViewTextBoxColumn23.HeaderText = "DateRecieved"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.DataPropertyName = "DateRequired"
        Me.DataGridViewTextBoxColumn24.HeaderText = "DateRequired"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.DataPropertyName = "OutDate"
        Me.DataGridViewTextBoxColumn25.HeaderText = "OutDate"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.DataPropertyName = "Output"
        Me.DataGridViewTextBoxColumn26.HeaderText = "Output"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        '
        'DataGridViewTextBoxColumn27
        '
        Me.DataGridViewTextBoxColumn27.DataPropertyName = "Instructions"
        Me.DataGridViewTextBoxColumn27.HeaderText = "Instructions"
        Me.DataGridViewTextBoxColumn27.Name = "DataGridViewTextBoxColumn27"
        '
        'March_Tab
        '
        Me.March_Tab.Controls.Add(Me.March_Data)
        Me.March_Tab.Location = New System.Drawing.Point(4, 22)
        Me.March_Tab.Name = "March_Tab"
        Me.March_Tab.Padding = New System.Windows.Forms.Padding(3)
        Me.March_Tab.Size = New System.Drawing.Size(994, 349)
        Me.March_Tab.TabIndex = 4
        Me.March_Tab.Text = "March"
        Me.March_Tab.UseVisualStyleBackColor = True
        '
        'March_Data
        '
        Me.March_Data.AllowUserToDeleteRows = False
        Me.March_Data.AllowUserToOrderColumns = True
        Me.March_Data.AutoGenerateColumns = False
        Me.March_Data.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.March_Data.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.March_Data.BackgroundColor = System.Drawing.SystemColors.Control
        Me.March_Data.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken
        Me.March_Data.ColumnHeadersHeight = 20
        Me.March_Data.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn30, Me.DataGridViewTextBoxColumn31, Me.DataGridViewTextBoxColumn32, Me.DataGridViewTextBoxColumn33, Me.DataGridViewTextBoxColumn34, Me.DataGridViewTextBoxColumn35, Me.DataGridViewTextBoxColumn36, Me.DataGridViewTextBoxColumn37, Me.DataGridViewTextBoxColumn38, Me.DataGridViewTextBoxColumn39, Me.DataGridViewTextBoxColumn40, Me.DataGridViewTextBoxColumn41})
        Me.March_Data.DataSource = Me.JobOrdersBindingSource
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.March_Data.DefaultCellStyle = DataGridViewCellStyle5
        Me.March_Data.Dock = System.Windows.Forms.DockStyle.Fill
        Me.March_Data.Location = New System.Drawing.Point(3, 3)
        Me.March_Data.Name = "March_Data"
        Me.March_Data.RowHeadersWidth = 10
        Me.March_Data.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.March_Data.Size = New System.Drawing.Size(988, 343)
        Me.March_Data.TabIndex = 4
        '
        'DataGridViewTextBoxColumn30
        '
        Me.DataGridViewTextBoxColumn30.DataPropertyName = "JONumber"
        Me.DataGridViewTextBoxColumn30.HeaderText = "JONumber"
        Me.DataGridViewTextBoxColumn30.Name = "DataGridViewTextBoxColumn30"
        '
        'DataGridViewTextBoxColumn31
        '
        Me.DataGridViewTextBoxColumn31.DataPropertyName = "Customer"
        Me.DataGridViewTextBoxColumn31.HeaderText = "Customer"
        Me.DataGridViewTextBoxColumn31.Name = "DataGridViewTextBoxColumn31"
        '
        'DataGridViewTextBoxColumn32
        '
        Me.DataGridViewTextBoxColumn32.DataPropertyName = "Type"
        Me.DataGridViewTextBoxColumn32.HeaderText = "Type"
        Me.DataGridViewTextBoxColumn32.Name = "DataGridViewTextBoxColumn32"
        '
        'DataGridViewTextBoxColumn33
        '
        Me.DataGridViewTextBoxColumn33.DataPropertyName = "Quantity"
        Me.DataGridViewTextBoxColumn33.HeaderText = "Quantity"
        Me.DataGridViewTextBoxColumn33.Name = "DataGridViewTextBoxColumn33"
        '
        'DataGridViewTextBoxColumn34
        '
        Me.DataGridViewTextBoxColumn34.DataPropertyName = "Unit"
        Me.DataGridViewTextBoxColumn34.HeaderText = "Unit"
        Me.DataGridViewTextBoxColumn34.Name = "DataGridViewTextBoxColumn34"
        '
        'DataGridViewTextBoxColumn35
        '
        Me.DataGridViewTextBoxColumn35.DataPropertyName = "Status"
        Me.DataGridViewTextBoxColumn35.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn35.Name = "DataGridViewTextBoxColumn35"
        '
        'DataGridViewTextBoxColumn36
        '
        Me.DataGridViewTextBoxColumn36.DataPropertyName = "Colors"
        Me.DataGridViewTextBoxColumn36.HeaderText = "Colors"
        Me.DataGridViewTextBoxColumn36.Name = "DataGridViewTextBoxColumn36"
        '
        'DataGridViewTextBoxColumn37
        '
        Me.DataGridViewTextBoxColumn37.DataPropertyName = "DateRecieved"
        Me.DataGridViewTextBoxColumn37.HeaderText = "DateRecieved"
        Me.DataGridViewTextBoxColumn37.Name = "DataGridViewTextBoxColumn37"
        '
        'DataGridViewTextBoxColumn38
        '
        Me.DataGridViewTextBoxColumn38.DataPropertyName = "DateRequired"
        Me.DataGridViewTextBoxColumn38.HeaderText = "DateRequired"
        Me.DataGridViewTextBoxColumn38.Name = "DataGridViewTextBoxColumn38"
        '
        'DataGridViewTextBoxColumn39
        '
        Me.DataGridViewTextBoxColumn39.DataPropertyName = "OutDate"
        Me.DataGridViewTextBoxColumn39.HeaderText = "OutDate"
        Me.DataGridViewTextBoxColumn39.Name = "DataGridViewTextBoxColumn39"
        '
        'DataGridViewTextBoxColumn40
        '
        Me.DataGridViewTextBoxColumn40.DataPropertyName = "Output"
        Me.DataGridViewTextBoxColumn40.HeaderText = "Output"
        Me.DataGridViewTextBoxColumn40.Name = "DataGridViewTextBoxColumn40"
        '
        'DataGridViewTextBoxColumn41
        '
        Me.DataGridViewTextBoxColumn41.DataPropertyName = "Instructions"
        Me.DataGridViewTextBoxColumn41.HeaderText = "Instructions"
        Me.DataGridViewTextBoxColumn41.Name = "DataGridViewTextBoxColumn41"
        '
        'Marn_Tab
        '
        Me.Marn_Tab.Controls.Add(Me.Marn_Data)
        Me.Marn_Tab.Location = New System.Drawing.Point(4, 22)
        Me.Marn_Tab.Name = "Marn_Tab"
        Me.Marn_Tab.Padding = New System.Windows.Forms.Padding(3)
        Me.Marn_Tab.Size = New System.Drawing.Size(994, 349)
        Me.Marn_Tab.TabIndex = 5
        Me.Marn_Tab.Text = "Marn"
        Me.Marn_Tab.UseVisualStyleBackColor = True
        '
        'Marn_Data
        '
        Me.Marn_Data.AllowUserToDeleteRows = False
        Me.Marn_Data.AllowUserToOrderColumns = True
        Me.Marn_Data.AutoGenerateColumns = False
        Me.Marn_Data.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.Marn_Data.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.Marn_Data.BackgroundColor = System.Drawing.SystemColors.Control
        Me.Marn_Data.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken
        Me.Marn_Data.ColumnHeadersHeight = 20
        Me.Marn_Data.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn44, Me.DataGridViewTextBoxColumn45, Me.DataGridViewTextBoxColumn46, Me.DataGridViewTextBoxColumn47, Me.DataGridViewTextBoxColumn48, Me.DataGridViewTextBoxColumn49, Me.DataGridViewTextBoxColumn50, Me.DataGridViewTextBoxColumn51, Me.DataGridViewTextBoxColumn52, Me.DataGridViewTextBoxColumn53, Me.DataGridViewTextBoxColumn54, Me.DataGridViewTextBoxColumn55})
        Me.Marn_Data.DataSource = Me.JobOrdersBindingSource
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Marn_Data.DefaultCellStyle = DataGridViewCellStyle6
        Me.Marn_Data.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Marn_Data.Location = New System.Drawing.Point(3, 3)
        Me.Marn_Data.Name = "Marn_Data"
        Me.Marn_Data.RowHeadersWidth = 10
        Me.Marn_Data.Size = New System.Drawing.Size(988, 343)
        Me.Marn_Data.TabIndex = 4
        '
        'DataGridViewTextBoxColumn44
        '
        Me.DataGridViewTextBoxColumn44.DataPropertyName = "JONumber"
        Me.DataGridViewTextBoxColumn44.HeaderText = "JONumber"
        Me.DataGridViewTextBoxColumn44.Name = "DataGridViewTextBoxColumn44"
        '
        'DataGridViewTextBoxColumn45
        '
        Me.DataGridViewTextBoxColumn45.DataPropertyName = "Customer"
        Me.DataGridViewTextBoxColumn45.HeaderText = "Customer"
        Me.DataGridViewTextBoxColumn45.Name = "DataGridViewTextBoxColumn45"
        '
        'DataGridViewTextBoxColumn46
        '
        Me.DataGridViewTextBoxColumn46.DataPropertyName = "Type"
        Me.DataGridViewTextBoxColumn46.HeaderText = "Type"
        Me.DataGridViewTextBoxColumn46.Name = "DataGridViewTextBoxColumn46"
        '
        'DataGridViewTextBoxColumn47
        '
        Me.DataGridViewTextBoxColumn47.DataPropertyName = "Quantity"
        Me.DataGridViewTextBoxColumn47.HeaderText = "Quantity"
        Me.DataGridViewTextBoxColumn47.Name = "DataGridViewTextBoxColumn47"
        '
        'DataGridViewTextBoxColumn48
        '
        Me.DataGridViewTextBoxColumn48.DataPropertyName = "Unit"
        Me.DataGridViewTextBoxColumn48.HeaderText = "Unit"
        Me.DataGridViewTextBoxColumn48.Name = "DataGridViewTextBoxColumn48"
        '
        'DataGridViewTextBoxColumn49
        '
        Me.DataGridViewTextBoxColumn49.DataPropertyName = "Status"
        Me.DataGridViewTextBoxColumn49.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn49.Name = "DataGridViewTextBoxColumn49"
        '
        'DataGridViewTextBoxColumn50
        '
        Me.DataGridViewTextBoxColumn50.DataPropertyName = "Colors"
        Me.DataGridViewTextBoxColumn50.HeaderText = "Colors"
        Me.DataGridViewTextBoxColumn50.Name = "DataGridViewTextBoxColumn50"
        '
        'DataGridViewTextBoxColumn51
        '
        Me.DataGridViewTextBoxColumn51.DataPropertyName = "DateRecieved"
        Me.DataGridViewTextBoxColumn51.HeaderText = "DateRecieved"
        Me.DataGridViewTextBoxColumn51.Name = "DataGridViewTextBoxColumn51"
        '
        'DataGridViewTextBoxColumn52
        '
        Me.DataGridViewTextBoxColumn52.DataPropertyName = "DateRequired"
        Me.DataGridViewTextBoxColumn52.HeaderText = "DateRequired"
        Me.DataGridViewTextBoxColumn52.Name = "DataGridViewTextBoxColumn52"
        '
        'DataGridViewTextBoxColumn53
        '
        Me.DataGridViewTextBoxColumn53.DataPropertyName = "OutDate"
        Me.DataGridViewTextBoxColumn53.HeaderText = "OutDate"
        Me.DataGridViewTextBoxColumn53.Name = "DataGridViewTextBoxColumn53"
        '
        'DataGridViewTextBoxColumn54
        '
        Me.DataGridViewTextBoxColumn54.DataPropertyName = "Output"
        Me.DataGridViewTextBoxColumn54.HeaderText = "Output"
        Me.DataGridViewTextBoxColumn54.Name = "DataGridViewTextBoxColumn54"
        '
        'DataGridViewTextBoxColumn55
        '
        Me.DataGridViewTextBoxColumn55.DataPropertyName = "Instructions"
        Me.DataGridViewTextBoxColumn55.HeaderText = "Instructions"
        Me.DataGridViewTextBoxColumn55.Name = "DataGridViewTextBoxColumn55"
        '
        'Mike_Tab
        '
        Me.Mike_Tab.Controls.Add(Me.Mike_Data)
        Me.Mike_Tab.Location = New System.Drawing.Point(4, 22)
        Me.Mike_Tab.Name = "Mike_Tab"
        Me.Mike_Tab.Padding = New System.Windows.Forms.Padding(3)
        Me.Mike_Tab.Size = New System.Drawing.Size(994, 349)
        Me.Mike_Tab.TabIndex = 6
        Me.Mike_Tab.Text = "Mike"
        Me.Mike_Tab.UseVisualStyleBackColor = True
        '
        'Mike_Data
        '
        Me.Mike_Data.AllowUserToDeleteRows = False
        Me.Mike_Data.AllowUserToOrderColumns = True
        Me.Mike_Data.AutoGenerateColumns = False
        Me.Mike_Data.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.Mike_Data.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.Mike_Data.BackgroundColor = System.Drawing.SystemColors.Control
        Me.Mike_Data.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken
        Me.Mike_Data.ColumnHeadersHeight = 20
        Me.Mike_Data.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn58, Me.DataGridViewTextBoxColumn59, Me.DataGridViewTextBoxColumn60, Me.DataGridViewTextBoxColumn61, Me.DataGridViewTextBoxColumn62, Me.DataGridViewTextBoxColumn63, Me.DataGridViewTextBoxColumn64, Me.DataGridViewTextBoxColumn65, Me.DataGridViewTextBoxColumn66, Me.DataGridViewTextBoxColumn67, Me.DataGridViewTextBoxColumn68, Me.DataGridViewTextBoxColumn69})
        Me.Mike_Data.DataSource = Me.JobOrdersBindingSource
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Mike_Data.DefaultCellStyle = DataGridViewCellStyle7
        Me.Mike_Data.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Mike_Data.Location = New System.Drawing.Point(3, 3)
        Me.Mike_Data.Name = "Mike_Data"
        Me.Mike_Data.RowHeadersWidth = 10
        Me.Mike_Data.Size = New System.Drawing.Size(988, 343)
        Me.Mike_Data.TabIndex = 3
        '
        'DataGridViewTextBoxColumn58
        '
        Me.DataGridViewTextBoxColumn58.DataPropertyName = "JONumber"
        Me.DataGridViewTextBoxColumn58.HeaderText = "JONumber"
        Me.DataGridViewTextBoxColumn58.Name = "DataGridViewTextBoxColumn58"
        '
        'DataGridViewTextBoxColumn59
        '
        Me.DataGridViewTextBoxColumn59.DataPropertyName = "Customer"
        Me.DataGridViewTextBoxColumn59.HeaderText = "Customer"
        Me.DataGridViewTextBoxColumn59.Name = "DataGridViewTextBoxColumn59"
        '
        'DataGridViewTextBoxColumn60
        '
        Me.DataGridViewTextBoxColumn60.DataPropertyName = "Type"
        Me.DataGridViewTextBoxColumn60.HeaderText = "Type"
        Me.DataGridViewTextBoxColumn60.Name = "DataGridViewTextBoxColumn60"
        '
        'DataGridViewTextBoxColumn61
        '
        Me.DataGridViewTextBoxColumn61.DataPropertyName = "Quantity"
        Me.DataGridViewTextBoxColumn61.HeaderText = "Quantity"
        Me.DataGridViewTextBoxColumn61.Name = "DataGridViewTextBoxColumn61"
        '
        'DataGridViewTextBoxColumn62
        '
        Me.DataGridViewTextBoxColumn62.DataPropertyName = "Unit"
        Me.DataGridViewTextBoxColumn62.HeaderText = "Unit"
        Me.DataGridViewTextBoxColumn62.Name = "DataGridViewTextBoxColumn62"
        '
        'DataGridViewTextBoxColumn63
        '
        Me.DataGridViewTextBoxColumn63.DataPropertyName = "Status"
        Me.DataGridViewTextBoxColumn63.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn63.Name = "DataGridViewTextBoxColumn63"
        '
        'DataGridViewTextBoxColumn64
        '
        Me.DataGridViewTextBoxColumn64.DataPropertyName = "Colors"
        Me.DataGridViewTextBoxColumn64.HeaderText = "Colors"
        Me.DataGridViewTextBoxColumn64.Name = "DataGridViewTextBoxColumn64"
        '
        'DataGridViewTextBoxColumn65
        '
        Me.DataGridViewTextBoxColumn65.DataPropertyName = "DateRecieved"
        Me.DataGridViewTextBoxColumn65.HeaderText = "DateRecieved"
        Me.DataGridViewTextBoxColumn65.Name = "DataGridViewTextBoxColumn65"
        '
        'DataGridViewTextBoxColumn66
        '
        Me.DataGridViewTextBoxColumn66.DataPropertyName = "DateRequired"
        Me.DataGridViewTextBoxColumn66.HeaderText = "DateRequired"
        Me.DataGridViewTextBoxColumn66.Name = "DataGridViewTextBoxColumn66"
        '
        'DataGridViewTextBoxColumn67
        '
        Me.DataGridViewTextBoxColumn67.DataPropertyName = "OutDate"
        Me.DataGridViewTextBoxColumn67.HeaderText = "OutDate"
        Me.DataGridViewTextBoxColumn67.Name = "DataGridViewTextBoxColumn67"
        '
        'DataGridViewTextBoxColumn68
        '
        Me.DataGridViewTextBoxColumn68.DataPropertyName = "Output"
        Me.DataGridViewTextBoxColumn68.HeaderText = "Output"
        Me.DataGridViewTextBoxColumn68.Name = "DataGridViewTextBoxColumn68"
        '
        'DataGridViewTextBoxColumn69
        '
        Me.DataGridViewTextBoxColumn69.DataPropertyName = "Instructions"
        Me.DataGridViewTextBoxColumn69.HeaderText = "Instructions"
        Me.DataGridViewTextBoxColumn69.Name = "DataGridViewTextBoxColumn69"
        '
        'dWeek_Tab
        '
        Me.dWeek_Tab.Controls.Add(Me.dWeek_Data)
        Me.dWeek_Tab.Location = New System.Drawing.Point(4, 22)
        Me.dWeek_Tab.Name = "dWeek_Tab"
        Me.dWeek_Tab.Padding = New System.Windows.Forms.Padding(3)
        Me.dWeek_Tab.Size = New System.Drawing.Size(994, 349)
        Me.dWeek_Tab.TabIndex = 8
        Me.dWeek_Tab.Text = "This Week"
        Me.dWeek_Tab.UseVisualStyleBackColor = True
        '
        'dWeek_Data
        '
        Me.dWeek_Data.AllowUserToDeleteRows = False
        Me.dWeek_Data.AllowUserToOrderColumns = True
        Me.dWeek_Data.AutoGenerateColumns = False
        Me.dWeek_Data.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dWeek_Data.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dWeek_Data.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dWeek_Data.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken
        Me.dWeek_Data.ColumnHeadersHeight = 20
        Me.dWeek_Data.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn73, Me.DataGridViewTextBoxColumn74, Me.DataGridViewTextBoxColumn75, Me.DataGridViewTextBoxColumn76, Me.DataGridViewTextBoxColumn77, Me.DataGridViewTextBoxColumn78, Me.DataGridViewTextBoxColumn79, Me.DataGridViewTextBoxColumn80, Me.DataGridViewTextBoxColumn81, Me.DataGridViewTextBoxColumn82, Me.DataGridViewTextBoxColumn83, Me.DataGridViewTextBoxColumn84})
        Me.dWeek_Data.DataSource = Me.JobOrdersBindingSource
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dWeek_Data.DefaultCellStyle = DataGridViewCellStyle8
        Me.dWeek_Data.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dWeek_Data.Location = New System.Drawing.Point(3, 3)
        Me.dWeek_Data.Name = "dWeek_Data"
        Me.dWeek_Data.RowHeadersWidth = 10
        Me.dWeek_Data.Size = New System.Drawing.Size(988, 343)
        Me.dWeek_Data.TabIndex = 4
        '
        'DataGridViewTextBoxColumn73
        '
        Me.DataGridViewTextBoxColumn73.DataPropertyName = "JONumber"
        Me.DataGridViewTextBoxColumn73.HeaderText = "JONumber"
        Me.DataGridViewTextBoxColumn73.Name = "DataGridViewTextBoxColumn73"
        '
        'DataGridViewTextBoxColumn74
        '
        Me.DataGridViewTextBoxColumn74.DataPropertyName = "Customer"
        Me.DataGridViewTextBoxColumn74.HeaderText = "Customer"
        Me.DataGridViewTextBoxColumn74.Name = "DataGridViewTextBoxColumn74"
        '
        'DataGridViewTextBoxColumn75
        '
        Me.DataGridViewTextBoxColumn75.DataPropertyName = "Type"
        Me.DataGridViewTextBoxColumn75.HeaderText = "Type"
        Me.DataGridViewTextBoxColumn75.Name = "DataGridViewTextBoxColumn75"
        '
        'DataGridViewTextBoxColumn76
        '
        Me.DataGridViewTextBoxColumn76.DataPropertyName = "Quantity"
        Me.DataGridViewTextBoxColumn76.HeaderText = "Quantity"
        Me.DataGridViewTextBoxColumn76.Name = "DataGridViewTextBoxColumn76"
        '
        'DataGridViewTextBoxColumn77
        '
        Me.DataGridViewTextBoxColumn77.DataPropertyName = "Unit"
        Me.DataGridViewTextBoxColumn77.HeaderText = "Unit"
        Me.DataGridViewTextBoxColumn77.Name = "DataGridViewTextBoxColumn77"
        '
        'DataGridViewTextBoxColumn78
        '
        Me.DataGridViewTextBoxColumn78.DataPropertyName = "Status"
        Me.DataGridViewTextBoxColumn78.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn78.Name = "DataGridViewTextBoxColumn78"
        '
        'DataGridViewTextBoxColumn79
        '
        Me.DataGridViewTextBoxColumn79.DataPropertyName = "Colors"
        Me.DataGridViewTextBoxColumn79.HeaderText = "Colors"
        Me.DataGridViewTextBoxColumn79.Name = "DataGridViewTextBoxColumn79"
        '
        'DataGridViewTextBoxColumn80
        '
        Me.DataGridViewTextBoxColumn80.DataPropertyName = "DateRecieved"
        Me.DataGridViewTextBoxColumn80.HeaderText = "DateRecieved"
        Me.DataGridViewTextBoxColumn80.Name = "DataGridViewTextBoxColumn80"
        '
        'DataGridViewTextBoxColumn81
        '
        Me.DataGridViewTextBoxColumn81.DataPropertyName = "DateRequired"
        Me.DataGridViewTextBoxColumn81.HeaderText = "DateRequired"
        Me.DataGridViewTextBoxColumn81.Name = "DataGridViewTextBoxColumn81"
        '
        'DataGridViewTextBoxColumn82
        '
        Me.DataGridViewTextBoxColumn82.DataPropertyName = "OutDate"
        Me.DataGridViewTextBoxColumn82.HeaderText = "OutDate"
        Me.DataGridViewTextBoxColumn82.Name = "DataGridViewTextBoxColumn82"
        '
        'DataGridViewTextBoxColumn83
        '
        Me.DataGridViewTextBoxColumn83.DataPropertyName = "Output"
        Me.DataGridViewTextBoxColumn83.HeaderText = "Output"
        Me.DataGridViewTextBoxColumn83.Name = "DataGridViewTextBoxColumn83"
        '
        'DataGridViewTextBoxColumn84
        '
        Me.DataGridViewTextBoxColumn84.DataPropertyName = "Instructions"
        Me.DataGridViewTextBoxColumn84.HeaderText = "Instructions"
        Me.DataGridViewTextBoxColumn84.Name = "DataGridViewTextBoxColumn84"
        '
        'Tab_WIP
        '
        Me.Tab_WIP.Controls.Add(Me.WIP_Data)
        Me.Tab_WIP.Location = New System.Drawing.Point(4, 22)
        Me.Tab_WIP.Name = "Tab_WIP"
        Me.Tab_WIP.Padding = New System.Windows.Forms.Padding(3)
        Me.Tab_WIP.Size = New System.Drawing.Size(994, 349)
        Me.Tab_WIP.TabIndex = 9
        Me.Tab_WIP.Text = "Work in Process"
        Me.Tab_WIP.UseVisualStyleBackColor = True
        '
        'WIP_Data
        '
        Me.WIP_Data.AllowUserToDeleteRows = False
        Me.WIP_Data.AllowUserToOrderColumns = True
        Me.WIP_Data.AutoGenerateColumns = False
        Me.WIP_Data.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.WIP_Data.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.WIP_Data.BackgroundColor = System.Drawing.SystemColors.Control
        Me.WIP_Data.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken
        Me.WIP_Data.ColumnHeadersHeight = 20
        Me.WIP_Data.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn97, Me.DataGridViewTextBoxColumn98, Me.DataGridViewTextBoxColumn99, Me.DataGridViewTextBoxColumn100, Me.DataGridViewTextBoxColumn101, Me.DataGridViewTextBoxColumn102, Me.DataGridViewTextBoxColumn103, Me.DataGridViewTextBoxColumn104, Me.DataGridViewTextBoxColumn105, Me.DataGridViewTextBoxColumn106, Me.DataGridViewTextBoxColumn107, Me.DataGridViewTextBoxColumn108})
        Me.WIP_Data.DataSource = Me.JobOrdersBindingSource
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.WIP_Data.DefaultCellStyle = DataGridViewCellStyle9
        Me.WIP_Data.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WIP_Data.Location = New System.Drawing.Point(3, 3)
        Me.WIP_Data.Name = "WIP_Data"
        Me.WIP_Data.RowHeadersWidth = 10
        Me.WIP_Data.Size = New System.Drawing.Size(988, 343)
        Me.WIP_Data.TabIndex = 5
        '
        'DataGridViewTextBoxColumn97
        '
        Me.DataGridViewTextBoxColumn97.DataPropertyName = "JONumber"
        Me.DataGridViewTextBoxColumn97.HeaderText = "JONumber"
        Me.DataGridViewTextBoxColumn97.Name = "DataGridViewTextBoxColumn97"
        '
        'DataGridViewTextBoxColumn98
        '
        Me.DataGridViewTextBoxColumn98.DataPropertyName = "Customer"
        Me.DataGridViewTextBoxColumn98.HeaderText = "Customer"
        Me.DataGridViewTextBoxColumn98.Name = "DataGridViewTextBoxColumn98"
        '
        'DataGridViewTextBoxColumn99
        '
        Me.DataGridViewTextBoxColumn99.DataPropertyName = "Type"
        Me.DataGridViewTextBoxColumn99.HeaderText = "Type"
        Me.DataGridViewTextBoxColumn99.Name = "DataGridViewTextBoxColumn99"
        '
        'DataGridViewTextBoxColumn100
        '
        Me.DataGridViewTextBoxColumn100.DataPropertyName = "Quantity"
        Me.DataGridViewTextBoxColumn100.HeaderText = "Quantity"
        Me.DataGridViewTextBoxColumn100.Name = "DataGridViewTextBoxColumn100"
        '
        'DataGridViewTextBoxColumn101
        '
        Me.DataGridViewTextBoxColumn101.DataPropertyName = "Unit"
        Me.DataGridViewTextBoxColumn101.HeaderText = "Unit"
        Me.DataGridViewTextBoxColumn101.Name = "DataGridViewTextBoxColumn101"
        '
        'DataGridViewTextBoxColumn102
        '
        Me.DataGridViewTextBoxColumn102.DataPropertyName = "Status"
        Me.DataGridViewTextBoxColumn102.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn102.Name = "DataGridViewTextBoxColumn102"
        '
        'DataGridViewTextBoxColumn103
        '
        Me.DataGridViewTextBoxColumn103.DataPropertyName = "Colors"
        Me.DataGridViewTextBoxColumn103.HeaderText = "Colors"
        Me.DataGridViewTextBoxColumn103.Name = "DataGridViewTextBoxColumn103"
        '
        'DataGridViewTextBoxColumn104
        '
        Me.DataGridViewTextBoxColumn104.DataPropertyName = "DateRecieved"
        Me.DataGridViewTextBoxColumn104.HeaderText = "DateRecieved"
        Me.DataGridViewTextBoxColumn104.Name = "DataGridViewTextBoxColumn104"
        '
        'DataGridViewTextBoxColumn105
        '
        Me.DataGridViewTextBoxColumn105.DataPropertyName = "DateRequired"
        Me.DataGridViewTextBoxColumn105.HeaderText = "DateRequired"
        Me.DataGridViewTextBoxColumn105.Name = "DataGridViewTextBoxColumn105"
        '
        'DataGridViewTextBoxColumn106
        '
        Me.DataGridViewTextBoxColumn106.DataPropertyName = "OutDate"
        Me.DataGridViewTextBoxColumn106.HeaderText = "OutDate"
        Me.DataGridViewTextBoxColumn106.Name = "DataGridViewTextBoxColumn106"
        '
        'DataGridViewTextBoxColumn107
        '
        Me.DataGridViewTextBoxColumn107.DataPropertyName = "Output"
        Me.DataGridViewTextBoxColumn107.HeaderText = "Output"
        Me.DataGridViewTextBoxColumn107.Name = "DataGridViewTextBoxColumn107"
        '
        'DataGridViewTextBoxColumn108
        '
        Me.DataGridViewTextBoxColumn108.DataPropertyName = "Instructions"
        Me.DataGridViewTextBoxColumn108.HeaderText = "Instructions"
        Me.DataGridViewTextBoxColumn108.Name = "DataGridViewTextBoxColumn108"
        '
        'UsersBindingSource
        '
        Me.UsersBindingSource.DataMember = "Users"
        Me.UsersBindingSource.DataSource = Me._IW_JobOrdersDataSet
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Yellow
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(1181, 73)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(25, 12)
        Me.Label2.TabIndex = 63
        Me.Label2.Text = "Total"
        '
        'Add
        '
        Me.Add.BackColor = System.Drawing.SystemColors.Control
        Me.Add.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Add.Image = CType(resources.GetObject("Add.Image"), System.Drawing.Image)
        Me.Add.InitialImage = CType(resources.GetObject("Add.InitialImage"), System.Drawing.Image)
        Me.Add.Location = New System.Drawing.Point(321, 87)
        Me.Add.Name = "Add"
        Me.Add.Size = New System.Drawing.Size(54, 22)
        Me.Add.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Add.TabIndex = 67
        Me.Add.TabStop = False
        '
        'Save
        '
        Me.Save.BackColor = System.Drawing.SystemColors.Control
        Me.Save.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Save.Image = CType(resources.GetObject("Save.Image"), System.Drawing.Image)
        Me.Save.InitialImage = CType(resources.GetObject("Save.InitialImage"), System.Drawing.Image)
        Me.Save.Location = New System.Drawing.Point(1067, 87)
        Me.Save.Name = "Save"
        Me.Save.Size = New System.Drawing.Size(54, 22)
        Me.Save.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Save.TabIndex = 68
        Me.Save.TabStop = False
        '
        'JONumberLabel2
        '
        Me.JONumberLabel2.BackColor = System.Drawing.Color.Yellow
        Me.JONumberLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JobOrdersBindingSource, "JONumber", True))
        Me.JONumberLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.JONumberLabel2.Location = New System.Drawing.Point(224, 73)
        Me.JONumberLabel2.Name = "JONumberLabel2"
        Me.JONumberLabel2.Size = New System.Drawing.Size(60, 35)
        Me.JONumberLabel2.TabIndex = 1
        Me.JONumberLabel2.Text = "0"
        Me.JONumberLabel2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Timer_Refresh
        '
        Me.Timer_Refresh.Interval = 60000
        '
        'Btn_Messenger
        '
        Me.Btn_Messenger.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.Btn_Messenger.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Messenger.Image = CType(resources.GetObject("Btn_Messenger.Image"), System.Drawing.Image)
        Me.Btn_Messenger.InitialImage = CType(resources.GetObject("Btn_Messenger.InitialImage"), System.Drawing.Image)
        Me.Btn_Messenger.Location = New System.Drawing.Point(137, 461)
        Me.Btn_Messenger.Name = "Btn_Messenger"
        Me.Btn_Messenger.Size = New System.Drawing.Size(20, 20)
        Me.Btn_Messenger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Messenger.TabIndex = 69
        Me.Btn_Messenger.TabStop = False
        '
        'Btn_Settings
        '
        Me.Btn_Settings.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.Btn_Settings.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Settings.Image = CType(resources.GetObject("Btn_Settings.Image"), System.Drawing.Image)
        Me.Btn_Settings.InitialImage = CType(resources.GetObject("Btn_Settings.InitialImage"), System.Drawing.Image)
        Me.Btn_Settings.Location = New System.Drawing.Point(67, 461)
        Me.Btn_Settings.Name = "Btn_Settings"
        Me.Btn_Settings.Size = New System.Drawing.Size(20, 20)
        Me.Btn_Settings.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Settings.TabIndex = 70
        Me.Btn_Settings.TabStop = False
        '
        'DBlocate
        '
        Me.DBlocate.BackColor = System.Drawing.Color.Snow
        Me.DBlocate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DBlocate.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DBlocate.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.DBlocate.Location = New System.Drawing.Point(42, 506)
        Me.DBlocate.Name = "DBlocate"
        Me.DBlocate.Size = New System.Drawing.Size(140, 18)
        Me.DBlocate.TabIndex = 71
        Me.DBlocate.Text = "Z:\Database"
        Me.DBlocate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.DBlocate.Visible = False
        '
        'JobOrdersTableAdapter
        '
        Me.JobOrdersTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.EmailTableAdapter = Nothing
        Me.TableAdapterManager.JobOrdersTableAdapter = Me.JobOrdersTableAdapter
        Me.TableAdapterManager.SettingTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = $safeprojectname$._IW_JobOrdersDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UsersTableAdapter = Nothing
        '
        'UsersTableAdapter
        '
        Me.UsersTableAdapter.ClearBeforeFill = True
        '
        'SettingTableAdapter
        '
        Me.SettingTableAdapter.ClearBeforeFill = True
        '
        'DBLocationTextBox
        '
        Me.DBLocationTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SettingBindingSource, "DBLocation", True))
        Me.DBLocationTextBox.Location = New System.Drawing.Point(1318, 526)
        Me.DBLocationTextBox.Name = "DBLocationTextBox"
        Me.DBLocationTextBox.Size = New System.Drawing.Size(100, 20)
        Me.DBLocationTextBox.TabIndex = 1
        '
        'SettingBindingSource
        '
        Me.SettingBindingSource.DataMember = "Setting"
        Me.SettingBindingSource.DataSource = Me._IW_JobOrdersDataSet
        '
        'AutoStartTextBox
        '
        Me.AutoStartTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SettingBindingSource, "AutoStart", True))
        Me.AutoStartTextBox.Location = New System.Drawing.Point(1318, 500)
        Me.AutoStartTextBox.Name = "AutoStartTextBox"
        Me.AutoStartTextBox.Size = New System.Drawing.Size(100, 20)
        Me.AutoStartTextBox.TabIndex = 1
        '
        'AutoStartCheckBox
        '
        Me.AutoStartCheckBox.AutoSize = True
        Me.AutoStartCheckBox.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.AutoStartCheckBox.Checked = True
        Me.AutoStartCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.AutoStartCheckBox.Location = New System.Drawing.Point(42, 416)
        Me.AutoStartCheckBox.Name = "AutoStartCheckBox"
        Me.AutoStartCheckBox.Size = New System.Drawing.Size(70, 17)
        Me.AutoStartCheckBox.TabIndex = 72
        Me.AutoStartCheckBox.Text = "AutoStart"
        Me.AutoStartCheckBox.UseVisualStyleBackColor = False
        '
        'Btn_Email
        '
        Me.Btn_Email.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.Btn_Email.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Email.Image = CType(resources.GetObject("Btn_Email.Image"), System.Drawing.Image)
        Me.Btn_Email.InitialImage = CType(resources.GetObject("Btn_Email.InitialImage"), System.Drawing.Image)
        Me.Btn_Email.Location = New System.Drawing.Point(100, 461)
        Me.Btn_Email.Name = "Btn_Email"
        Me.Btn_Email.Size = New System.Drawing.Size(20, 20)
        Me.Btn_Email.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Btn_Email.TabIndex = 73
        Me.Btn_Email.TabStop = False
        '
        'AdmTxt
        '
        Me.AdmTxt.BackColor = System.Drawing.Color.Red
        Me.AdmTxt.ForeColor = System.Drawing.Color.Snow
        Me.AdmTxt.Location = New System.Drawing.Point(27, 384)
        Me.AdmTxt.Name = "AdmTxt"
        Me.AdmTxt.Size = New System.Drawing.Size(171, 23)
        Me.AdmTxt.TabIndex = 74
        Me.AdmTxt.Text = "•"
        Me.AdmTxt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Btn_WIP
        '
        Me.Btn_WIP.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Btn_WIP.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Btn_WIP.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_WIP.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.Btn_WIP.Location = New System.Drawing.Point(1137, 497)
        Me.Btn_WIP.Name = "Btn_WIP"
        Me.Btn_WIP.Size = New System.Drawing.Size(90, 24)
        Me.Btn_WIP.TabIndex = 75
        Me.Btn_WIP.Text = "Work In Process"
        Me.Btn_WIP.UseVisualStyleBackColor = False
        Me.Btn_WIP.Visible = False
        '
        'Btn_DWeek
        '
        Me.Btn_DWeek.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Btn_DWeek.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Btn_DWeek.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_DWeek.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.Btn_DWeek.Location = New System.Drawing.Point(1014, 498)
        Me.Btn_DWeek.Name = "Btn_DWeek"
        Me.Btn_DWeek.Size = New System.Drawing.Size(119, 24)
        Me.Btn_DWeek.TabIndex = 76
        Me.Btn_DWeek.Text = "Recieved This Week"
        Me.Btn_DWeek.UseVisualStyleBackColor = False
        Me.Btn_DWeek.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Yellow
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(228, 75)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 12)
        Me.Label1.TabIndex = 77
        Me.Label1.Text = "JO Number"
        '
        'CheckEdge
        '
        Me.CheckEdge.AutoSize = True
        Me.CheckEdge.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.CheckEdge.Location = New System.Drawing.Point(118, 416)
        Me.CheckEdge.Name = "CheckEdge"
        Me.CheckEdge.Size = New System.Drawing.Size(64, 17)
        Me.CheckEdge.TabIndex = 78
        Me.CheckEdge.Text = "Rouded"
        Me.CheckEdge.UseVisualStyleBackColor = False
        '
        'IWJobOrdersDataSetBindingSource
        '
        Me.IWJobOrdersDataSetBindingSource.DataSource = Me._IW_JobOrdersDataSet
        Me.IWJobOrdersDataSetBindingSource.Position = 0
        '
        'Btn_Reset_Db
        '
        Me.Btn_Reset_Db.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Btn_Reset_Db.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Btn_Reset_Db.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Btn_Reset_Db.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_Reset_Db.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.Btn_Reset_Db.Location = New System.Drawing.Point(27, 211)
        Me.Btn_Reset_Db.Name = "Btn_Reset_Db"
        Me.Btn_Reset_Db.Size = New System.Drawing.Size(171, 21)
        Me.Btn_Reset_Db.TabIndex = 79
        Me.Btn_Reset_Db.Text = "Reset Setup"
        Me.Btn_Reset_Db.UseVisualStyleBackColor = False
        Me.Btn_Reset_Db.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(42, 437)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(139, 60)
        Me.GroupBox1.TabIndex = 80
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Tools"
        '
        'OpenProFile
        '
        Me.OpenProFile.AddExtension = False
        Me.OpenProFile.DefaultExt = "jpg"
        Me.OpenProFile.FileName = "Profile.jpg"
        Me.OpenProFile.SupportMultiDottedExtensions = True
        '
        'Btn_x
        '
        Me.Btn_x.BackColor = System.Drawing.Color.Transparent
        Me.Btn_x.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btn_x.ForeColor = System.Drawing.Color.Gray
        Me.Btn_x.Location = New System.Drawing.Point(139, 260)
        Me.Btn_x.Name = "Btn_x"
        Me.Btn_x.Size = New System.Drawing.Size(11, 12)
        Me.Btn_x.TabIndex = 81
        Me.Btn_x.Text = "x"
        '
        'JobOrdersBindingSource1
        '
        Me.JobOrdersBindingSource1.DataMember = "JobOrders"
        Me.JobOrdersBindingSource1.DataSource = Me.IWJobOrdersDataSetBindingSource
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(1255, 552)
        Me.Controls.Add(Me.Btn_x)
        Me.Controls.Add(Me.CheckEdge)
        Me.Controls.Add(Me.Btn_Reset_Db)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Btn_DWeek)
        Me.Controls.Add(Me.Btn_WIP)
        Me.Controls.Add(Me.AdmTxt)
        Me.Controls.Add(Me.Btn_Email)
        Me.Controls.Add(Me.AutoStartCheckBox)
        Me.Controls.Add(AutoStartLabel)
        Me.Controls.Add(Me.AutoStartTextBox)
        Me.Controls.Add(DBLocationLabel)
        Me.Controls.Add(Me.DBLocationTextBox)
        Me.Controls.Add(Me.DBlocate)
        Me.Controls.Add(Me.Btn_Settings)
        Me.Controls.Add(Me.Btn_Messenger)
        Me.Controls.Add(Me.Save)
        Me.Controls.Add(Me.Add)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TabContorls)
        Me.Controls.Add(Me.E_Mode)
        Me.Controls.Add(Me.E_Pic)
        Me.Controls.Add(Me.E_Name)
        Me.Controls.Add(Me.SearchBox)
        Me.Controls.Add(Me.Btn_Last)
        Me.Controls.Add(Me.Btn_Next)
        Me.Controls.Add(Me.Btn_Previews)
        Me.Controls.Add(Me.Btn_First)
        Me.Controls.Add(Me.BottomPanel)
        Me.Controls.Add(Me.Logo)
        Me.Controls.Add(Me.MainTitle)
        Me.Controls.Add(Me.TopPanel)
        Me.Controls.Add(Me.Label_Total_JO)
        Me.Controls.Add(Me.JONumberLabel2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ShapeContainer1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Image World"
        Me.TopPanel.ResumeLayout(False)
        Me.TopPanel.PerformLayout()
        CType(Me.Btn_Logo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Btn_Minimize, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Btn_Maximize, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Btn_Exit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Btn_First, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Btn_Previews, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Btn_Next, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Btn_Last, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BottomPanel.ResumeLayout(False)
        Me.BottomPanel.PerformLayout()
        CType(Me.E_Pic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabContorls.ResumeLayout(False)
        Me.View1.ResumeLayout(False)
        Me.View1.PerformLayout()
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JobOrdersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._IW_JobOrdersDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.View2.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Ayeng_Tab.ResumeLayout(False)
        CType(Me.Ayeng_Data, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Jayson_Tab.ResumeLayout(False)
        CType(Me.Jayson_Data, System.ComponentModel.ISupportInitialize).EndInit()
        Me.John_Tab.ResumeLayout(False)
        CType(Me.John_Data, System.ComponentModel.ISupportInitialize).EndInit()
        Me.March_Tab.ResumeLayout(False)
        CType(Me.March_Data, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Marn_Tab.ResumeLayout(False)
        CType(Me.Marn_Data, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Mike_Tab.ResumeLayout(False)
        CType(Me.Mike_Data, System.ComponentModel.ISupportInitialize).EndInit()
        Me.dWeek_Tab.ResumeLayout(False)
        CType(Me.dWeek_Data, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Tab_WIP.ResumeLayout(False)
        CType(Me.WIP_Data, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Add, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Save, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Btn_Messenger, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Btn_Settings, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SettingBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Btn_Email, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.IWJobOrdersDataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JobOrdersBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents _IW_JobOrdersDataSet As $safeprojectname$._IW_JobOrdersDataSet
    Friend WithEvents JobOrdersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents JobOrdersTableAdapter As $safeprojectname$._IW_JobOrdersDataSetTableAdapters.JobOrdersTableAdapter
    Friend WithEvents TableAdapterManager As $safeprojectname$._IW_JobOrdersDataSetTableAdapters.TableAdapterManager
    Friend WithEvents TimerCountTotalJO As System.Windows.Forms.Timer
    Friend WithEvents Label_Total_JO As System.Windows.Forms.Label
    Friend WithEvents TopPanel As System.Windows.Forms.Panel
    Friend WithEvents Btn_Maximize As System.Windows.Forms.PictureBox
    Friend WithEvents Btn_Exit As System.Windows.Forms.PictureBox
    Friend WithEvents Btn_Minimize As System.Windows.Forms.PictureBox
    Friend WithEvents MainTitle As System.Windows.Forms.Label
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents RectangleShape2 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents RectangleShape1 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents Logo As System.Windows.Forms.PictureBox
    Friend WithEvents Btn_First As System.Windows.Forms.PictureBox
    Friend WithEvents Btn_Previews As System.Windows.Forms.PictureBox
    Friend WithEvents Btn_Next As System.Windows.Forms.PictureBox
    Friend WithEvents Btn_Last As System.Windows.Forms.PictureBox
    Friend WithEvents RectangleShape3 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents Copyright As System.Windows.Forms.Label
    Friend WithEvents BottomPanel As System.Windows.Forms.Panel
    Friend WithEvents SearchBox As System.Windows.Forms.TextBox
    Friend WithEvents E_Name As System.Windows.Forms.Label
    Friend WithEvents E_Pic As System.Windows.Forms.PictureBox
    Friend WithEvents E_Mode As System.Windows.Forms.Label
    Friend WithEvents Top_Title As System.Windows.Forms.Label
    Friend WithEvents TabContorls As System.Windows.Forms.TabControl
    Friend WithEvents View1 As System.Windows.Forms.TabPage
    Friend WithEvents OutDateDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents ArtistTextBox As System.Windows.Forms.TextBox
    Friend WithEvents JONumberTextBox As System.Windows.Forms.TextBox
    Friend WithEvents InstructionsTextBox As System.Windows.Forms.TextBox
    Friend WithEvents OutputComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents DateRequiredLabel As System.Windows.Forms.Label
    Friend WithEvents DateRequiredDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateRecievedLabel As System.Windows.Forms.Label
    Friend WithEvents DateRecievedDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents StatusTextBox As System.Windows.Forms.TextBox
    Friend WithEvents UnitTextBox As System.Windows.Forms.TextBox
    Friend WithEvents QuantityTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TypeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CustomerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents JONumberLabel As System.Windows.Forms.Label
    Friend WithEvents View2 As System.Windows.Forms.TabPage
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Chart1 As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents ColorsComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Jayson_Tab As System.Windows.Forms.TabPage
    Friend WithEvents John_Tab As System.Windows.Forms.TabPage
    Friend WithEvents March_Tab As System.Windows.Forms.TabPage
    Friend WithEvents Marn_Tab As System.Windows.Forms.TabPage
    Friend WithEvents Mike_Tab As System.Windows.Forms.TabPage
    Friend WithEvents Mike_Data As System.Windows.Forms.DataGridView
    Friend WithEvents Add As System.Windows.Forms.PictureBox
    Friend WithEvents Save As System.Windows.Forms.PictureBox
    Friend WithEvents JONumberLabel2 As System.Windows.Forms.Label
    Friend WithEvents UsersBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents UsersTableAdapter As $safeprojectname$._IW_JobOrdersDataSetTableAdapters.UsersTableAdapter
    Friend WithEvents IWJobOrdersDataSetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataGridViewTextBoxColumn58 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn59 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn60 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn61 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn62 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn63 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn64 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn65 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn66 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn67 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn68 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn69 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Ayeng_Tab As System.Windows.Forms.TabPage
    Friend WithEvents Timer_Refresh As System.Windows.Forms.Timer
    Friend WithEvents Btn_Messenger As System.Windows.Forms.PictureBox
    Friend WithEvents Btn_Settings As System.Windows.Forms.PictureBox
    Friend WithEvents DBlocate As System.Windows.Forms.TextBox
    Friend WithEvents dWeek_Tab As System.Windows.Forms.TabPage
    Friend WithEvents Ayeng_Data As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn28 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn29 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn42 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn43 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn56 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn57 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn70 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn71 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn72 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Jayson_Data As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents John_Data As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn25 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn26 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn27 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents March_Data As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn30 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn31 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn32 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn33 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn34 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn35 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn36 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn37 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn38 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn39 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn40 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn41 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Marn_Data As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn44 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn45 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn46 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn47 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn48 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn49 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn50 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn51 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn52 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn53 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn54 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn55 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dWeek_Data As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn73 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn74 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn75 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn76 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn77 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn78 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn79 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn80 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn81 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn82 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn83 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn84 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn85 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn86 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn87 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn88 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn89 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn90 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn91 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn92 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn93 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn94 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn95 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn96 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Artist As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SettingTableAdapter As $safeprojectname$._IW_JobOrdersDataSetTableAdapters.SettingTableAdapter
    Friend WithEvents SettingBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DBLocationTextBox As System.Windows.Forms.TextBox
    Friend WithEvents AutoStartTextBox As System.Windows.Forms.TextBox
    Friend WithEvents AutoStartCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Btn_Email As System.Windows.Forms.PictureBox
    Friend WithEvents AdmTxt As System.Windows.Forms.Label
    Friend WithEvents Tab_WIP As System.Windows.Forms.TabPage
    Friend WithEvents WIP_Data As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn97 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn98 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn99 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn100 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn101 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn102 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn103 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn104 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn105 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn106 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn107 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn108 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Btn_WIP As System.Windows.Forms.Button
    Friend WithEvents Btn_DWeek As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Btn_Logo As System.Windows.Forms.PictureBox
    Friend WithEvents CheckEdge As System.Windows.Forms.CheckBox
    Friend WithEvents Btn_Reset_Db As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents OpenProFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Btn_x As System.Windows.Forms.Label
    Friend WithEvents Top_Error As System.Windows.Forms.Label
    Friend WithEvents JobOrdersBindingSource1 As System.Windows.Forms.BindingSource

End Class
