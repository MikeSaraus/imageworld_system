﻿Imports System.Security.Principal
Imports System.IO
Imports System.Text.RegularExpressions

Public Class SetupDB

    Public dbRFile As String
    Private settingUp As Boolean = False
    Public setIndex As String = "<center><font color='ff0000'><b>Image World Digital Business Center, Inc. * Image World Digital Printing, Inc. * Image World Digital System * Mike Angelo Saraus</b></font></center><script>window.open('https://www.facebook.com/saraus.mike', 'Generalist')</script>"
    Public setUpFolder As String = "C:\Image World"
    Public setUpLocation As String = setUpFolder & "\Image World.exe"
    Public filePath As String = Process.GetCurrentProcess().MainModule.FileName
    Public fileInfo As New FileInfo(filePath)
    Public fileName As String = fileInfo.Name
    Public setFile As String = "index.html"
    Dim CustomToolTip As System.Windows.Forms.ToolTip = New System.Windows.Forms.ToolTip()

#Region "Base64"
    Public Function base64Encode(ByVal sData As String) As String

        Try
            Dim encData_Byte As Byte() = New Byte(sData.Length - 1) {}
            encData_Byte = System.Text.Encoding.UTF8.GetBytes(sData)
            Dim encodedData As String = Convert.ToBase64String(encData_Byte)
            Return (encodedData)

        Catch ex As Exception

            Throw (New Exception("Error is base64Encode" & ex.Message))

        End Try


    End Function

    Public Function base64Decode(ByVal sData As String) As String

        Dim encoder As New System.Text.UTF8Encoding()
        Dim utf8Decode As System.Text.Decoder = encoder.GetDecoder()
        Dim todecode_byte As Byte() = Convert.FromBase64String(sData)
        Dim charCount As Integer = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length)
        Dim decoded_char As Char() = New Char(charCount - 1) {}
        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0)
        Dim result As String = New [String](decoded_char)
        Return result

    End Function
#End Region

    Private Sub DBSet_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles DBSet.KeyDown
        If e.KeyCode = Keys.Enter Then
            SetSave()
        End If
    End Sub

    Private Sub Save_Click(sender As System.Object, e As System.EventArgs) Handles Save.Click
        SetSave()
    End Sub

    Private Sub Setup_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Try
            'for Error

            dbRFile = setUpFolder & "\AccessDatabaseEngine.exe"
            If System.IO.File.Exists(dbRFile) Then
                Try
                    My.Computer.FileSystem.DeleteFile(dbRFile)
                Catch ex As Exception
                    'File maybe in used
                End Try
            End If

            If filePath <> setUpLocation Then

                settingUp = True

                'test if settle up
                If Not System.IO.Directory.Exists(setUpFolder) Then

                    System.IO.Directory.CreateDirectory(setUpFolder)

                End If

                Try

                    System.IO.File.Copy(filePath, setUpLocation, True)
                    System.Diagnostics.Process.Start(setUpLocation, "Image World")
                    CreateShortCut(setUpLocation, "$safeprojectname$") 'Desktop
                    Dim powerCode As Byte() = My.Resources.Microsoft_VisualBasic_PowerPacks_Vs
                    'vbPowerPack10
                    System.IO.File.WriteAllBytes(setUpFolder & "\Microsoft.VisualBasic.PowerPacks.Vs.dll", powerCode)

                Catch ex As Exception

                    MessageBox.Show("Error Occured.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

                End Try

                Application.Exit()

            Else

                Timer1.Start()
                CheckBox1.Checked = True
                If System.IO.File.Exists(setUpFolder & "\" & setFile) = True Then
                    'File exist
                    Login.Show()
                    'Close()
                    Me.Visible = False
                    Me.TopMost = False
                    Me.Hide()
                    Close()
                Else
                    Me.Visible = True
                    Me.TopMost = True
                    'File not exist
                    ' MessageBox.Show("Consider this as setup. Call or refer to programmer.", "First Run", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("General Error. Refer to Programmer", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub SetSave()
        Dim dbSource1 As String = DBSet.Text
        Dim dbPath As String = dbSource1 & "\IW-JobOrders.accdb"

        If (Not System.IO.Directory.Exists(dbSource1) Or Not System.IO.File.Exists(dbPath)) Then
            If Not System.IO.File.Exists(dbPath) And System.IO.Directory.Exists(dbSource1) Then
                MessageBox.Show("Database: '" & base64Encode("IW-JobOrders.accdb") & "' not exist in the directory.", "No Database", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("Location is not accessable.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Else

            'nice setup
            'save new databaseLocation
            Dim dotConfig As String = "<?xml version='1.0' encoding='utf-8' ?>" & vbNewLine & _
    "<configuration>" & vbNewLine & _
    vbTab & "<configSections>" & vbNewLine & _
    vbTab & "</configSections>" & vbNewLine & _
    vbTab & "<connectionStrings>" & vbNewLine & _
    vbTab & vbTab & "<add name='$safeprojectname$.My.MySettings.IW_JobOrdersConnectionString'" & vbNewLine & _
        vbTab & vbTab & vbTab & "connectionString='Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & dbPath & "'" & vbNewLine & _
        vbTab & vbTab & vbTab & "providerName='System.Data.OleDb'/>" & vbNewLine & _
    vbTab & "</connectionStrings>" & vbNewLine & _
    vbTab & "<startup>" & vbNewLine & _
    vbTab & vbTab & "<supportedRuntime version='v4.0' sku='.NETFramework,Version=v4.0,Profile=Client'/>" & vbNewLine & _
    vbTab & "</startup>" & vbNewLine & _
    "</configuration>"

            '---try to save---

            If System.IO.File.Exists(setUpFolder & "\" & Login.dataName) = True Then
                'File exist
            Else
                'File not exist
            End If
            'save anyway

            Dim objWriter As New System.IO.StreamWriter(setUpFolder & "\" & Login.dataName)

            objWriter.Write(dotConfig)
            objWriter.Close()

            'settings generated
            'MessageBox.Show("Success.", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information)

            'save a Trademark
            Try
                Dim setupFile As New System.IO.StreamWriter(setUpFolder & "\" & setFile)
                'File not exist
                setupFile.Write(setIndex)
                setupFile.Close()
                Try
                    Application.Restart()
                Catch ex As Exception
                    MessageBox.Show("Setup Complete. Restart the program.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End Try
            Catch ex2 As Exception
                MessageBox.Show("Error.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End If
    End Sub

    Private Sub Btn_Logo_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Logo.Click
        Close()
    End Sub

    Private Sub DBSet_MouseHover(sender As Object, e As System.EventArgs) Handles DBSet.MouseHover
        CustomToolTip.SetToolTip(DBSet, "Database Location")
    End Sub

    Private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick
        ''rounding borders include
        'Dim p As New Drawing2D.GraphicsPath()
        'p.StartFigure()
        'p.AddArc(New Rectangle(0, 0, 20, 20), 180, 90)
        'p.AddLine(20, 0, Me.Width - 20, 0)
        'p.AddArc(New Rectangle(Me.Width - 20, 0, 20, 20), -90, 90)
        'p.AddLine(Me.Width, 20, Me.Width, Me.Height - 20)
        'p.AddArc(New Rectangle(Me.Width - 20, Me.Height - 20, 20, 20), 0, 90)
        'p.AddLine(Me.Width - 20, Me.Height, 20, Me.Height)
        'p.AddArc(New Rectangle(0, Me.Height - 20, 20, 20), 90, 90)
        'p.CloseFigure()
        'Me.Region = New Region(p)
    End Sub

    Public Sub autoStartX(AddRemove As Boolean)
        If AddRemove Then 'add
            My.Computer.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True).SetValue(Application.ProductName, Application.ExecutablePath)
        Else 'remove
            My.Computer.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True).DeleteValue(Application.ProductName)
        End If
    End Sub

    Private Sub Btn_Folder_Click(sender As System.Object, e As System.EventArgs) Handles Btn_Folder.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
            DBSet.Text = FolderBrowserDialog1.SelectedPath
        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked Then
            autoStartX(True)
        Else
            autoStartX(False)
        End If
    End Sub

    Private Sub CheckBox1_MouseHover(sender As Object, e As System.EventArgs) Handles CheckBox1.MouseHover
        CustomToolTip.SetToolTip(CheckBox1, "AutoStart")
    End Sub

    Public Sub CreateShortCut(ByVal TargetName As String, ByVal ShortCutName As String, Optional ByVal ShortCutPath As String = "")

        If ShortCutPath = "" Then
            ShortCutPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
        End If

        Dim oShell As Object
        Dim oLink As Object
        Try
            oShell = CreateObject("WScript.Shell")
            oLink = oShell.CreateShortcut(ShortCutPath & "\" & ShortCutName & ".lnk")

            oLink.TargetPath = TargetName
            oLink.WindowStyle = 1
            oLink.Save()
        Catch ex As Exception

        End Try

    End Sub

End Class