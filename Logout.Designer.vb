﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Logout
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Logout))
        Me.TopPanel = New System.Windows.Forms.Panel()
        Me.Top_Title = New System.Windows.Forms.Label()
        Me.BottomPanel = New System.Windows.Forms.Panel()
        Me.Save = New System.Windows.Forms.Button()
        Me.Txt_Note = New System.Windows.Forms.Label()
        Me.Copyright = New System.Windows.Forms.Label()
        Me.outKey = New System.Windows.Forms.PictureBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.TopPanel.SuspendLayout()
        Me.BottomPanel.SuspendLayout()
        CType(Me.outKey, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TopPanel
        '
        Me.TopPanel.BackColor = System.Drawing.Color.Black
        Me.TopPanel.Controls.Add(Me.Top_Title)
        Me.TopPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me.TopPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopPanel.Name = "TopPanel"
        Me.TopPanel.Size = New System.Drawing.Size(135, 25)
        Me.TopPanel.TabIndex = 49
        '
        'Top_Title
        '
        Me.Top_Title.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.Top_Title.AutoSize = True
        Me.Top_Title.BackColor = System.Drawing.Color.Black
        Me.Top_Title.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.Top_Title.Location = New System.Drawing.Point(46, 5)
        Me.Top_Title.Name = "Top_Title"
        Me.Top_Title.Size = New System.Drawing.Size(40, 13)
        Me.Top_Title.TabIndex = 51
        Me.Top_Title.Text = "Logout"
        Me.Top_Title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BottomPanel
        '
        Me.BottomPanel.BackColor = System.Drawing.Color.Black
        Me.BottomPanel.Controls.Add(Me.Save)
        Me.BottomPanel.Controls.Add(Me.Txt_Note)
        Me.BottomPanel.Controls.Add(Me.Copyright)
        Me.BottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BottomPanel.Location = New System.Drawing.Point(0, 125)
        Me.BottomPanel.Name = "BottomPanel"
        Me.BottomPanel.Size = New System.Drawing.Size(135, 75)
        Me.BottomPanel.TabIndex = 55
        '
        'Save
        '
        Me.Save.AutoEllipsis = True
        Me.Save.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Save.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.Save.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Save.FlatAppearance.BorderSize = 0
        Me.Save.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Save.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Save.Location = New System.Drawing.Point(34, 47)
        Me.Save.Name = "Save"
        Me.Save.Size = New System.Drawing.Size(61, 21)
        Me.Save.TabIndex = 1
        Me.Save.Text = "Save"
        Me.Save.UseVisualStyleBackColor = False
        '
        'Txt_Note
        '
        Me.Txt_Note.BackColor = System.Drawing.Color.Transparent
        Me.Txt_Note.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_Note.ForeColor = System.Drawing.Color.Brown
        Me.Txt_Note.Location = New System.Drawing.Point(11, 15)
        Me.Txt_Note.Name = "Txt_Note"
        Me.Txt_Note.Size = New System.Drawing.Size(111, 28)
        Me.Txt_Note.TabIndex = 32
        Me.Txt_Note.Text = "Unsave work will no longer recover."
        Me.Txt_Note.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Copyright
        '
        Me.Copyright.AutoSize = True
        Me.Copyright.BackColor = System.Drawing.Color.Transparent
        Me.Copyright.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Copyright.ForeColor = System.Drawing.Color.Red
        Me.Copyright.Location = New System.Drawing.Point(5, 4)
        Me.Copyright.Name = "Copyright"
        Me.Copyright.Size = New System.Drawing.Size(28, 12)
        Me.Copyright.TabIndex = 31
        Me.Copyright.Text = "Note:"
        '
        'outKey
        '
        Me.outKey.BackColor = System.Drawing.Color.Transparent
        Me.outKey.Image = CType(resources.GetObject("outKey.Image"), System.Drawing.Image)
        Me.outKey.Location = New System.Drawing.Point(19, 25)
        Me.outKey.Name = "outKey"
        Me.outKey.Size = New System.Drawing.Size(101, 100)
        Me.outKey.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.outKey.TabIndex = 61
        Me.outKey.TabStop = False
        '
        'Timer1
        '
        '
        'Logout
        '
        Me.AcceptButton = Me.Save
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(135, 200)
        Me.Controls.Add(Me.outKey)
        Me.Controls.Add(Me.BottomPanel)
        Me.Controls.Add(Me.TopPanel)
        Me.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Logout"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Logout"
        Me.TopMost = True
        Me.TopPanel.ResumeLayout(False)
        Me.TopPanel.PerformLayout()
        Me.BottomPanel.ResumeLayout(False)
        Me.BottomPanel.PerformLayout()
        CType(Me.outKey, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TopPanel As System.Windows.Forms.Panel
    Friend WithEvents Top_Title As System.Windows.Forms.Label
    Friend WithEvents BottomPanel As System.Windows.Forms.Panel
    Friend WithEvents Copyright As System.Windows.Forms.Label
    Friend WithEvents outKey As System.Windows.Forms.PictureBox
    Friend WithEvents Txt_Note As System.Windows.Forms.Label
    Friend WithEvents Save As System.Windows.Forms.Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
End Class
